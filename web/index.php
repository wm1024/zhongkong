<?php
//定义框架目录
session_start();
date_default_timezone_set('PRC');// 取中国的标准时区


define('CP_PATH', dirname(__FILE__).'/include/');//注意目录后面加“/”
define('CP_BASE_TEMP', '');//模板调用地址
define('CP_BASE_STATIC', '/');//模板调用地址

include_once(CP_PATH.'/ip.php');//加载配置
require(CP_PATH.'/config.php');//加载配置
require(CP_PATH.'core/cpApp.class.php');//加载应用控制类
$app=new cpApp($config);//实例化单一入口应用控制类
$app->run();//执行项目
