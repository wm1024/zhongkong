<?php
class aboutMod extends commonMod{

	public function index(){

		
		$this->assign('all_nav_2', true);
		$this->assign('class_name', '公司简介');
		$this->assign('about_nav_1', true);		
		$this->display('about');
	}
	public function advantage(){
		
		
		$this->assign('all_nav_2', true);
		$this->assign('class_name', '服务优势');
		$this->assign('about_nav_2', true);
		$this->display('advantage');
	}
	public function culture(){
		
		$this->assign('all_nav_2', true);
		$this->assign('class_name', '企业文化');
		$this->assign('about_nav_3', true);
		$this->display('culture');
	}


}