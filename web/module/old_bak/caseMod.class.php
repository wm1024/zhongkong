<?php
class caseMod extends commonMod{

	public function index(){

		$cid = intval($_GET['cid']);
		if (empty($cid)) $cid=1110;
		
		
		// 列表信息
        $listRows = 8;
        $page = intval($_GET['page']);
        $page = empty($page) ? 1:$page; if ($page<1) $page=1;
        $startRows = ($page-1)*$listRows;
		
		$where = " classid in ({$cid}".get_classsql($cid).") and `show`=1 and `recover`=0 ";
		
        $totalRows = $this->model->table('case')->where($where)->count();
        $list_arr  = $this->model->table('case')->where($where)->limit("$startRows,$listRows")->order('sequence desc,id desc')->select();
        //$page_str  = $this->page('/case/index-cid-'.$cid, $totalRows,$listRows);
        
        $this->assign('list_arr', $list_arr);
		//$this->assign('page_str', $page_str);

		$this->assign('all_nav_4', true);
		$this->assign('cid', $cid);
		$this->display('case');
	}

	public function ajax(){

		$cid = intval($_GET['cid']);
		if (empty($cid)) $cid=1110;
		
		
		// 列表信息
        $listRows = 8;
        $page = intval($_GET['page']);
        $page = empty($page) ? 1:$page; if ($page<1) $page=1;
		$page++;
        $startRows = ($page-1)*$listRows;
		
		$where = " classid in ({$cid}".get_classsql($cid).") and `show`=1 and `recover`=0 ";
        $list_arr  = $this->model->table('case')->where($where)->limit("$startRows,$listRows")->order('sequence desc,id desc')->select();

		if (! empty($list_arr)) {
			foreach($list_arr as $k=>$v) {
				
				$height = get_imgHeight(get_imgurl($v['pic']));
				
				$str .= '<div class="cell"><a href="/case/detail-id-'.$v['id'].'.html"><div class="case_img"><img src="/'.get_imgurl($v['pic']).'" width=290 height='.$height.' /></div><div class="case_con"><h1>'.str_len($v['title'],14).'</h1><p>地&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;点：'.$v['address'].'</p><p>时&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;间：'.$v['casetime'].'</p> <p>参加人数：'.$v['renshu'].'人</p><p>服务内容：'.$v['service'].'</p></div></a></div>';
				
			}
		}
		$arr['all'] = $str;
		$json_str = json_encode($arr);
		exit($json_str);
	}

	public function detail(){
		
		$id = intval($_GET['id']);
		if (empty($id)) exit('参数有误');
		
		
		$arr = __query("select * from {tabpre}case where id=".$id);
		$cid = $arr[0]['classid'];
		
		// 上一篇
		$prev_arr = __query("select * from {tabpre}case where id>$id and classid=$cid and `show`=1 and `recover`=0 order by id asc limit 1");
		$this->assign('prev_arr', $prev_arr[0]);
		
		// 下一篇
		$next_arr = __query("select * from {tabpre}case where id<$id and classid=$cid and `show`=1 and `recover`=0 order by id desc limit 1");
		$this->assign('next_arr', $next_arr[0]);
		
		
		$this->assign('all_nav_4', true);
		$this->assign('id', $id);
		$this->assign('cid', $cid);
		$this->assign('det', $arr[0]);
		$this->display('case_detail');
	}




}