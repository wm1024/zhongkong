<?php
class newsMod extends commonMod{


	// 新闻-频道页面
	public function index() {
		$nationid = get_nationId();

		// 头条推荐1
		$news_top_arr = __query("select * from {tabpre}news where `rmd1`=1 and `show`=1 and `recover`=0 and `nation`=".$nationid." order by sequence desc,id desc limit 0,1");

		// News Highlights
		$news_1136_arr = __query("select * from {tabpre}news where classid=1136 and `show`=1 and `recover`=0 and `nation`=".$nationid." order by sequence desc,id desc limit 0,3");

		// news 头条
		$news_rmd_arr = __query("select * from {tabpre}news where `rmd3`=1 and `show`=1 and `recover`=0 and `nation`=".$nationid." order by sequence desc,id desc limit 0,1");

		// news 列表
		$news_1135_arr = __query("select * from {tabpre}news where classid=1135 and `show`=1 and `recover`=0 and `nation`=".$nationid." order by sequence desc,id desc limit 0,4");

		$journal_time  = __query("select * from {tabpre}subscribe where `show`=1 and `recover`=0 order by `sequence` desc,`year` desc,`month` desc limit 0,1");
//print_r($news_1134_arr);
//		print_r($news_1135_arr);




		$this->assign('news_top_arr',  $news_top_arr);
		$this->assign('news_1136_arr',  $news_1136_arr);
		$this->assign('news_rmd_arr',  $news_rmd_arr);
		$this->assign('news_1135_arr',  $news_1135_arr);
		$this->assign('journal_time',  $journal_time);



		$this->display('news_index');
	}

	// 新闻-列表
	public function nlist() {
		$nationid = get_nationId();

		$cid = $_GET['cid']?intval($_GET['cid']):'';

		if(!empty($cid)){
			$where = ' and classid='.$cid;
		}else{
			$where = '';
		}
		/* 处理搜索 */
		$act = $_GET['act']?safe($_GET['act']):'';
		if(!empty($act)){
			safe($_POST);
			$keywords = $_POST['keywords'];//接收关键字

			$news = __query("select * from {tabpre}news where `nation` in(0,".$nationid.") and `show`=1 and `recover`=0 ".$where." and `title` like '%".$keywords."%' order by sequence desc,id desc");
		}else{
			$news = __query("select * from {tabpre}news where `nation` in(0,".$nationid.") and `show`=1 and `recover`=0 ".$where." order by sequence desc,id desc");
		}

		/*---------------分页参数------------------*/
		$pageNum = 12;

		$explainTable = 'news';
		//获取内容的条数
		$totalNum = count($news);
		//echo $count;
		// total($explainTable,$where);
		$pageSum = intval(ceil($totalNum/$pageNum));
		$page = empty($_GET['page'])?1:safe($_GET['page']);

		//恶意更改url后的操作
		if($totalNum != 0){
			if($page > ceil($totalNum/$pageNum) && $page != 1){
				msg("","/news/nlist-cid-".$cid."-page-1.html",3);
				$page = 1;
			}
		}
		/*----------------获得记录--------------*/
		$start = ($page-1)*$pageNum;
		$where = "classid=".$cid;
		$explainArr = selectList($explainTable,'',$start,$pageNum,$where);

		$this->assign('page',$page);
		$this->assign('pageSum',$pageSum);
		$this->assign('explainArr',$explainArr);

		$this->assign('cid',$cid);
		$this->assign('keywords',$keywords);
		$this->assign('news',$news);
		$this->display('news_list');
	}

	// 新闻-详细
	public function detail() {
		$nationid = get_nationId();

		$id = intval($_GET['id']);
		$rs = __query("select * from {tabpre}news where `id`=$id and `show`=1 and `recover`=0 order by sequence desc,id desc limit 0,1");
		$detail = $rs[0];

		/*上一篇 下一篇*/
		$sql = "select * from {tabpre}news where `nation`=".$nationid." and classid=".$detail['classid']." and `show`=1 and `recover`=0 order by rmd desc,sequence desc,id asc";
		$upanddown = get_updownpage($sql,$detail['id']);

		$this->assign('id',$id);
		$this->assign('detail',$detail);
		$this->assign('upanddown',$upanddown);
		$this->display('news_detail');
	}
}