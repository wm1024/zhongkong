<?php
class newsMod extends commonMod{

	public function index(){

		$act = addslashes($_GET['act']);
		if ($act == 'search'){
			$keyword = addslashes($_REQUEST['keyword']);
 			$this->assign('keyword', $keyword);
		}

		// 列表信息
        $listRows = 7;
        $page = intval($_GET['page']);
        $page = empty($page) ? 1:$page; if ($page<1) $page=1;
        $startRows = ($page-1)*$listRows;
		
		$where = "  `show`=1 and `recover`=0 ";
		
		if (! empty($keyword)){
			$where .= " and `title` like '%$keyword%' ";
			$url = '-act-search-keyword-'.$keyword;
		}


        $totalRows = $this->model->table('news')->where($where)->count();
        $list_arr  = $this->model->table('news')->where($where)->limit("$startRows,$listRows")->order('sequence desc,id desc')->select();
        $page_str  = $this->page('/news/index-page-{page}'.$url.'.html', $totalRows,$listRows);
        
        $this->assign('list_arr', $list_arr);
		$this->assign('page_str', $page_str);


		$this->display('news');
	}
	
	public function detail(){
		$id = intval($_GET['id']);
		if (empty($id)) exit('参数有误');
		
		
		$arr = __query("select * from {tabpre}news where id=".$id);
		$cid = $arr[0]['classid'];
		
		// 上一篇
		$prev_arr = __query("select * from {tabpre}news where id>$id and classid=$cid and `show`=1 and `recover`=0 order by id asc limit 1");
		$this->assign('prev_arr', $prev_arr[0]);
		
		// 下一篇
		$next_arr = __query("select * from {tabpre}news where id<$id and classid=$cid and `show`=1 and `recover`=0 order by id desc limit 1");
		$this->assign('next_arr', $next_arr[0]);


		$this->assign('id', $id);
		$this->assign('cid', $cid);
		$this->assign('det', $arr[0]);
		$this->display('news_detail');
	}
}