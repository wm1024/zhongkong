<?php
class studioMod extends commonMod{

	public function index(){

		$cid = intval($_GET['cid']);
		if (empty($cid)) $cid = 1;
		$this->assign('cid', $cid);

		// 列表信息
        $listRows = 9;
        $page = intval($_GET['page']);
        $page = empty($page) ? 1:$page; if ($page<1) $page=1;
        $startRows = ($page-1)*$listRows;
		
		$where = " classid2=$cid and `show`=1 and `recover`=0 ";
		
        $totalRows = $this->model->table('live')->where($where)->count();
        $list_arr  = $this->model->table('live')->where($where)->limit("$startRows,$listRows")->order('sequence desc,id desc')->select();
        $page_str  = $this->page('/info/index-cid-'.$cid, $totalRows,$listRows);
        
        $this->assign('list_arr', $list_arr);
		$this->assign('page_str', $page_str);
		


		$this->display('studio');
	}
	public function detail(){

		$id = intval($_GET['id']);
		if (empty($id)) exit('参数有误');
		
		$arr = __query("select * from {tabpre}live where id=".$id);
		$cid = $arr[0]['classid'];
		
		$list = __query("select * from {tabpre}live where id<>$id and classid=$cid and `show`=1 and `recover`=0 order by hits desc,id desc limit 0,3");
		$this->assign('list', $list);

		$this->assign('id', $id);
		$this->assign('cid', $cid);
		$this->assign('det', $arr[0]);
		$this->display('studio_detail');
	}
}