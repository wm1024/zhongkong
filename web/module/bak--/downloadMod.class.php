<?php
class downloadMod extends commonMod{

	public function index(){

		$arr = __query("select * from {tabpre}down where `show`=1 and `recover`=0 order by id desc limit 0,8");
		$this->assign('list', $arr);

	
		$this->display('download');
	}
	public function ajax(){

		// 列表信息
        $listRows = 8;
        $page = intval($_GET['page']);
        $page = empty($page) ? 1:$page; if ($page<1) $page=1;
		$page++;
        $startRows = ($page-1)*$listRows;
		
		$where = " `show`=1 and `recover`=0 ";
        $list_arr  = $this->model->table('down')->where($where)->limit("$startRows,$listRows")->order('id desc')->select();
		if (! empty($list_arr)) {
			foreach($list_arr as $k=>$v) {
				
				$height = get_imgHeight(get_imgurl($v['pic']));
				
				$str .= '<div class="cell"><a href="/download/detail-cid-'.$v['id'].'.html"><img src="/'.get_imgurl($v['pic']).'" height="'.$height.'" /><div class="download_con">'.str_replace('1', '电视剧', str_replace('2', '电视剧', $v['classid'])).'：'.$v['title'].'</div></a></div>';
			}
		}
		$arr['all'] = $str;
		$json_str = json_encode($arr);
		exit($json_str);

	}
	public function detail(){

		$cid = intval($_GET['cid']);
		if (empty($cid)) exit('参数有误');

		// 影片详细
		$det = __query("select * from {tabpre}down where id=".$cid);
		$this->assign('det', $det);



		// 列表信息
        $listRows = 9;
        $page = intval($_GET['page']);
        $page = empty($page) ? 1:$page; if ($page<1) $page=1;
        $startRows = ($page-1)*$listRows;
		
		$where = " classid in ({$cid}".get_classsql($cid).") and `show`=1 and `recover`=0 ";
		
        $totalRows = $this->model->table('down2')->where($where)->count();
        $list_arr  = $this->model->table('down2')->where($where)->limit("$startRows,$listRows")->order('sequence desc,id desc')->select();
        $page_str  = $this->page('/info/index-cid-'.$cid, $totalRows,$listRows);
        
        $this->assign('list_arr', $list_arr);
		$this->assign('page_str', $page_str);

	
		$this->display('download_detail');
	}
	public function file(){
		// 下载图片

		$id = intval($_GET['id']);
		if (empty($id)) exit('参数有误');

		$det = __query("select * from {tabpre}down2 where id=".$id);
		$pic_url = '/'.get_imgurl($det[0]['pic']);
		$img_url = 'http://'.$_SERVER['SERVER_NAME'].$pic_url;


		header("Content-Type: application/force-download");//输出文件类型
		header("Content-Disposition: attachment; filename=".$det[0]['pic']);//输出文件为附件，输出文件名为name.csv

		if (img_exists($img_url)){
			$img = file_get_contents($img_url); 
			echo $img;
		}
	}
}