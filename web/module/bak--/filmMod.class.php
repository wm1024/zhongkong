<?php
class filmMod extends commonMod{

	public function index(){

		$type = intval($_GET['type']);
		$this->assign('type', $type);

		$year = intval($_GET['year']);
		$this->assign('year', $year);

		$keys = addslashes($_GET['keys']);
		$this->assign('keys', urldecode($keys));



		// 类型
		$class9991 = __query("select ID,ClassName from {tabpre}class where SystemID=9991 ");
		$this->assign('class9991', $class9991);
		// 年份
		$class9992 = __query("select ID,ClassName from {tabpre}class where SystemID=9992 ");
		$this->assign('class9992', $class9992);


		$cid = intval($_GET['cid']);
		if (empty($cid)) $cid = 1138;

		// 列表信息
        $listRows = 12;
        $page = intval($_GET['page']);
        $page = empty($page) ? 1:$page; if ($page<1) $page=1;
        $startRows = ($page-1)*$listRows;
		
		$where = " classid in ({$cid}".get_classsql($cid).") and `show`=1 and `recover`=0 ";

		if (! empty($type)){
			$where .= " and classid1=". $type;
		}
		if (! empty($year)){
			$where .= " and classid2=". $year;
		}
		if (! empty($keys)){
			$where .= " and title like '%".urldecode($keys)."%' ";
		}

        $totalRows = $this->model->table('tv')->where($where)->count();
        $list_arr  = $this->model->table('tv')->where($where)->limit("$startRows,$listRows")->order('sequence desc,id desc')->select();
        $page_str  = $this->page('/info/index-cid-'.$cid, $totalRows,$listRows);
        
        $this->assign('list_arr', $list_arr);
		$this->assign('page_str', $page_str);

	
		$this->display('film');
	}
	public function det(){
		
		$id = intval($_GET['id']);
		if (empty($id)) exit('参数有误');
		
		
		$arr = __query("select * from {tabpre}tv where classid=1138 and id=".$id);
		$cid = $arr[0]['classid'];
		
		// 上一篇
		$prev_arr = __query("select * from {tabpre}tv where id>$id and classid=$cid and `show`=1 and `recover`=0 order by id asc limit 1");
		$this->assign('prev_arr', $prev_arr[0]);
		
		// 下一篇
		$next_arr = __query("select * from {tabpre}tv where id<$id and classid=$cid and `show`=1 and `recover`=0 order by id desc limit 1");
		$this->assign('next_arr', $next_arr[0]);

		$this->assign('id', $id);
		$this->assign('cid', $cid);
		$this->assign('det', $arr[0]);

		$this->display('tv_det');
	}
}