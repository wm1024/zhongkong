<?php
class cooperationMod extends commonMod{

	public function index(){

		// 合作伙伴
		$link = __query("select * from {tabpre}ads where classid=1150 and `show`=1 and `recover`=0 ");
		$this->assign('link', $link);

		// 合作案例
		$coop = __query("select * from {tabpre}tv where rmd2=1 and `show`=1 and `recover`=0 ");
		$this->assign('coop', $coop);


		$this->assign('all_nav_2', true);
		$this->assign('class_name', '公司简介');
		$this->assign('about_nav_1', true);		
		$this->display('cooperation');
	}
}