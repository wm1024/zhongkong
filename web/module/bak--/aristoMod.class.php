<?php
class aristoMod extends commonMod{

	public function index(){

		$class145_arr = __query("select ID,ClassName,ClassInfo from {tabpre}class where `SystemID`=145 order by Sequence asc,id asc");
		

		$this->assign('class145_arr', $class145_arr);
		$this->display('aristo');
	}
	public function detail(){

		$id = intval($_GET['id']);
		if (empty($id)) exit('参数有误');
		
		
		$arr = __query("select * from {tabpre}aris where id=".$id);
		$cid = $arr[0]['classid'];
		
		// 上一篇
		$prev_arr = __query("select * from {tabpre}aris where id>$id and classid=$cid and `show`=1 and `recover`=0 order by id asc limit 1");
		$this->assign('prev_arr', $prev_arr[0]);
		
		// 下一篇
		$next_arr = __query("select * from {tabpre}aris where id<$id and classid=$cid and `show`=1 and `recover`=0 order by id desc limit 1");
		$this->assign('next_arr', $next_arr[0]);


		$this->assign('id', $id);
		$this->assign('cid', $cid);
		$this->assign('det', $arr[0]);
		$this->display('aristo_detail');
	}

}