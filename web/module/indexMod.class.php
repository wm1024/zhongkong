<?php
class indexMod extends commonMod{

	// 首页
	public function index(){

		// 新闻中心
		$banner = __query("select * from {tabpre}index where `classid`=1221 and `show`=1 and `recover`=0 order by sequence desc,id desc");
		$overview = __query("select * from {tabpre}overview where classid=1137 and `show`=1 and `recover`=0 order by sequence desc,id desc");
		$news_rmd2_arr = __query("select * from {tabpre}news where `rmd2`=1 and `show`=1 and `recover`=0 order by sequence desc,id desc limit 0,3");

		//数据获取
		$product_rmd2 = __query("select * from {tabpre}products where rmd2=1 and `show`=1 and `recover`=0 order by sequence desc,id asc");

		$this->assign('banner', $banner);
		$this->assign('overview', $overview);
		$this->assign('news_rmd2_arr', $news_rmd2_arr);
		$this->assign('product_rmd2', $product_rmd2);
		$this->display('index_index');
	}

	public function sitmap(){


		$this->display('index_sitmap');
	}
}