<?php
// ==================================================================
//
// 项目自定义函数部分
//
// ------------------------------------------------------------------


/**
 * 提示并跳转
 * @param mixed
 *      $txt    提示信息
 *      $link   跳转地址
 *      $type   类型
 *              1----提示并后退
 *              2----提示并跳转
 *              3----直接跳转，无提示
 * @return null
 * @autor yc
 * @creattime 2014-06-18
 */
function msg($txt='', $link='', $type=1) {
	echo "<script language=\"JavaScript\">";
	switch($type){
		case 1://提示并后退
			echo "alert(\"$txt\");";
			echo "history.back();";
			break;
		case 2://提示并跳转
			echo "alert(\"$txt\");";
			echo "location.replace(\"$link\")";
			break;
		case 3://直接跳转
			echo "location.replace(\"$link\")";
			break;
	}
	echo "</script>";
}


function selectList($table,$cid="",$start="",$pageNum="",$where=null,$ord=""){
	$sql = "SELECT * FROM {tabpre}{$table} WHERE `show`=1 AND `recover`=0 ";
	if(!empty($where)){
		$sql .= " AND ".$where;
	}
	if(!empty($cid)){
		$sql .= " AND `classid` = {$cid} ";
	}
	if($pageNum!=""){$dd = "LIMIT {$start},{$pageNum}";}
	if($ord!=""){$ord .= ",";}
	$sql .= " ORDER BY ".$ord." `rmd` DESC,`sequence` DESC,`datetime` DESC,`id` DESC ".$dd;
//	echo $sql;
	$result = __query($sql);
	return $result;
}

function selectList_in($table,$cid="",$start="",$pageNum="",$where=null,$ord=""){
	$sql = "SELECT * FROM {tabpre}{$table} WHERE `show`=1 AND `recover`=0 ";
	if(!empty($where)){
		$sql .= $where;
	}
	if(!empty($cid)){
		$sql .= " AND `classid` = {$cid} ";
	}
	if($pageNum!=""){$dd = "LIMIT {$start},{$pageNum}";}
	if($ord!=""){$ord .= ",";}
	$sql .= " ORDER BY ".$ord." `rmd` DESC,`sequence` DESC,`datetime` DESC,`id` DESC ".$dd;
//	echo $sql;
	$result = __query($sql);
	return $result;
}

function get_page_str($page,$pageSum,$href,$pagesPer=3){
	$str = '';
	$str .= '<a href="'.$href.'-page-1.html" style="margin-right:5px;"><<</a><a ';
	if($page==1){$str .= 'disabled="disabled"';}
	$str .= 'href="'.$href.'-page-'.($page>1?($page-1):1).'.html" style="margin-right:5px;"><</a>';
    if($page<=$pagesPer){
		for($p=1;$p<=$pagesPer;$p++){
			if($p<=$pageSum){
				$str .= '<a ';
				if($p==$page){$str .= 'class="on"';}
				$str .= ' href="'.$href.'-page-'.$p.'.html">'.$p.'</a>';
			}
		}
	}else{
		for($p=$page-floor($pagesPer/2);$p<=$page+floor($pagesPer/2);$p++){
			if($p<=$pageSum){
				$str .= '<a ';
				if($p==$page){$str .= 'class="on"';}
				$str .= ' href="'.$href.'-page-'.$p.'.html">'.$p.'</a>';
			}
		}
	}
	$str .= '<a ';
	if($page==$pageSum){$str .= 'disabled="disabled"';}
	$str .= 'href="'.$href.'-page-'.($page<$pageSum?($page+1):$pageSum).'.html" style="margin-right:5px;">></a>';
	$str .= '<a href="'.$href.'-page-'.$pageSum.'.html" style="margin-right:5px;">>></a>';
	return $str;
}

function get_nationId(){
	$obj = new getCountry();
	$nation = $obj->getMsg();
	$nationid = get_classid($nation);
	return $nationid;
}

//获取会员信息
function get_member_info(){
	$name = safe($_SESSION['name']);
	$password = safe($_SESSION['password']);
//	if(!$name || !$password){
//		echo '<script>alert("Illegally access！");location.href="/index/index.html";</script>';
//	}
	$rs = __query("select * from {tabpre}member where `show`=1 and `recover`=0 and (`name`='".$name."' or `email`='".$name."') and password='".$password."'","only");
	return $rs[0];
}

//获取格式化时间
function get_eng_date($time){
return substr(get_eng_month(substr($time,5,2)),0,3).' '. substr($time,8,2).', '.substr($time,0,4);//如 May.31,2016
}

//获取一级分类
function getClassList($systemid){
	$class_arr = __query("select * from {tabpre}class where SystemID=".$systemid." and depth=0 order by ID asc");
	return $class_arr;
}

function get_eng_month($key){
	$arr = array(
		'01' => 'January',
		'02' => 'February',
		'03' => 'March',
		'04' => 'April',
		'05' => 'May',
		'06' => 'June',
		'07' => 'July',
		'08' => 'August',
		'09' => 'September',
		'10' => 'October',
		'11' => 'November ',
		'12' => 'December',
	);
	return $arr[$key];
}


/**
 * 获取分类下级子分类列表（只有一级）
 * @parm:
 *      $classid 分类ID
 *      $isIncludeSelf 是否包含自身,0--不包含,1--包含
 * @return:$classlist = array();
 * @autor:yc
 * @creattime:2014-06-17
 */
function get_child_class($id,$isIncludeSelf=0) {
	if(is_numeric($id)){
		$sid_class      = array();
		$num            = 0;
		$where          = ($isIncludeSelf)?"partid='".$id."' or ID='".$id."'":"partid='".$id."'";
		$sql            = "SELECT * FROM `{tabpre}class` WHERE (".$where.") order by sequence asc";
		$mysql_query    = __query($sql);

		foreach ($mysql_query as $key=> $value) {
			$sid_class[$num]	= $value;
			$num++;
		}
		return $sid_class;
	}
}



function get_nav_news() {
	$arr = __query("select * from {tabpre}news where `rmd3`=1 and `show`=1 and `recover`=0 order by sequence desc,id desc limit 0,1");
	return $arr;
}


// 输入数组
function p($arr) {
	print_r($arr);
}

// 检测图片是否存在
function img_exists($url) 
{
    if(file_get_contents($url,0,null,0,1))
        return 1;
else
        return 0;
}

function get_footer_link(){
	// 友情链接
	$link = __query("select * from {tabpre}ads where classid='1151' and `show`=1 and `recover`=0 order by sequence desc,id desc");
	if (! empty($link)){
		foreach ($link as $k=> $v) {
			echo '<li><a href="'.$v['content'].'" target="_blank">'.$v['title'].'</a></li>';
		}
	}
}

function get_aristo_li($id){

	$arr = __query("select * from {tabpre}aris where classid=$id and `show`=1 and `recover`=0 ");
	if (! empty($arr)){
		$str = '';
		foreach ($arr as $k=>$v) {

			$str .= '<a onclick="popWin.showWin(\'980\',\'100%\',\'/aristo/detail-id-'.$v['id'].'.html\');" href="javascript:;" title="'.$v['title'].'" class="clearfix">
			<img src="/'. get_imgurl($v['pic']) .'" width="141" height="92" class="fl"/>
			<div class="aristo_right fr">
			<h1>'.str_len($v['title'],20).'</h1>
			<p>'.str_len(replace_content($v['content']), 56).'</p>
			</div></a>';
		}
	}

	return $str;

}

function get_imgHeight($imgurl)
{
	if (! empty($imgurl)) {
		$arr = getimagesize('http://'.$_SERVER['SERVER_NAME'].'/'.$imgurl);
		if (! empty($arr[0])) {
			
			$width = $arr[0];
			$height = $arr[1];
			$x = $width/290;
			$new_height = $height/$x;
			
			return $new_height;
		}
	}
}

function get_news_new()
{
	$arr = __query("select * from {tabpre}news where `show`=1 and `recover`=0 ");
	if (! empty($arr)) {
		$str = '';
		foreach($arr as $k=>$v){
			$str .= '<li><a href="/news/detail-id-'.$v['id'].'.html">'.str_len($v['title'],24).'</a></li>';
		}
	}
	return $str;
}


function get_index_ads($cid)
{
	$arr = __query("select * from {tabpre}info where classid='$cid' ");
	if (! empty($arr)) {
		$str .= '<dt><a href="'.$arr[0]['url'].'"><img src="'.get_imgurl($arr[0]['pic']).'" width="232" height="139" alt=""/></a></dt>
		<dd><a href="'.$arr[0]['url'].'">'.$arr[0]['content'].'</a></dd>';
	}
	return $str;
}

function get_prolist($cid)
{
	$arr = __query("select * from {tabpre}products where classid='$cid' and `show`=1 and `recover`=0 order by sequence desc,id desc limit 0,4");
	if (! empty($arr)) {
		foreach($arr as $k=>$v){
			$str .= '<li><a href="product/detail-id-'.$v['id'].'.html">'.str_len($v['title'],12).'</a></li>';
		}
	}
	return $str;
}

function get_history($cid)
{
	$arr = __query("select * from {tabpre}course where classid='$cid' ");
	if (! empty($arr)) {
		foreach ($arr as $k => $v) {
            $str .= '<dd class="clearfix"><h3 class="fl">'.$v['title'].'</h3><div class="fr">';
            if (! empty($v['content'])) {
                $textarr = explode('‖', $v['content']);
                foreach ($textarr as $kk => $vv) {
                    $str .= '<div class="clearfix"><p>'.$vv.'</p></div>';
                }
            }
            $str .= '</div></dd>';
		}
	}

	return $str;
}

function base_links()
{
	$arr = __query("select * from {tabpre}links where title<>'' order by sequence desc, id desc");
	if (! empty($arr)){
		foreach ($arr as $k => $v) {
			$str .= '<li><a href="'.$v['content'].'" target="_blank">'.$v['title'].'</a></li>';
		}
	}

	return $str;
}

function get_base($k){
	//获取
	$arr = __query("select * from {tabpre}config ");
	if (!empty($arr[0][$k])){
		return $arr[0][$k];
	}
}

function get_base_union($k){
	//获取-联盟网站head信息
	$arr = __query("select * from {tabpre}config ");
	if (!empty($arr[0][$k])){
		return $arr[0][$k];
	}
}

function get_left_product(){
	$cid = intval($_GET['cid']);
	if (! empty($cid)){
		
		$top_id = top_partid($cid);//父ID
		
		
		$arr = __query("select * from {tabpre}class where partid='".$top_id."' order by ycpaixu asc,id asc");
		foreach($arr as $k=>$v){
			if ($v['ID']==$cid || is_ss($v['ID'],$cid)){ $ss = 'class="left_nava"'; $ssa=' style="display:block;"'; }else{ $ss = '';$ssa=''; }
			
			$arr2 = __query("select * from {tabpre}class where partid='".$v['ID']."' order by ycpaixu asc,id asc");
			if (! empty($arr2)){
				//有三级子分类
				echo '<li '.$ss.'><a href="javascript:void;" title="'.$v['ClassName'].'">'.$v['ClassName'].'</a>';	
				echo '<dl '.$ssa.'>';
				foreach ($arr2 as $kk=>$vv) {
					if ($vv['ID'] == $cid){ $ss_1 = 'class="left_navcur"'; }else{ $ss_1 = ''; }
					echo '<dd '.$ss_1.'><a href="product/index-cid-'.$vv['ID'].'.html" title="'.$vv['ClassName'].'">&bull;&nbsp;&nbsp;&nbsp;'.$vv['ClassName'].'</a></dd>';
				} 
				echo '</dl>';
			}else{
				//无子分类
				echo '<li '.$ss.'><a href="product/index-cid-'.$v['ID'].'.html" title="'.$v['ClassName'].'">'.$v['ClassName'].'</a>';	
			}
			echo '</li>';
		}
		
		
			
	}	
}

function is_ss($id, $cid){
	$arr = __query("select * from {tabpre}class where partid='".$id."' ");
	if (!empty($arr)){
		foreach ($arr as $k=>$v) {
			if ($v['ID']==$cid){ return true; }
		} 
	}
}

function get_left_environ($eid = 0){
	$arr = __query("select * from {tabpre}news2 where `recover`=0 and title<>'' order by sequence desc,id desc");
	foreach ($arr as &$v) {
		if ($v['id'] == $eid){ $ss = 'class="left_navcur"'; }else{ $ss = ''; }
		$str .= '<dd '.$ss.'><a href="about/environ-id-'. $v['id'] .'.html" title="'. $v['title'] .'">&bull;&nbsp;&nbsp;&nbsp;'. $v['title'] .'</a></dd>';
	}
	return $str;
}

function get_updownpage($sql, $id){
	if (empty($sql) or empty($id)) exit('参数不能为空');

	$current_id_k = 0;
	$arr = __query($sql);
	foreach ($arr as $k=>$v) {
		if ($v['id']==$id){
			$current_id_k = $k;
			break;
		}
	}

	//上一篇
	if (! empty($arr[$current_id_k-1])){
		$page_arr['up_str'] = $arr[$current_id_k-1];	
	}
	//下一篇
	if (! empty($arr[$current_id_k+1])){
		$page_arr['down_str'] = $arr[$current_id_k+1];
	}

	return $page_arr;
}

function get_baby_name($id){
	if (!empty($id)){
		$arr = __query("select baby_id from {tabpre}member_parent where member_id='".$id."' ");
		if (!empty($arr[0])){
			$user = __query("select name from {tabpre}member_baby where id='".$arr[0]['baby_id']."' ");
			return $user[0]['name'];
		}
	}
}

function get_msgcontent($id){
	if (!empty($id)){
		$arr = __query("select content from {tabpre}msg_content where id='".$id."' ");
		return $arr[0]['content'];
	}
}

function get_member_name($id){
	if (!empty($id)){

		$arr = __query("select type from {tabpre}member where id='".$id."' ");
		//获取数据
		if ($arr[0]['type']=='0' or $arr[0]['type']=='1' or $arr[0]['type']=='2')
		{
			$name_arr = __query("select name from {tabpre}member_basis  where member_id='".$id."' ");
		}else{
			$name_arr = __query("select name from {tabpre}member_parent where member_id='".$id."' ");
		}
		//返回名字
		if (!empty($name_arr[0]))
		{
			return $name_arr[0]['name'];
		}else{
			return;
		}
	}
}

function get_member_name_arr($str){
	if (!empty($str) ) {
		$arr = __query("select id,type from {tabpre}member where id in (".$str.") ");
		foreach ($arr as $k=>$v) {
			//获取数据
			if ($v['type']=='0' or $v['type']=='1' or $v['type']=='2')
			{
				$name_arr = __query("select name from {tabpre}member_basis  where member_id='".$v['id']."' ");
				$sstr .= $name_arr[0]['name'].'、';
			} else {
				$name_arr = __query("select name from {tabpre}member_parent where member_id='".$v['id']."' ");
				$sstr .= $name_arr[0]['name'].'、';
			}
		}
		//返回名字组
		if (!empty($sstr))
		{
			return substr($sstr, 0, -3);
		} else {
			return;
		}
	}
}

function get_member_imgs($id){
	if (!empty($id)){

		$arr = __query("select type from {tabpre}member where id='".$id."' ");
		//获取数据
		if ($arr[0]['type']=='0' or $arr[0]['type']=='1' or $arr[0]['type']=='2')
		{
			$name_arr = __query("select headpic from {tabpre}member_basis  where member_id='".$id."' ");
		}else{
			$name_arr = __query("select headpic from {tabpre}member_parent where member_id='".$id."' ");
		}

		//返回名字
		if (!empty($name_arr[0]['headpic']))
		{
			return $name_arr[0]['headpic'];
		}else{
			return 'public/images/default_portrait.jpg';
			exit;
		}
	}
}

//数据过滤
function safe($str){
	if (is_array($str)){
		foreach ($str as $k=>$v) {
			$data[$k] = safe($v);
		}
		return $data;
	}else{
		return in(html_in($str));
	}
}
//解决substr截取乱码函数
function str_len($str,$strlen)
{
	if(empty($str)||!is_numeric($strlen)) {return false;}
	//echo utf8_strlen(strip_tags($str));
	if(utf8_strlen(strip_tags($str))<=$strlen)
	{
		return strip_tags($str);
	}else{
		return mb_substr(strip_tags($str),0,$strlen,'utf-8')."...";
	}
}
function utf8_strlen($string = null) {
	preg_match_all("/./us", $string, $match);
	return count($match[0]);
}

//去掉内容中的多余HTML代码
function replace_content($str){
	$search = array ("'<script[^>]*?>.*?</script>'si", // 去掉 javascript
	"'<style[^>]*?>.*?</style>'si", // 去掉 css
	"'<[/!]*?[^<>]*?>'si", // 去掉 HTML 标记
	"'<!--[/!]*?[^<>]*?>'si", // 去掉 注释标记
	"'([rn])[s]+'", // 去掉空白字符
	"'&(quot|#34);'i", // 替换 HTML 实体
	"'&(amp|#38);'i",
	"'&(lt|#60);'i",
	"'&(gt|#62);'i",
	"'&(nbsp|#160);'i",
	"'&(iexcl|#161);'i",
	"'&(cent|#162);'i",
	"'&(pound|#163);'i",
	"'&(copy|#169);'i",
	"'&#(d+);'e"); // 作为 PHP 代码运行
	$replace = array ("","","","","\1","\"","&","<",">"," ",chr(161),chr(162),chr(163),chr(169),"chr(\1)");
	//$document为需要处理字符串，如果来源为文件可以$document = file_get_contents('http://www.sina.com.cn');
	$out = preg_replace($search, $replace, $str);
	return $out;
	//echo $out;
}

// 执行SQL语句
function __query($sql) {
if (! empty($sql))
{
	$sql = strtolower($sql);
	global $config;
	$cModel = commonMod::initModel( $config );
	$sql = str_replace('{tabpre}', $cModel->pre, $sql);
	$arr = $cModel->query($sql);
	if (strpos($sql,'insert') !== false) $arr = $cModel->db->lastId();//返回ID
	if (strpos($sql,'update') !== false) $arr = $cModel->db->affectedRows();//影响行数
	if (empty($arr)) $arr=array();
	return $arr;
}
}

// 提示信息
function __alert($msg, $url = NULL){
	header("Content-type: text/html; charset=utf-8");
	$alert_msg="alert('$msg');";
	if( empty($url) ) {
		$gourl = 'history.go(-1);';
	}else{
		$gourl = "window.location.href = '{$url}'";
	}
	echo "<script>$alert_msg $gourl</script>";
	exit;
}

// 取得图片地址
function get_imgurl($str){
	if (empty($str)){ return 'public/images/default_news_imgs.jpg'; }
	if (strpos('-'.$str, 'upload')>0)
	{
		$str = ''.$str;
	}else{
		$str = 'upload/image/'.substr($str,0,7).'/'.$str;
	}
	return $str;
}

// 图片上传
function c_upfile($file, $isThumb=0, $isWatermark=0, $schoolName = '')
{
	//图片上传错误
	if($file['error'] > 0){
	   switch($file['error'])
	   {
	     case 1: return 'error:文件大小超过服务器限制';
	             break;
	     case 2: return 'error:文件太大！';
	             break;
	     case 3: return 'error:文件只加载了一部分！';
	             break;
	     case 4: return 'error:文件加载失败！';
	             break;
	   }
	   exit;
	}
	if($file['size'] > 1000000){
		return 'error:文件过大！';
		exit;
	}
	if($file['type']!='image/jpeg' && $file['type']!='image/gif'){
		return 'error:文件不是JPG或者GIF图片！';
		exit;
	}
	$today = date("YmdHis").rand(100000,999999);
	$filetype = $file['type'];
	if($filetype == 'image/jpeg'){ $type = '.jpg'; }
	if($filetype == 'image/gif') { $type = '.gif'; }
	$updir = 'upload/user_img/image/'.date('Ymd').'/';
	$upfile = $updir . $today.$type;//文件上传地址
    if(!is_dir($updir)) { @mkdir($updir, 0777, true); }
    if(is_uploaded_file($file['tmp_name']))
    {
        if($isThumb){//需要处理缩略图
            $image = new Image();
            $thumbImageName = $image->thumb($file['tmp_name'],'headpic_'.$file['name'],$updir);
            $upfile = $updir.$thumbImageName;
        }elseif($isWatermark){
            $image = new Image();
            $image->textWater($file['tmp_name'],$schoolName);
            $image->textWater($file['tmp_name'],'www.embaobao.com','255','250','30');
            move_uploaded_file($file['tmp_name'], $upfile);
        }elseif(!move_uploaded_file($file['tmp_name'], $upfile)){
            return 'error:移动文件失败';
            exit;
        }
    }else{
        return 'error:0';
        exit;
    }

	return 'success:'.$upfile;
}



//取得当前位置
function get_location($id) {
if (is_numeric($id)) {

	$arr = __query('select id,ClassName,partid from `{tabpre}class` where id='.$id);
	if (! empty($arr))
	{
		$stra = '<a href="#">'.$arr[0]['ClassName'].'</a> <i></i> ';
		$partid = $arr[0]['partid'];
		if ($partid==0)
		{
			return $stra;
		}else{
			return $str = get_location($partid).$stra;
		}
	}
}
}

//取得分类名称
function get_classname($id) {
if (is_numeric($id)) {
	$arr = __query('select id,classname from `{tabpre}class` where id='.$id);
	//print_r($arr);
	if (! empty($arr)) return $arr[0]['classname'];
}
}

//取得分类ID
function get_classid($name) {
	if (!empty($name)) {
		$arr = __query('select id,classname from `{tabpre}class` where classname="'.$name.'"');
		//print_r($arr);
		if (! empty($arr)) return $arr[0]['id'];
	}
}

/**
 * 获取表下所有一级分类列表
 * @parm:$classid,$depth
 * @return:$classlist = array();
 * @autor:yc
 * @creattime:2014-06-17
 */
function getClass($sid,$depth=2) {
	if(is_numeric($sid)){
		$classlist      = array();
		$sql            = "SELECT * FROM `{tabpre}class` WHERE SystemID=".$sid." AND depth=".$depth." order by sequence asc";
		$mysql_query    = __query($sql);
		foreach ($mysql_query as $key=> $value) {
			$classlist[] = $value;
		}
		return $classlist;
	}
}

//取得分类父ID
function get_partid($id) {
if (is_numeric($id)) {
	$arr = __query('select id,partid from `{tabpre}class` where id='.$id);
	if (! empty($arr)) return $arr[0]['partid'];
}
}

//取得分类老祖ID
function top_partid($id) {
if (is_numeric($id)) {
	$arr = __query('select id,partid from `{tabpre}class` where id='.$id);
	if (! empty($arr))
	{
		$partid = $arr[0]['partid'];
		if ($partid==0) { return $arr[0]['id']; }else{ return top_partid($partid); }
	}
}
}

// 取得会员名称
function get_membername($uid)
{
	if (!empty($uid))
	{
		if ( $user[0]['type']==0 or $user[0]['type']==1 or $user[0]['type']==2 ){
			$arr = __query("select b.name from {tabpre}member as a JOIN {tabpre}member_basis as b ON a.id=b.member_id where a.id=".$uid);
		}else{
			$arr = __query("select b.name from {tabpre}member as a JOIN {tabpre}member_parent as b ON a.id=b.member_id where a.id=".$uid);
		}

		return $arr[0]['name'];
	}
}

function get_classsql($id)
//生成当前分类下所有子类ID
{
if (is_numeric($id)) {

	$querys=__query("select ID from {tabpre}class where partid='".$id."'");
	foreach ($querys as $key => $value) {
		$arrayin .= ",".$value['id'];
		$xj_arrayin = get_classsql($value['id']);//查看是否有下级
		if ($xj_arrayin!="") { $arrayin .= ",".$xj_arrayin; }
	}

	return str_replace(',,', ',', $arrayin);
}
}


// 图片上传
function cc_upfile($file)
{
	//图片上传错误
	if($file['error'] > 0){
	   switch($file['error'])
	   {
	     case 1: return 'error:文件大小超过服务器限制';
	             break;
	     case 2: return 'error:文件太大！';
	             break;
	     case 3: return 'error:文件只加载了一部分！';
	             break;
	     case 4: return 'error:文件加载失败！';
	             break;
	   }
	   exit;
	}
	if($file['size'] > 1000000){
		return 'error:文件过大！';
		exit;
	}
	if($file['type']!='application/msword'){
		return 'error:上传的不是word文件！';
		exit;
	}
	$today = date("YmdHis").rand(100000,999999);
	$filetype = $file['type'];
	
	if($filetype == 'application/msword'){ $type = '.doc'; }
	
	$updir = 'upload/user_file/'.date('Ymd').'/';
	$upfile = $updir.$today.$type;//文件上传地址
    if(!is_dir($updir)) { @mkdir($updir, 0777, true); }
    if(is_uploaded_file($file['tmp_name'])) {
    
        move_uploaded_file($file['tmp_name'], $upfile);

    }else{
        return 'error:0';
        exit;
    }

	return 'success:'.$upfile;
}
