$.fn.mag =function(can){
	can = $.extend({
	box:".small",//小图框架
	box_w:450,//小图宽
	box_h:450,//小图高
	box2:".big",//大图框架
	box2_w:1920,//大图宽
	box2_h:1080,//大图高
	range:".range",//放大镜框架
	range_w:100,//小图放大镜范围宽度
	range_h:100//小图放大镜范围高度
	}, can || {});
	var prop = can.box2_w / can.box_w;
	//比例
	var l_small = $(can.box).offset().left;//小图距离浏览器左边距离
	var t_small = $(can.box).offset().top;//小图距离浏览器顶部距离
	$(can.range).width(can.range_w).height(can.range_h);//放大镜范围
	$(can.box).width(can.box_w).height(can.box_h);//小图框架大小
	$(can.box).find('img').width(can.box_w).height(can.box_h);//小图大小
	$(can.box2).width(can.range_w*prop).height(can.range_h*prop);//大图框架
	$(can.box2).find('img').width(can.box2_w).height(can.box2_h);//大图大小

	// 获取鼠标在小图中的坐标
	$(this).hover(function(){
		$(can.range).show();
		$(can.box2).show();
	},function(){
		$(can.range).hide();
		$(can.box2).hide();
		});
	$(this).mousemove(function(e){
		var x_zb = parseInt(e.pageX)-l_small;//距离左边距离
		var y_zb = parseInt(e.pageY)-t_small;//距离顶部距离

		if(x_zb<=(can.range_w/2)){
			$(can.range).css('left','0');
			$(can.box2).find('img').css('left','0');
		}else if(can.box_w-x_zb<=(can.range_w/2)){
			$(can.range).css('left',can.box_w-can.range_w);
			$(can.box2).find('img').css('left',-(can.box2_w-can.range_w*prop));
		}else{
			$(can.range).css('left',x_zb-(can.range_w/2));
			$(can.box2).find('img').css('left',-(x_zb-can.range_w/2)*prop);
		}//放大镜在小图中的横向位移,大图横向位移

		if(y_zb<=(can.range_h/2)){
			$(can.range).css('top','0');
			$(can.box2).find('img').css('top','0');
		}else if(can.box_h-y_zb<=(can.range_h/2)){
			$(can.range).css('top',can.box_h-can.range_h);
			$(can.box2).find('img').css('top',-(can.box2_h-can.range_h*prop));
		}else{
			$(can.range).css('top',y_zb-(can.range_h/2));
			$(can.box2).find('img').css('top',-(y_zb-can.range_h/2)*prop);
		}//放大镜在小图中的纵向位移，大图纵向位移
	});
};