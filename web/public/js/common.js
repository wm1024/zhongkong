$(function(){
	//产品列表划过
	$('.explore_pro li').hover(function(){
		$('.main',this).css('top','auto');
		$('.main',this).stop(true,true).animate({'bottom':0});
	},function(){
		
		$('.main',this).stop(true,true).animate({'top':149});
		$('.main',this).css('bottom','auto');
	});
	
	$('.explore_nav li').click(function(){
		$(this).addClass('on').siblings().removeClass('on');
		var i=$(this).index();
		$('.explore_main .path').eq(i).fadeIn().siblings().hide();
	});
	//产品
	if($('.pro_nav a').length>0){
		$('.pro_nav a:last').css('margin',0);
	}
	$('.proDet_prev,.proDet_next').hover(function(){
		$(this).stop(true,true).animate({'width':259})
		.find('i').css('background-color','#84c4fd').stop(true,true).animate({'width':130})
		.parent().siblings('.con').stop(true,true).slideDown();
	},function(){
		$(this).stop(true,true).animate({'width':60})
		.find('i').css('background-color','#999').stop(true,true).animate({'width':60})
		.parent().siblings('.con').stop(true,true).slideUp();
	});
	
});
//首页banner轮播
jQuery(".banner").slide({titCell:".hd ul",mainCell:".bd ul",autoPage:true,effect:"leftLoop",autoPlay:true,scroll:1,vis:1});


$(".ind_news_more a").hover(function(){
	
	$(this).find(".ind_more_icon").stop().animate({"right":15+"px"});
},function(){

	$(this).find(".ind_more_icon").stop().animate({"right":25+"px"});
});

$(".ind_product_more a").hover(function(){
	
	$(this).find(".ind_more_icon").stop().animate({"right":9+"px"});
},function(){

	$(this).find(".ind_more_icon").stop().animate({"right":19+"px"});

});

$(".need_number_tel a").hover(function(){
	
	$(this).find(".tel_more").stop().animate({"right":15+"px"});	
},function(){

	$(this).find(".tel_more").stop().animate({"right":25+"px"});	
});


//首页悬浮头部
$(window).scroll(function(){
	
	var topheight=$(window).scrollTop();	
	if(topheight>=160){
		$('.head_fixed').show();	
	}
	else{
		$('.head_fixed').hide();
	}
});

//首页定位

$('.ind_position').delegate('li', 'click', function (){
	//$(this).addClass('on').siblings().removeClass('on');
	var ddIndex = $(this).find('a').stop().attr('href').lastIndexOf('#');
	var ddId = $(this).find('a').stop().attr('href').substring(ddIndex+1);
	var windowTop = $('a.anchor[name="' + ddId + '"]').offset().top-68;
	$('body,html').animate({scrollTop: windowTop}, 'slow');
});



//点击menu
$(".head_menu,.ind_head_menu").click(function(){

	$(".mainbody").stop().animate({"marginLeft":-300+"px"});
	$(".head_fixed").stop().animate({"left":-300+"px"});	
	$(".head_fixed_erify").animate({"right":0});
});

//menu 二级展开
$(".head_fixed_erify .fixed_tabs").click(function(){
	
	$(this).siblings(".fixed_sub_menu").stop().slideToggle();
	$(this).parent().toggleClass("on").siblings().removeClass("on");
	$(this).parent().siblings().find(".fixed_sub_menu").stop().slideUp();
	
});


//关闭menu
$(".head_fixed_erify ul li.close").click(function(){
		
	$(".mainbody").stop().animate({"marginLeft":0});
	$(".head_fixed").stop().animate({"left":0});	
	$(".head_fixed_erify").animate({"right":-300+"px"});	
	
});

//内页头部下拉
$(".head_sub_nav ul li").hover(function(){

	var index=$(this).index();
	if(index==0){
		
		$(".header_nav_erify").stop(true,true).slideDown();
		$(".header_nav_erify").find(".nav_erify_list").eq(0).show().siblings().hide();
		$(".head_erify_list").find(".head_erify_right_list").eq(0).show().siblings.show();		
	}
	else if(index==1){
		$(".header_nav_erify").stop(true,true).slideDown();
		$(".header_nav_erify").find(".nav_erify_list").eq(1).show().siblings().hide();	
	}
	else if(index==3){
		$(".header_nav_erify").stop(true,true).slideDown();
		$(".header_nav_erify").find(".nav_erify_list").eq(2).show().siblings().hide();	
	}
	else if(index==6){
		$(".header_nav_erify").stop(true,true).slideDown();
		$(".header_nav_erify").find(".nav_erify_list").eq(3).show().siblings().hide();	
	}
	else{
		$(".header_nav_erify").stop(true,true).slideUp();		
	}

});

$(".header_nav_erify").mouseleave(function(){
	
	$(this).stop().slideUp();
});

//头部产品列表切换
$(".head_product_nav ul li").hover(function(){
	
	var index=$(this).index();
	$(".head_product_tabs").find(".head_erify_right_list").eq(index).show().siblings().hide();
});





//点击搜索
$(".head_search").click(function(){
	
	$(this).addClass("on");
	$(".head_search_word").stop().slideDown();
});

$(".head_search_close").click(function(){
	
	$(".head_search_word").stop().slideUp();
});

//加入书签
function join_favorite(sTitle,sURL)
{
	try
	{
		window.external.addFavorite(sURL, sTitle);
	}
	catch (e)
	{
	try
	{
		window.sidebar.addPanel(sTitle, sURL, "");
	}
	catch (e)
	{
		alert("加入收藏失败，请使用Ctrl+D进行添加");
	}
	}
}




//首页产品轮播
jQuery(".ind_product_sildeBox").slide({titCell:".hd ul",mainCell:".bd ul",autoPage:true,effect:"leftLoop",autoPlay:true,scroll:1,vis:1});


//首页视频
function openVideo(){
	$(".ind_video").show();
}

//导航视频
function opennavVideo(){

	$(".nav_video").show();
}



$(".ind_video_close").click(function(){
	CKobject.getObjectById('ckplayer_vedio').videoPause();
	$(".ind_video").hide();
});

function navcloseVideo(){
	CKobject.getObjectById('ckplayer_vedio').videoPause();
	$(".nav_video").hide();
}


//产品轮播按钮居中

var product_length=$(".ind_product_sildeBox .hd ul li").length;
$(".ind_product_sildeBox .hd").width(product_length*26);
$(".ind_product_sildeBox .hd").css({"right":(270-product_length*26)/2+"px"})


//底部百度分享插件
//window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)]; 
 
//底部友情链接
$(".foot_right_nav").click(function(){
	
	$(this).find("ul").stop().slideToggle();
});

$(".foot_right_nav").mouseleave(function(){
	
	$(this).find("ul").stop().slideUp();
});

//底部打印
function pagePrint(){
	
	window.print();
}


//发展历程内容上下居中
$(".milestone_list ul li").each(function() {
    
	var index=$(this).index();
	if(index>0){
		
		var height=$(this).find(".milestone_info").height();
		$(this).find(".milestone_info").css({"top":-(height+18)/2+"px"});
		$(this).find(".milestone_arrow").css({"top":(height+14)/2+"px"});
	}
});

//简历弹窗
function applyNow(obj){

	var position=$(obj).parent().siblings(".job_det_position").text();
	$("#applay_position").val(position);
	$(".applay_pop").show();
	
}

function closeApplay(){

	$(".applay_pop").hide();	
}

function addDocname(obj){

	var file_name=$(obj).val();
	$("#applay_docname").val(file_name);
}

//荣誉弹窗
function openHonor(obj){

	var src=$(obj).find(".honor_pic").attr("rel");
	var name=$(obj).find(".honor_txt").text();
	$(".honor_pop_pic img").attr("src",src);
	$(".honr_pop_txt").text(name);
	$(".honor_pop").show();
}

$(".honor_pop_close").click(function(){
	
	$(".honor_pop").hide();
});

//资源鼠标划过

$(".resource_list ul li").hover(function(){
    
	$(this).find(".resource_type").stop().animate({"paddingTop":50+"px"});
},function(){

	$(this).find(".resource_type").stop().animate({"paddingTop":90+"px"});
});

//select下拉
$('body').on('click','.select p',function(){
	$(this).siblings('ul').slideDown(200);
});
$('body').on('click','.select li',function(){
	var text=$(this).text();
	$(this).parent().slideUp(200).siblings('p').text(text).siblings('input').val(text);
});
$('body').on('mouseleave','.select ul',function(){
	$(this).slideUp(200);
});

//资源列表最后2个不要border
$(".resource_sub_list ul li").each(function() {
   
   if(($(this).index()==2)||($(this).index()==3)){
	  $(this).css({"border":"none"}); 
	  } 
});

//视频列表最后3个不要border
$(".video_list ul li").each(function() {
   
   if(($(this).index()==3)||($(this).index()==4)||($(this).index()==5)){
	  $(this).css({"border":"none"}); 
	  } 
});

//搜索结果tab切换
$(".search_tabs .search_result_list").eq(0).show().siblings().hide();
$(".search_nav ul li").click(function(){
	
	var index=$(this).index();
	$(this).addClass("on").siblings().removeClass("on");	
	$(this).parents(".search_nav").siblings(".search_tabs").children().eq(index).show().siblings().hide();
});

//招聘列表
$(function(){
	var job_num=$(".job_list ul li").length;
	if(job_num==1){
		$(".job_list").css({"border":"none"});
		$(".job_list ul li").css({"border":"1px solid #e6e8e9"});
	}	
});



//登录弹窗
function openLogin(){

	$(".login_pop").show();	
}

$(".login_close").click(function(){

	$(".login_pop").hide();
	$(".login_main").show();
	$(".forget_main").hide();	
	$(".sign_main").hide();
});

//忘记密码
function showForget(){

	$(".login_main").hide().siblings(".forget_main").show();
}

//注册
function showSign(){

	$(".login_main").hide().siblings(".sign_main").show();		
}

function showChange(){

	$(".login_pop").show();	
	$(".login_main").hide();
	$(".forget_main").show();		
}

//高级搜索全选
function ChkAllClick(sonName, cbAllId){
   var arrSon = $('.'+sonName);
   var cbAll = document.getElementById(cbAllId);
   var tempState=cbAll.checked;
   for(i=0;i<arrSon.length;i++) {
       if(arrSon[i].checked!=tempState )
           arrSon[i].click();
	
   }
};

// --子项复选框被单击---
function ChkSonClick(sonName, cbAllId) {
   
   var arrSon = $('.'+sonName);
   var cbAll = document.getElementById(cbAllId);
   for(var i=0; i<arrSon.length; i++) {
       if(!arrSon[i].checked) {		
          cbAll.checked = false;
		  $('#'+cbAllId).next('label').removeClass('checked');
          return;
		  
       }
   }
   cbAll.checked = true;
//   $('#'+cbAllId).next('label').addClass('checked');
};

//合作伙伴轮播
jQuery(".parner_sildeBox").slide({titCell:".hd ul",mainCell:".bd .ulWrap",autoPage:true,effect:"top",autoPlay:true});

//解决方案鼠标滑过效果
$(".indust_list li").hover(function(){
	
	$(this).find(".indust_pic img").stop().animate({"marginTop":-81+"px"});
},function(){

	$(this).find(".indust_pic img").stop().animate({"marginTop":0});	
});

//解决方案详细轮播
jQuery(".indust_ccr_sideBox").slide({titCell:".hd ul",mainCell:".bd .ulWrap",autoPage:true,effect:"leftLoop",autoPlay:true});
jQuery(".indust_logo_sideBox").slide({titCell:".hd ul",mainCell:".bd .ulWrap",autoPage:true,effect:"leftLoop",autoPlay:true});

var crr_length=$(".indust_ccr_sideBox .hd ul li").length;
$(".indust_ccr_sideBox .hd ul").width(crr_length*22);

var crr_length=$(".indust_logo_sideBox .hd ul li").length;
$(".indust_logo_sideBox .hd ul").width(crr_length*22);

var partner_length=$(".parner_sildeBox .hd ul li").length;
$(".parner_sildeBox .hd ul").width(partner_length*22);


//点击左右箭头切换
var indust_width=$(window).width();
var indust_num=$(".indust_nav .indust_nav_list").length;
$(".indust_nav").width(indust_num*indust_width);
$(".indust_nav_list").width(indust_width);

$(".indust_button .next").click(function(){
	var cur_index=$(this).attr("rel");
	cur_index++;
	if(cur_index<indust_num){
		
		$(".indust_nav").stop().animate({"marginLeft":-(cur_index)*indust_width+"px"});
		$(this).parent().find("a").attr("rel",cur_index);
	}
});

$(".indust_button .prev").click(function(){
	
	var cur_index=$(this).attr("rel");
	cur_index--;
	if(cur_index>=0){
		
		$(".indust_nav").stop().animate({"marginLeft":-(cur_index)*indust_width+"px"});
		$(this).parent().find("a").attr("rel",cur_index);
	}
});

//解决方案文字上下切换
$(function(){
	
	var bestway_left=$(".bestway_left_list").height();	
	var bestway_left_length=Math.ceil(bestway_left/350); 
	var def_position=0;

	
	$(".bestway_left_bot .bestway_up").click(function(){
		
		if(def_position>0){
			
			$(".bestway_left_list").stop().animate({"marginTop":-(def_position-1)*350+"px"});
			def_position--;
		}
		
	});
	$(".bestway_left_bot .bestway_down").click(function(){
		
		if(def_position<(bestway_left_length-1)){
			
			$(".bestway_left_list").stop().animate({"marginTop":-(def_position+1)*350+"px"});
			def_position++;
		}		
		
	});	

});

//解决方案文字上下切换
$(function(){
	
	var bestway_right=$(".bestway_right_list").height();
	var bestway_right_length=Math.ceil(bestway_right/350); 
	var def_position=0;

	
	$(".bestway_right_bot .bestway_up").click(function(){
		
		if(def_position>0){
			
			$(".bestway_right_list").stop().animate({"marginTop":-(def_position-1)*350+"px"});
			def_position--;
		}
		
	});
	$(".bestway_right_bot .bestway_down").click(function(){
		
		if(def_position<(bestway_right_length-1)){
			
			$(".bestway_right_list").stop().animate({"marginTop":-(def_position+1)*350+"px"});
			def_position++;
		}		
		
	});	

});

//服务allday
$(".allday_list ul li").hover(function(){
	
	$(this).find(".allday_list_info").stop().animate({"top":0});
},function(){
	
	$(this).find(".allday_list_info").stop().animate({"top":307+"px"});
});


//服务Technical Trainings
$(".train_right").children().eq(0).show().siblings().hide();

$(".train_nav ul li").hover(function(){
	
	var index=$(this).index();
	$(this).addClass("on").siblings().removeClass("on");
	$(this).parents(".train_left").siblings(".train_right").children().eq(index).show().siblings().hide();
});

//esc 产品切换
$(".ecs_main_tabs").children().eq(0).show().siblings().hide();
$(".ecs_main_nav ul li").click(function(){
	
	var index=$(this).index();
	$(this).addClass("on").siblings().removeClass("on");
	$(this).parents(".ecs_main_nav").siblings(".ecs_main_tabs").children().eq(index).show().siblings().hide();
});

$(".ecs_intell_tabs").children().eq(0).show().siblings().hide();
$(".ecs_intell_nav ul li").click(function(){
	
	var index=$(this).index();
	$(this).addClass("on").siblings().removeClass("on");
	$(this).parents(".ecs_intell_nav").siblings(".ecs_intell_tabs").children().eq(index).show().siblings().hide();
	
});

//jx 产品
$(".jx_cont_title").click(function(){
	
	$(this).parent("li").addClass("on").siblings().removeClass("on");
});

$(".highly_tabs").children().eq(0).show().siblings().hide();
$(".highly_right ul li").click(function(){
	
	var index=$(this).index();
	$(this).find(".highly_erify").stop().animate({"right":0});
	$(this).siblings().find(".highly_erify").stop().animate({"right":-270+"px"});
	$(".highly_tabs").children().eq(index).show().siblings().hide();
});

//careers 底部连接鼠标滑过效果
$(".move_link a").hover(function(){
	
	$(this).find(".move_link_icon").stop().animate({"left":40+"px"});
},function(){
	$(this).find(".move_link_icon").stop().animate({"left":30+"px"});	
});



//关于我们
$(".about_industry_list ul li").hover(function(){
	
	$(this).find(".about_indust_pic img").stop().animate({"marginTop":-70+"px"});

},function(){
	$(this).find(".about_indust_pic img").stop().animate({"marginTop":0}).toggle;
});

//关于我们R&D Force 居中
$(".force_list ul li").each(function() {
    
	var index=$(this).index();
	if(index>0){
		
		var height=$(this).find(".force_info").height();
		$(this).find(".force_info").css({"top":-(height+18)/2+"px"});
	}
});


$(".about_pro_list ul li").hover(function(){
	
	$(this).find(".about_pro_pic img").stop().animate({"marginTop":-70+"px"});
	$(this).find(".about_pro_line").stop().animate({"width":62+"px"});
},function(){
	$(this).find(".about_pro_pic img").stop().animate({"marginTop":0});
	$(this).find(".about_pro_line").stop().animate({"width":30+"px"});
});

$(".about_pro_bot a").hover(function(){
	
	$(this).find(".move_link_icon").stop().animate({"left":60+"px"});
},function(){
	$(this).find(".move_link_icon").stop().animate({"left":50+"px"});	
});

$(".capacity_bot a").hover(function(){
	
	$(this).find(".move_link_icon").stop().animate({"left":29+"px"});
},function(){
	$(this).find(".move_link_icon").stop().animate({"left":19+"px"});		
	
});


	
//input 框，输入点击提示文字消失
$('input[type="text"]').focus(function(){
	if($(this).attr('_reg'))
	{
		$(this).addClass('focus');
		var reg=$(this).attr('_reg');
		if($(this).val() == reg)
		{
			$(this).val('');	
			if($(this).attr('_pwd'))
			{
			   $(this).attr('type','password');
			   
			}
		}	
	}
	
});

$('input[type="text"]').blur(function(){
	if($(this).attr('_reg'))
	{
		var reg=$(this).attr('_reg');
		if($(this).val() == '')
		{
			$(this).val(reg);	
			$(this).removeClass('focus');
			if($(this).attr('_pwd'))
			{
				$(this).attr('type','text');
			}
		}
	}
	
});	
