<?php
session_start();
header("Pragma:no-cache\r\n");
header("Cache-Control:no-cache\r\n");
header("Expires:0\r\n");
header("Content-Type: text/html; charset=uft-8");
define('INIT_XMALL',true);
define('INIT_ROOT','../');
include_once(INIT_ROOT."system_include/conn.php"); 
include_once(INIT_ROOT."system_include/check_purview.php");
//include('../editor/fckeditor.php');

//=========== 读出网站基本数据 ===========

   $CmsSql="Select * from ".$tablepre."config where ID=1";
   $Result = $db->query($CmsSql);
   $Row = $db->fetch_array($Result);
   
   //print_r($Row);
   
   //Split 的一种用法
   list($hxcms_url,$hxcms_title,$hxcms_Emal,$hxcms_Number)= @split("/",$Row[$tablepre.'config'],4);
   $hxcms_Copyright = $Row['hxcms_Copyright'];
   $hxcms_open    = $Row['hxcms_open'];
   $hxcms_Content = $Row['hxcms_webContent'];
   $regkeywords   = $Row['regkeywords'];
   $Keywords      = $Row['Keywords'];
   $User_Config   = $Row['User_Config'];
   list($User_Name,$User_tel,$User_phone,$User_fax,$User_email,$User_address,$User_zipcode) = @split("‖",$User_Config,7);
   

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../system_style/css/style.css" />
<script type="text/javascript" src="../system_style/js/HxCms.js"></script>
</head>
<body class="indexbody">

<div class="sys_location">
	<div class="current">
		<a href="admin_main.php" class="current-hot">管理中心</a>&minus;
		<a href="admin_setting.php">基本信息</a>
	</div>
	<a href="admin_main.php" class="sys_item">返回首页</a>
</div><!--sys_location-->

<?php
// 保存页面信息 操作 Start()
$action=$_REQUEST['action'];
if ($action!="")
{
	switch ($action)
	{
	case "savemain";
		echo savemain();
		break;
	case "saveconta";
		echo saveconta();
		break;
	}
} 
// 保存页面信息 操作 end()
?>
<form name="myform" method="post" action="?action=savemain">
<table class="table_c" cellspacing="0" cellpadding="0" width="100%" border="0">
<tr>
	<th colspan="2">网站基本信息　<span class="clicksubmit" onClick="document.myform.submit();">[保存设置]</span></th>
</tr>
<tr>
	<td width="15%" ><u title="hxcms_weburl">网站域名</u>：</td><?=$Row['hxcms_url']?>
	<td width="85%"><input name="hxcms_weburl" type="text" id="hxcms_weburl" value="<?php echo stripcslashes($Row['hxcms_weburl']);?>" size="35"> </td>
</tr>
<tr>
	<td  ><u title="hxcms_webtitle">网站名称</u>：</td>
	<td ><input name="hxcms_webtitle" type="text" id="hxcms_webtitle" value="<?php echo stripcslashes($Row['hxcms_webtitle']);?>" size="35"></td>
</tr>
<tr>
	<td  ><u title="hxcms_webEmal">管理员Email</u>：</td>
	<td ><input name="hxcms_webEmal" type="text" id="hxcms_webEmal" value="<?php echo stripcslashes($Row['hxcms_webEmal']);?>" size="35"></td>
</tr>
<tr>
	<td ><u title="hxcms_webNumber">网站备案号</u>：</td>
	<td><input name="hxcms_webNumber" type="text" id="hxcms_webNumber" value="<?php echo stripcslashes($Row['hxcms_webNumber']);?>" size="35"></td>
</tr>
<tr>
	<td ><u title="hxcms_webCopyright">网站版权信息</u>：</td>
	<td><textarea name="hxcms_webCopyright" cols="60" rows="3" id="hxcms_webCopyright"><?php echo stripcslashes($Row['hxcms_webCopyright']);?></textarea></td>
</tr>
<tr>
	<td ><u title="Keywords">网站关键字</u>：</td>
	<td><textarea name="Keywords" cols="80" rows="3" id="Keywords"><?=$Row['Keywords']?></textarea></td>
</tr>
<tr>
	<td ><u title="regkeywords">网站描述</u>：</td>
	<td><textarea name="regkeywords" cols="80" rows="3" id="regkeywords" ><?=$Row['regkeywords'];?></textarea></td>
</tr>
<tr>
	<td >&nbsp;</td>
	<td><input type="submit" value="保存设置" name="submit_button" id="submit_button" class="button"> </td>
</tr>
</table>
</form>








<form name="myconta" method="post" action="?action=saveconta">
<table class="table_c" cellspacing="0" cellpadding="0" width="100%" border="0">
<tr>
	<th colspan="2">联系方式　<span class="clicksubmit" onClick="document.myform.submit();">[保存设置]</span></th>
</tr>
<tr>
	<td width="15%" ><u title="User_address">联系地址</u>：</td>
	<td width="85%"><input name="User_address" type="text" id="User_address" value="<?php echo stripcslashes($User_address);?>" size="35"> </td>
</tr>
<tr>
	<td  ><u title="User_email">联系邮箱</u>：</td>
	<td ><input name="User_email" type="text" id="User_email" value="<?php echo stripcslashes($User_email);?>" size="35"></td>
</tr>
<tr>
	<td  ><u title="User_phone">联系电话</u>：</td>
	<td ><input name="User_phone" type="text" id="User_phone" value="<?php echo stripcslashes($User_phone);?>" size="35"></td>
</tr>
<tr style="display:none;">
	<td ><u title="User_fax">联系传真</u>：</td>
	<td ><input name="User_fax" type="text" id="User_fax" value="<?php echo stripcslashes($User_fax);?>" size="35"> </td>
</tr>

<tr>
	<td  >&nbsp;</td>
	<td ><input type="submit" value="保存设置" name="submit_button" id="submit_button" class="button"> </td>
</tr>
</table>
</form>
<?php

  function savemain()  //保存网站基本信息
  { 
  		global $tablepre;
		
  	 $hxcms_weburl       = addslashes($_POST["hxcms_weburl"]);   //域名
	 $hxcms_webtitle     = addslashes($_POST['hxcms_webtitle']); //网站名称
  	 $hxcms_webEmal      = addslashes($_POST['hxcms_webEmal']);  //邮件
	 $hxcms_webNumber    = addslashes($_POST['hxcms_webNumber']);//备案号
	 $hxcms_webCopyright = addslashes($_POST['hxcms_webCopyright']);//网站版权信息
	 $hxcms_webopen      = addslashes($_POST['hxcms_webopen']);        // 网站打开状态
	 $regkeywords        = addslashes($_POST['regkeywords']); 
	 $Keywords           = addslashes($_POST['Keywords']);
	 
	 //合并信息
	 $hxcms_webConfig =$hxcms_weburl."/".$hxcms_webtitle."/".$hxcms_webEmal."/".$hxcms_webNumber;
     
	 //$hxcms_date =date("Y-m-d H:i:s",time()+3600*8);
     date_default_timezone_set('PRC');//定义时区
     $hxcms_date =date("Y-m-d H:i:s");
     
	 //Sql语句
     $CmsSql = "Update ".$tablepre."config set ";
     $CmsSql = $CmsSql . "hxcms_weburl      ='$hxcms_weburl' ";   
     $CmsSql = $CmsSql . ",hxcms_webtitle    ='$hxcms_webtitle' "; 
     $CmsSql = $CmsSql . ",hxcms_webopen     ='$hxcms_webopen' "; 
     $CmsSql = $CmsSql . ",hxcms_webEmal      ='$hxcms_webEmal' ";   
     $CmsSql = $CmsSql . ",hxcms_webNumber    ='$hxcms_webNumber' "; 
	 $CmsSql = $CmsSql . ",hxcms_webCopyright='$hxcms_webCopyright' ";
	 $CmsSql = $CmsSql . ",regkeywords      ='$regkeywords' ";
	 $CmsSql = $CmsSql . ",Keywords         ='$Keywords' ";
     $CmsSql = $CmsSql . ",hxcms_date       ='$hxcms_date' ";
     // $CmsSql = $CmsSql ." where ID=" 
	 //echo $CmsSql;exit;
     
	 global $db; 
     if(!$db->query($CmsSql)){die("数据库错误<br>");}
	  echo ok("信息更新成功","admin_setting.php",2);
  }
  
  function saveconta()//保存网站图片上传信息配置;
  {
	global $tablepre;
  	$User_Name    = addslashes($_REQUEST["User_Name"]);   // 联系人
	$User_tel     = addslashes($_REQUEST["User_tel"]);    // 联系电话
	$User_phone   = addslashes($_REQUEST["User_phone"]);  // 联系手机
	$User_fax     = addslashes($_REQUEST["User_fax"]);    // 传真
	$User_email   = addslashes($_REQUEST["User_email"]);  // 邮箱
	$User_address = addslashes($_REQUEST["User_address"]);// 联系地址
	$User_zipcode = addslashes($_REQUEST["User_zipcode"]);// 邮编
	
	$User_Config  = $User_Name."‖".$User_tel."‖".$User_phone."‖".$User_fax."‖".$User_email."‖".$User_address."‖".$User_zipcode;
	
	global $db; 
    if(!$db->query("update ".$tablepre."config set `User_Config`='$User_Config' where ID=1")){die("数据库错误<br>");}
	echo ok("信息更新成功","admin_setting.php",2);
  }
  $db->Close();
 ?>
<!--<div class="sys_bottom">版权所有 © 2013-2033 杭州乐邦科技有限公司，并保留所有权利。</div>-->
</body>
</html>