<?php
session_start();
header("Pragma:no-cache\r\n");
header("Cache-Control:no-cache\r\n");
header("Expires:0\r\n");
header("Content-Type: text/html; charset=uft-8");
define('INIT_XMALL',true);
define('INIT_ROOT','../');
include_once(INIT_ROOT."system_include/conn.php");
include_once(INIT_ROOT."system_include/check_purview.php");

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../system_style/css/left.css" />
<script type="text/javascript" src="../system_style/js/HxCms.js"></script>
<script language="JavaScript">
function showsubmenu(sid) {
	var whichEl = document.getElementById("submenu" + sid);
	var menuTitle = document.getElementById("menuTitle" + sid);
	if (whichEl!=null) {
		if (whichEl.style.display == "none"){
			whichEl.style.display='';
			if (menuTitle!=null)
			menuTitle.className='menu_title';
		}else{
			whichEl.style.display='none';
			if (menuTitle!=null)
			menuTitle.className='menu_title2';
		}
	}
}
</script>
</head>
<body>
<table class="listflow" cellspacing="0" cellpadding="0" height="100%">