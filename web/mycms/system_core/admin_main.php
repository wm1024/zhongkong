<?php
session_start();
header("Pragma:no-cache\r\n");
header("Cache-Control:no-cache\r\n");
header("Expires:0\r\n");
header("Content-Type: text/html; charset=uft-8");
define('INIT_XMALL',true);
define('INIT_ROOT','../');
include_once(INIT_ROOT."system_include/conn.php"); 
include_once(INIT_ROOT."system_include/check_purview.php");

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../system_style/css/style.css" />
<script type="text/javascript" src="../system_style/js/HxCms.js"></script>
</head>
<body class="indexbody">

<div class="sys_location">
	<div class="current">
		<a href="admin_main.php" class="current-hot">管理中心</a>&minus;
		<a href="#">系统首页</a>
	</div>
	<!--<a href="#" class="sys_item">&nbsp;</a>-->
</div><!--sys_location-->
<!--
<table class="table_c" cellspacing="0" cellpadding="0" width="100%" border="0">
	<tr>
		<th colspan="8" align="left">我的快捷方式</th>
	</tr>
	<tr>
		<td align="center" width="12.5%" height="23"><strong>首页</strong></td>
        <td align="center" width="12.5%"><strong>关于申昊</strong></td>
        <td align="center" width="12.5%"><strong>新闻中心</strong></td>
        <td align="center" width="12.5%"><strong>产品展示</strong></td>
        <td align="center" width="12.5%"><strong>荣誉责任</strong></td>
        <td align="center" width="12.5%"><strong>人才招聘</strong></td>
        <td align="center" width="12.5%"><strong>招资者关系</strong></td>
   	  <td align="center" width="12.5%"><strong>其它</strong></td>
	</tr>
	<tr>
<td align="center" height="23">
<a href="../ads/admin_index.php?str_classid=1086" title="banner广告">切换广告</a><br />
<a href="../news/admin_index.php" title="即：新闻首页推荐">图片广告</a><br />
<a href="../info/admin_index.php?str_classid=1088">关于申昊</a><br />
<a href="../info/admin_index.php?str_classid=1089">岗位招聘</a><br />
<a href="../info/admin_index.php?str_classid=1090">企业文化</a>
</td>
<td align="center">
<a href="../abouts/admin_index.php?str_classid=1058">申昊简介</a><br />
<a href="../abouts/admin_index.php?str_classid=1059">组织架构</a><br />
<a href="../course/admin_index.php">申昊历程</a><br />
<a href="../abouts/admin_index.php?str_classid=1060">申昊文化</a><br />
<a href="../abouts/admin_index.php?str_classid=1061">董事长致辞</a><br />
<a href="../abouts/admin_index.php?str_classid=1062">联系我们</a>
</td>
<td align="center">
<a href="../news/admin_index.php?str_classid=1066">公司新闻</a><br />
<a href="../news/admin_index.php?str_classid=1067">行业新闻</a>
</td>
<td align="center">
<a href="../products/admin_index.php?str_classid=1068">变压器在线监测系统</a><br />
<a href="../products/admin_index.php?str_classid=1069">输电线路在线监测系统</a><br />
<a href="../products/admin_index.php?str_classid=1070">配电自动化及终端设备</a><br />
<a href="../products/admin_index.php?str_classid=1071">系统智能集成解决方案</a><br />
<a href="../products/admin_index.php?str_classid=1072">其他</a>
</td>
<td align="center">
<a href="../imgs/admin_index.php?str_classid=1073">社会责任</a><br />
<a href="../imgs/admin_index.php?str_classid=1074">领导关怀</a><br />
<a href="../imgs/admin_index.php?str_classid=1075">申昊荣誉</a>
</td>
<td align="center">
<a href="../jobs/admin_index.php">岗位招聘</a><br />
<a href="../info/admin_index.php">在线招聘</a>
</td>
<td align="center">
<a href="../abouts/admin_index.php?str_classid=1080">公司概况</a><br />
<a href="../abouts/admin_index.php?str_classid=1081">实时行情</a><br />
<a href="../abouts/admin_index.php?str_classid=1082">股本分红</a><br />
<a href="../abouts/admin_index.php?str_classid=1083">财务数据</a><br />
<a href="../news/admin_index.php?str_classid=1078">临时公告</a><br />
<a href="../abouts/admin_index.php?str_classid=1084">定期报告</a><br />
<a href="../abouts/admin_index.php?str_classid=1085">投资者热线</a>
</td>
<td align="center">
<a href="../links/admin_index.php">友情链接</a>
</td>
</tr>
</table>
-->
	
<table class="table_c" cellspacing="0" cellpadding="0" width="100%" border="0">
<tr>
		<th colspan="2" align="left">我的个人信息</th>
	</tr>
	<tr>
		<td width="100" height="23">用户基本信息：</td>
        <td style="line-height: 150%">您好，<font color="#ed6137"><?php echo $_SESSION['adminName'];?></font> <a href="admin_member.php" title="点击修改管理员密码">							        <?php
		if (checkManag($_SESSION["adminLov"])==true){echo " <span style='color:#fff;background:red;'>&nbsp;超级管理员&nbsp;</span>";}
		if (checkManag($_SESSION["adminLov"])==false)
		{
			if ($_SESSION["adminLov"]==1){echo " <span style='color:#fff;background:#016AA9;'>&nbsp;超级管理员&nbsp;</span>";}
			if ($_SESSION["adminLov"]==0){echo " <span style='color:#fff;background:blue;'>&nbsp;普通管理员&nbsp;</span>";}
		}
        ?></a>　
		<a href="admin_logout.php" target="_parent">退出</a></td>
	</tr>
	<tr>
		<td width="100" height="23">上次登陆时间：</td>
		<td style="line-height: 150%">2012-3-24 17:15:19</td>
	</tr>
	<tr>
		<td width="100" height="23">上次登陆IP：</td>
		<td style="line-height: 150%"><?php echo getip_out();?></td>
	</tr>
</table>

<table class="table_c" cellspacing="0" cellpadding="0" width="100%" border="0">
	<tr>
		<th colspan="2" align="left">网站系统信息</th>
	</tr>
	<tr>
		<td width="100" height="23">系统当前版本：</td>
		<td >LbMyCms V1.1 版本</td>
	</tr>
	<tr>
		<td width="100" height="23">操作系统：</td>
		<td ><?php echo userOS()?> <?php echo userBrowser()?></td>
	</tr>
	<tr>
		<td width="100" height="23">服务器软件：</td>
		<td >NULL<?php //echo apache_get_version();?></td>
	</tr>
	<tr>
		<td width="100" height="23">MySQL版本：</td>
		<td ><?php echo mysql_get_server_info();?></td>
	</tr>
	<tr>
		<td width="100" height="23">配置信息：</td>
		<td ><a href="not_safe/myphpinfo.php" target="_blank">查看配置信息</a></td>
	</tr>
	<tr>
		<td width="100" height="23">幼儿园信息：</td>
		<td ><a href="indexlist.php" target="_blank">查看列表信息</a></td>
	</tr>
</table>

<table class="table_c" cellspacing="0" cellpadding="0" width="100%" border="0">
	<tr>
		<th colspan="2" align="left">系统版权信息</th>
	</tr>
	<tr>
		<td height="23">版权声明：</td>
		<td style="line-height: 150%">1、本系统未经书面授权，不得向任何第三方提供本软件系统;<br />
		2、用户自由选择是否使用,在使用中出现任何问题和由此造成的一切损失作者将不承担任何责任;<br />
	  3、本系统受中华人民共和国《著作权法》《计算机软件保护条例》等相关法律、法规保护，作者保留一切权利。　</td>
	</tr>
</table>

<!--<div class="sys_bottom">版权所有 © 2013-2033 杭州乐邦科技有限公司，并保留所有权利。</div>-->

</body>
</html>