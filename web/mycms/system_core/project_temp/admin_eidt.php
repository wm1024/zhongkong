<?php
session_start();
header("Pragma:no-cache\r\n");
header("Cache-Control:no-cache\r\n");
header("Expires:0\r\n");
header("Content-Type: text/html; charset=uft-8");
define('INIT_XMALL',true);
define('INIT_ROOT','../');
include_once(INIT_ROOT."system_include/conn.php"); 
include_once(INIT_ROOT."system_include/check_purview.php");
include_once("admin_config.php");
	
	//获取栏目ID
	//$SystemID =addslashes($_REQUEST["m"]);
	$tabName  =tabName($SystemID);
	$strid =$_REQUEST["stids"];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link rel="stylesheet" type="text/css" href="../system_style/css/style.css" />
<script type="text/javascript" src="../system_style/js/HxCms.js"></script>
</head>
<body class="indexbody">

<div class="sys_location">
	<div class="current">
		<a href="admin_main.php" class="current-hot">管理中心</a>&minus;
		<a href="#">编辑信息</a>
	</div>
	<a href="admin_index.php" class="sys_item">返回列表</a>
</div><!--sys_location-->

<?=$htmls?>
<form name="myform" method="post" action="admin_save.php?action=editsave" onSubmit="return checkform()">
<INPUT type="hidden" id="strid" value="<?=$strid?>" name="strid">
<table class="sys_add" cellspacing="0" cellpadding="0" width="100%" border="0">
<?php
$cms_sql="select * from $tabName where id=$strid";
$cms_result=$db->query($cms_sql);
$cms_row=$db->fetch_array($cms_result);
$fieSql="Select * from ".$tablepre."field where SystemID=$SystemID order by sequence asc";
 	$fieResult = $db->query($fieSql);
 	$x=0;
 	while ($fieRow=$db->fetch_array($fieResult))
 	{
 		if ($fieRow['show']==1) //判断字段是否显示
 		{
 			echo "<tr>";
 			echo "<th width='15%' align='right'>".$fieRow['fieldtxt']."：</th>";
 			echo "<td width='85%' >";
			
			echo showfieldout($fieRow['fieldout'],$fieRow['fieldName'],$fieRow['fieldlength'],$fieRow['fieldcontent'],$fieRow['iscontent'],$fieRow['fault'],$cms_row[$fieRow['fieldName']],$SystemID);

 			//Start
 			if($fieRow['fieldinfo']!="")
 			{
 				echo "&nbsp;&nbsp;<font style='color:#F00'>注</font>：<span style='color:#FF3C3C'>".$fieRow['fieldinfo']."</span>";
 			}
 			// end 
 			
 			echo "</td>";
 			echo "</tr>";	
 			$x++;
 		}
 	}
?>
<tr><th>&nbsp;</th><td><input type="submit" name="button" id="button" value="提交信息" style="width:100px; height:30px; font-size:13px" /></td></tr>
</table>
</form>
<?php 
 //调用检测表单函数；
 checkmyform($SystemID);
?>
<!--<div class="sys_bottom">版权所有 © 2013-2033 杭州乐邦科技有限公司，并保留所有权利。</div>-->
<script type="text/javascript" src="../system_style/datejs/date.js"></script>
</body>
</html>
