<?php
session_start();
header("Pragma:no-cache\r\n");
header("Cache-Control:no-cache\r\n");
header("Expires:0\r\n");
header("Content-Type: text/html; charset=uft-8");
define('INIT_XMALL',true);
define('INIT_ROOT','../');
include_once(INIT_ROOT."system_include/conn.php"); 
include_once(INIT_ROOT."system_include/check_purview.php");
include_once(INIT_ROOT."system_core/admin_main_list_control.php");

    /*取幼儿园列表*/
    $schoolList     = getSchoolList();
    foreach($schoolList as $k => $v){
        $schoolList[$k]['classList']      = getClassList($v['id']);
        foreach($schoolList[$k]['classList'] as $key => $value){
            $schoolList[$k]['classList'][$key]['teacher']           = getTeacherList($value['id']);
            $schoolList[$k]['classList'][$key]['baobao']            = getBaobaoList($value['id']);
            $schoolList[$k]['classList'][$key]['total']             = count($schoolList[$k]['classList'][$key]['baobao']);
        }
    }

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../system_style/css/style.css" />
<script type="text/javascript" src="../system_style/js/HxCms.js"></script>
</head>
<body class="indexbody">

<div class="sys_location">
	<div class="current">
		<a href="admin_main.php" class="current-hot">管理中心</a>&minus;
		<a href="#">幼儿园列表信息</a>
	</div>
	<!--<a href="#" class="sys_item">&nbsp;</a>-->
</div><!--sys_location-->

	
<table class="table_c" cellspacing="0" cellpadding="0" width="100%" border="0">
	<tr>
		<th colspan="3" align="left">幼儿园列表信息</th>
	</tr>
    <?php foreach($schoolList as $k => $v){?>
	<tr class="school">
        <td width="5%" style="text-align: center"><span style="cursor: pointer">展开</span></td>
        <td width="20%" height="23"><a href="<?php echo INIT_ROOT;?>school/admin_eidt.php?stids=<?php echo $v['id']?>&cid=0"><?php echo $v['title']?></a></td>
        <td width="75%">&nbsp;  </td>
    </tr>


    <tr style="display: none"><td colspan="3" style="text-align: left"><table style="width: 100%"cellspacing="0" cellpadding="0" border="0">
        <?php foreach($v['classList'] as $ke => $va){?>

                <tr class="class">
                    <td width="5%">&nbsp;</td>
                    <td width="20%">
                        <span style="cursor: pointer">展开</span>&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo INIT_ROOT;?>teams/admin_eidt.php?stids=<?php echo $va['id']?>&cid=0"><?php echo $va['title']?></a>
                    </td>
                    <td width="15%"><?php echo $va['total']?>个宝宝</td>
                    <td class="teacher" width="60%">
                        (包含老师：
                            <?php foreach($va['teacher'] as $value){?>
                            <a style="color: #f00" href="<?php echo INIT_ROOT;?>member_basis/admin_eidt.php?stids=<?php echo $value['id']?>&cid=0"><?php echo $value['name']?></a>
                            <?php }?>
                        )
                    </td>
                </tr>
                <tr style="display: none"><td colspan="3" style="text-align: left"><table style="width: 100%"cellspacing="0" cellpadding="0" border="0">
                    <?php foreach($va['baobao'] as $value){?>
                    <tr>
                        <td width="15%">&nbsp;</td>
                        <td width="20%"><a style="color: #f00" href="<?php echo INIT_ROOT;?>member_baby/admin_eidt.php?stids=<?php echo $value['id']?>&cid=0"><?php echo $value['name']?></a></td>
                        <td width="65%">&nbsp;</td>
                    </tr>
                    <?php }?>
                </table></td></tr>

        <?php }?>
    </table></td></tr>


    <?php }?>
</table>
<script type="text/javascript" src="../system_style/js/jquery-1.4.2.js"></script>
<script type="application/javascript">
    $(document).ready(function(){
        $(".school td span").click(function(){
            $(this).parent().parent().next().toggle();
            if($(this).html()=='展开'){
                $(this).html('闭合');
            }else{
                $(this).html('展开');
            }
        });
        $(".class td span").click(function(){
            $(this).parent().parent().next().toggle();
            if($(this).html()=='展开'){
                $(this).html('闭合');
            }else{
                $(this).html('展开');
            }
        });
    })
</script>
</body>
</html>