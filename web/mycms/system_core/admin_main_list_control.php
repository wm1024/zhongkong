<?php
session_start();
header("Pragma:no-cache\r\n");
header("Cache-Control:no-cache\r\n");
header("Expires:0\r\n");
header("Content-Type: text/html; charset=uft-8");
define('INIT_XMALL',true);
define('INIT_ROOT','../');
include_once(INIT_ROOT."system_include/conn.php"); 
include_once(INIT_ROOT."system_include/check_purview.php");

/*取所有学校*/
function getSchoolList(){
    global $tablepre;
    $result = array();
    $sql = "select * from ".$tablepre."school where `show`=1 and recover=0 order by sequence asc,id asc";
    $rel = mysql_query($sql);
    while($rs = mysql_fetch_array($rel)){
        $result[] = $rs;
    }
    return $result;
}
/*取学校里所有班级*/
function getClassList($schoolId){
    global $tablepre;
    $result = array();
    $sql = "select * from ".$tablepre."teams where `show`=1 and recover=0 and school_id='".$schoolId."' and partid<>0 order by partid asc,id asc";
    $rel = mysql_query($sql);
    while($rs = mysql_fetch_array($rel)){
        $result[] = $rs;
    }
    return $result;
}
/*取班级里所有的老师*/
function getTeacherList($classId){
    global $tablepre;
    $result = array();
    $sql = "select * from ".$tablepre."member_basis where recover=0 and find_in_set(".$classId.",class) order by id asc";
    $rel = mysql_query($sql);
    while($rs = mysql_fetch_array($rel)){
        $result[] = $rs;
    }
    return $result;
}
/*取班级里所有的宝宝*/
function getBaobaoList($classId){
    global $tablepre;
    $result = array();
    $sql = "select * from ".$tablepre."member_baby where recover=0 and class='".$classId."' order by id asc";
    $rel = mysql_query($sql);
    while($rs = mysql_fetch_array($rel)){
        $result[] = $rs;
    }
    return $result;
}