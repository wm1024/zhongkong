<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>信息管理系统_用户登录</title>
<link rel="stylesheet" type="text/css" href="system_style/css/login.css" />
<script src="system_style/js/form.js" type="text/javascript"></script>
<script src="system_style/js/login.js" type="text/javascript"></script>
</head>

<body>
<div class="login_div">
<div class="top"><img src="system_style/images/login_title.jpg" width="217" height="33" /></div><!-- END top -->

<div class="middle">
	<form action="system_include/check.php" method="post" name="myform" onsubmit="return login_check(this)">
	<div><input name="adminUserName" type="text" class="uname" id="adminUserName" onfocus="upbg(this);if(value=='用户名')value='';" onblur="upbg(this)" value="<?php if(isset($_COOKIE["re_username"])){echo $_COOKIE["re_username"];}else{echo '用户名';}?>" maxlength="20" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" /></div>
	<div><input name="adminPassWord" type="password" class="upswd" id="adminPassWord" value="" maxlength="30" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" /></div>
	<div><table cellpadding="0" cellspacing="0" border="0">
	<tr><td><input name="adminCode" type="text" class="ucode" id="code" onfocus="upbg(this);if(value=='验证码')value='';" onblur="upbg(this)" value="验证码" maxlength="4" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" /></td>
	<td valign="top"><img src="system_config/system/code.php" width="80" height="39" border="0" onClick="this.src=this.src+'?tm='+Math.random();" style="vertical-align:inherit;margin-left:1px;cursor:pointer;"  alt="看不清？点击更换"/></td></tr></table></div>
	<div class="name_on">
		<span class="left"><input name="name_on" type="checkbox" checked="checked" id="name_on" value="1" class="styled" />记住帐号</span>
		<span class="right"><a href="#" title="联系我们找回密码，热线电话：0571-89937200">&nbsp;</a></span>
	</div>
	<div><input type="submit" name="button" id="button" class="loginan" value="" /></div>
	<div id="login_msg"></div>
	</form>
</div><!-- END middle -->

<div class="bottom"></div><!-- END bottom -->
</div>

</body>
</html>