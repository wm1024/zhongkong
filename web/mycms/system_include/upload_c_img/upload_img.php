<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>test</title>
<link rel="stylesheet" type="text/css" href="css/imgareaselect-default.css" />
<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jquery.imgareaselect.pack.js"></script>
</head>
<?php
$wharr = $_GET['whs'];//比列
if (! empty($wharr))
{
	$wharr = explode('*', $wharr);
	$ww = $wharr[0];
	$hh = $wharr[1];
	$ww = $ww/5;
	$hh = $hh/5;
}

$picurl = substr($_GET['pic'],0,20);//图片
list($ws,$hs) = @getimagesize('../../../upload/image/'.substr($picurl,0,7).'/'.$picurl);

$wh =  explode('|',imgwh($ws, $hs));//等比缩小
$w = $wh[0];
$h = $wh[1];




?>
<body>
<script type="text/javascript">
function preview(img, selection) {

	var scaleX = <?=$ww?> / (selection.width || 1);
	var scaleY = <?=$hh?> / (selection.height || 1);
	$('#ferret + div > img').css({
		
	width: Math.round(scaleX * <?=$w?>) + 'px',
	height: Math.round(scaleY * <?=$h?>) + 'px',
	marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
	marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
	
	});

}

//这里通过jQuery语法在原来图片后插入同样的图片
$(document).ready(function () {

	$('<div style="border:2px solid #cccccc; margin-right:10px;"><img src="../../../upload/image/<?=substr($picurl,0,7)?>/<?=$picurl?>" style="position: relative;" /><div>').css({
	float: 'left',
	position: 'relative',
	overflow: 'hidden',
	width: '<?=$ww?>px',
	height: '<?=$hh?>px'
	})
	.insertAfter($('#ferret'));

	$('#ferret').imgAreaSelect({
		aspectRatio: '<?=$ww?>:<?=$hh?>',
		onSelectChange: preview,
		onSelectEnd: function (img, selection) {
		$('input[name="x1"]').val(selection.x1);
		$('input[name="y1"]').val(selection.y1);
		$('input[name="x2"]').val(selection.x2);
		$('input[name="y2"]').val(selection.y2);
		}
	});
});
</script>
<div>
<img src="../../../upload/image/<?=substr($picurl,0,7)?>/<?=$picurl?>" id="ferret" width="<?=$w?>" height="<?=$h?>" style="border:2px solid #cccccc;"/>
</div>
<div align="center" style="margin-top:15px; border-top:#eee 1px solid;"><br />
<form action="crop.php" method="post">
<input type="hidden" name="pic" value="<?=$picurl?>"  />
<input type="hidden" name="x1" value="" />
<input type="hidden" name="y1" value="" />
<input type="hidden" name="x2" value="" />
<input type="hidden" name="y2" value="" />
<input type="hidden" name="ss_w" value="<?=$w?>" />
<input type="hidden" name="ss_h" value="<?=$h?>" />
<input type="submit" name="submit" value="裁剪并保存" style="font-size:12px; font-family:Arial, '微软雅黑', '宋体';" />
</form>
<font color="#CCCCCC">使用说明：通过按住鼠标左键在图片中拖动矩形框来截图</font>
</div>
</body>
</html>
<?php

function imgwh($w, $h)
{
	if ($w>500 or $h>500)
	{
		$w = $w/2;
		$h = $h/2;
		return imgwh($w, $h);
	}
	else {
		return $w.'|'.$h;
	}
}


?>