<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/login_pop.php")?>
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="sub_cont">
    	<div class="container">
        	<div class="clearfix">	
        		<a class="resource_login fr" href="javascript:;" onclick="openLogin();"><i class="login"></i>Log in</a>
            </div>
 			<div class="milestone_title resource_title">Industry Solution</div>
            <div class="sub_top_search resource_top_search clearfix">
    			<input type="text" value="Search Now" _reg="Search Now" class="keyword_text fl" /> 
                <div class="select fl">
                	<p>ALL</p>
                    <i class="select_arrow"></i>
                	<ul>
                    	<li>ALL</li>
                    	<li>PDF</li>
                    	<li>JPG</li>
                        <li>Word</li>
                        <li>Excel</li>
                        <li>ZIP</li>
                    </ul>
                    <input type="hidden" value="" id="resource_type" />
                </div>
    			<input type="submit" value="Search" class="subBtn fl" />
                <p class="fl">or</p>
                <a class="fr" href="advanced_search.php">Advanced Search</a>
    		</div>
            <div class="resource_sub_list">
            	<ul class="clearfix">
            		<li class="clearfix">
            			<div class="resource_left fl"><a href="##"><img src="img/resourece_pic.jpg" width="156" height="205" />
                        	<div class="type_icon">JPG</div>
                        	<i class="lock_icon"></i>
                        </a></div>
            			<div class="resoucre_right fr">
                        	<div class="resource_sub_name"><a href="##">SUPCON Newsletter December 2010</a></div>
                        	<div class="resource_size">File size : 16.2M</div>
                        	<div class="update_time">Update Time : 2015/02/04</div>
                        	<div class="resource_download clearfix">
                            	<a class="download fl" href="#">Download</a>
                                <div class="resourse_share fl">
                                    <div class="bdsharebuttonbox">
                                        <a title="分享到一键分享" href="javascript" class="bds_mshare" data-cmd="mshare">Share</a>
                                    </div> 
                                </div>
                            </div>
                        </div>
            		</li>
            		<li class="clearfix">
            			<div class="resource_left fl"><a href="##"><img src="img/resourece_pic.jpg" width="156" height="205" />
                        	<div class="type_icon">JPG</div>
							<div class="view_yin"></div>
                        </a></div>
            			<div class="resoucre_right fr">
                        	<div class="resource_sub_name"><a href="##">SUPCON Newsletter December 2010</a></div>
                        	<div class="resource_size">File size : 16.2M</div>
                        	<div class="update_time">Update Time : 2015/02/04</div>
                        	<div class="resource_download clearfix">
                            	<a class="download fl" href="#">Download</a>
                                <div class="resourse_share fl">
                                    <div class="bdsharebuttonbox">
                                        <a title="分享到一键分享" href="javascript" class="bds_mshare" data-cmd="mshare">Share</a>
                                    </div> 
                                </div>
                            </div>
                        </div>
            		</li>
            		<li class="clearfix">
            			<div class="resource_left fl"><a href="##"><img src="img/resourece_pic.jpg" width="156" height="205" />
                        	<div class="type_icon">JPG</div>
                            
                        </a></div>
            			<div class="resoucre_right fr">
                        	<div class="resource_sub_name"><a href="##">SUPCON Automation Solution For …</a></div>
                        	<div class="resource_size">File size : 16.2M</div>
                        	<div class="update_time">Update Time : 2015/02/04</div>
                        	<div class="resource_download clearfix">
                            	<a class="download fl" href="#">Download</a>
                                <div class="resourse_share fl">
                                    <div class="bdsharebuttonbox">
                                        <a title="分享到一键分享" href="javascript" class="bds_mshare" data-cmd="mshare">Share</a>
                                    </div> 
                                </div>
                            </div>
                        </div>
            		</li>
            		<li class="clearfix">
            			<div class="resource_left fl"><a href="##"><img src="img/resourece_pic.jpg" width="156" height="205" />
                        	<div class="type_icon">JPG</div>
                        </a></div>
            			<div class="resoucre_right fr">
                        	<div class="resource_sub_name"><a href="##">SUPCON Newsletter December 2010</a></div>
                        	<div class="resource_size">File size : 16.2M</div>
                        	<div class="update_time">Update Time : 2015/02/04</div>
                        	<div class="resource_download clearfix">
                            	<a class="download fl" href="#">Download</a>
                                <div class="resourse_share fl">
                                    <div class="bdsharebuttonbox">
                                        <a title="分享到一键分享" href="javascript" class="bds_mshare" data-cmd="mshare">Share</a>
                                    </div> 
                                </div>
                            </div>
                        </div>
            		</li>
            	</ul>
            </div>
    	</div>
    </div>    
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/form.js"></script>

</body>
</html>