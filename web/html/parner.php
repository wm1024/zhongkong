<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="sub_cont">
    	<div class="container">
 			<div class="milestone_title parner_title">We are expecting your join<br />& become one of our partners</div>
            <div class="parner_txt">At SUPCON, we partner with various global suppliers, system integrators, agents, distributers, etc., to expand our<br />
 				globalization and localization strategy. We welcome the partnership and are willing to share our technologies and<br /> 
				resources with you for our mutual growth in a broader market.
			</div>
            <div class="parner_subtit">SUPCON Strategic Partners & Global Suppliers</div>
            <div class="parner_sildeBox">
            	<div class="hd"></div>
            	<div class="bd">
                	<div class="ulWrap">
                		<?php for($i=0;$i<5;$i++) { ?>
                        <ul class="clearfix">
                        	<?php for($j=0;$j<12;$j++) { ?>
                            <li><img src="img/parner_pic<?php echo($j+1); ?>.jpg" width="220" height="90" /></li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                	</div>
                </div>	
            </div>
            <div class="parner_subtit">Become Our Partner</div>
            <div class="parner_tb">
            	<table width="100%">
            		<tr>
                    	<td width="480"><div class="must">*</div>Your name</td>
                        <td><div class="must">*</div>Email</td>
                    </tr>
                    <tr>
                    	<td><input type="text" value="" class="parner_text"  id="parner_name"/></td>
                        <td><input type="text" value="" class="parner_text"  id="parner_email"/></td>
                    </tr>
                    <tr><td colspan="2" style="height:13px;"></td></tr>
            		<tr>
                		<td colspan="2"><div class="must">*</div>Phone number</td>
                	</tr>
                    <tr>
                    	<td colspan="2"><input type="text" value="" class="parner_text parner_phone"  id="parner_phone"/></td>
                    </tr>
                    <tr><td colspan="2" style="height:30px;border-bottom:1px solid #eaeaea;"></td></tr>
                    <tr><td colspan="2" style="height:20px;"></td></tr>
                    <tr>
                    	<td>Country/region</td>
                        <td>Company name</td>          
                    </tr>
                    <tr>
                    	<td><input type="text" value="" class="parner_text"  id="parner_country"/></td>
                        <td><input type="text" value="" class="parner_text"  id="parner_company"/></td>
                    </tr>
                    <tr><td colspan="2" style="height:13px;"></td></tr>
                    <tr>
                    	<td>Address</td>
                        <td>Website</td>          
                    </tr>
                    <tr>
                    	<td><input type="text" value="" class="parner_text"  id="parner_country"/></td>
                        <td><input type="text" value="" class="parner_text"  id="parner_company"/></td>
                    </tr>
                    <tr><td colspan="2" style="height:13px;"></td></tr>
                    <tr>
                    	<td>Company size </td>
                        <td>Company specialties</td>          
                    </tr>
                    <tr>
                    	<td><input type="text" value="" class="parner_text"  id="parner_country"/></td>
                        <td><input type="text" value="" class="parner_text"  id="parner_company"/></td>
                    </tr>
                    <tr><td colspan="2" style="height:13px;"></td></tr>
            		<tr>
                		<td colspan="2">Main industries</td>
                	</tr>
                    <tr>
                    	<td colspan="2"><textarea id="parner_main"></textarea></td>
                    </tr>
                    <tr><td colspan="2" style="height:60px;"></td></tr>
                    <tr>
                    	<td colspan="2" align="center"><input type="submit" value="Submit" class="changeBtn" onclick="parnerForm();" /></td>
                    </tr>
                </table>
            </div>
    	</div>
    </div>    
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/form.js"></script>

</body>
</html>