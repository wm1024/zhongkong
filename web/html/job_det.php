<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
    <div class="applay_pop">
		<div class="applay_layer"></div>
		<div class="applay_main">
        	<div class="applay_title">Apply Now</div>
            <div class="applay_tb">
            	<table width="100%">
                	<tr>
                    	<td width="126" align="right">Apply for job :</td>
                        <td><input type="text" value="Overseas Project Engineer" class="applay_text" id="applay_position" name="position" /></td>
                    </tr>
                    <tr><td colspan="2" style="height:14px;"></td></tr>
                	<tr>
                    	<td align="right">Name :</td>
                        <td><input type="text" value="" class="applay_text" id="applay_name" name="name" /></td>
                    </tr>
                    <tr><td colspan="2" style="height:14px;"></td></tr>
                	<tr>
                    	<td align="right">Country/Region :</td>
                        <td><input type="text" value="" class="applay_text" id="applay_area" name="area" /></td>
                    </tr>
                    <tr><td colspan="2" style="height:14px;"></td></tr>
                	<tr>
                    	<td align="right">Phone number :</td>
                        <td><input type="text" value="" class="applay_text" id="applay_phone" name="phone" /></td>
                    </tr>
                    <tr><td colspan="2" style="height:14px;"></td></tr>
                	<tr>
                    	<td align="right">E-mail :</td>
                        <td><input type="text" value="" class="applay_text" id="applay_email" name="email" /></td>
                    </tr>
                    <tr><td colspan="2" style="height:14px;"></td></tr>
                	<tr>
                    	<td align="right">Upload CV :</td>
                        <td><input type="text" value="" class="applay_text applay_docname" id="applay_docname" name="docment" /><div class="applay_upload fr">Browse<input type="file" value="" class="file_upload" onchange="addDocname(this)" /></div></td>
                    </tr>
                    <tr><td colspan="2" style="height:18px;"></td></tr>
                	<tr>
                    	<td align="right">&nbsp;</td>
                        <td align="right"><input type="submit" value="Apply" class="applayBtn" onclick="return applayForm();" /><input type="reset" value="Cancel" class="applayReset" onclick="closeApplay();" /></td>
                    </tr>
                </table>
        	</div>
        </div>
	</div>
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="sub_cont">
    	<div class="container">
 			<div class="milestone_title job_title">Job opportunities</div>
            <div class="job_det_cont">
            	<div class="job_det_position">Overseas Project Engineer</div>
            	<div class="job_det_info">
                    <p><span>Release date :</span> Jul.02,2015</p>
                    <p><span>Location :</span> Hangzhou</p>
                    <p><span>Quantity :</span> 5</p>
                </div>
                <div class="job_det_description">
                	<div class="description_title">Job Description :</div>
                	<div class="description_txt">
                    	<p>1. Overseas project engineering.</p>
						<p>2. Provide technical solution to overseas project.</p>
                    </div>
                </div>
                <div class="job_det_requirements">
                	<div class="requirements_title">Job Description :</div>
                	<div class="requirements_txt">
                    	<p>1. Bachelor degree or above, major in automation related.</p>
						<p>2. Fluent in both written and oral English.</p>
                        <p>3. Familiar with DCS, SCADA or large-scale PLC is preferred.</p>
                       	<p>4. Process industries background is preferable.</p>
                        <p>5. Working experience in foreign invested company is preferred.</p>
                        <p>6. Self-motivation, strong passion, hard working, flexible and teamwork.</p> 
                    </div>
                </div>
                <div class="ind_news_more job_applay"><a href="javascript:;" onclick="applyNow(this);">Apply Now<i class="ind_more_icon"></i></a></div>
            </div>
    	</div>
    </div>    
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/form.js"></script>

</body>
</html>