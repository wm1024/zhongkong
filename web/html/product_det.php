<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="proDet_imgs">
		<a href="#" class="proDet_prev">
			<div class="tit"><img src="img/milestone_pic.jpg" /><i></i></div>
			<div class="con">
				<p>Webfield ECS-100</p>
				<p><span>DCS/FCS</span></p>
			</div>
		</a>
		<a href="#" class="proDet_next">
			<div class="tit"><img src="img/milestone_pic.jpg"><i></i></div>
			<div class="con">
				<p>Webfield ECS-100</p>
				<p><span>DCS/FCS</span></p>
			</div>
		</a>
		<div class="container">
			<div id="pro_slider" class="pro_slider fl">
				<div class="bd">
					<ul>
						<li><a><img src="img/ind_product_pic.png" /></a></li>
						<li><a><img src="img/img.jpg" /></a></li>
						<li><a><img src="img/news_banner.jpg" /></a></li>
					</ul>
				</div>
				<a class="prev" href="javascript:void(0)"></a>
				<a class="next" href="javascript:void(0)"></a>
			</div>
			<div class="pro_tit fl">
				<div class="cn">LN81/82 Series</div>
				<div class="en">Top Guided Single-seated Control Valve</div>
			</div>
		</div>
	</div>
	<div class="container proDet">
		<div class="proDet_intro">
			<div class="tit">Overview</div>
			<div class="main">
				<p>As one of the WebField Series control system, ECS-700 system is a large-scale high-end control system designed and developed for large-scale integrated units based on summary of wide application of JX-300XP, ECS-100 and other WebField series control systems. It blends advanced control technologies, open field bus standards, industry Ethernet security technology and so on, providing users with a reliable and open control platform. </p>
				<img src="img/img1.jpg" />
			</div>
		</div>
		<div class="proDet_intro">
			<div class="tit">System Features</div>
			<div class="main">
				<p>● Security </p>
				<p>The security and anti-interference performance of ECS-700 system conforms to international standards in the industrial environment. The entire system including the power supply module, controller,  I/O module and communication bus fully supports redundancy.  IO module has a channel-level fault diagnosis and has complete fault safety function. The system has the configuration single-point online download function to ensure safe, continuous and stable operation at field. </p>
				<p>●  Openness  </p>
				<p>Incorporating various standard software and hardware interface, ECS-700 system is  compatible with digital signals and analog signals conforming to field bus standard, and provides open interfaces that comply with HART, FF, PROFIBUS, MODBUS,EPA and other standard protocols. </p>
				<img src="img/img2.jpg" />
			</div>
		</div>
		<div class="proDet_intro">
			<div class="tit">parameters</div>
			<div class="main">
				<img src="img/img3.jpg" />
			</div>
		</div>
		
		<?php include("include/explore_more.php")?>
	</div>
	
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
	jQuery(".pro_slider").slide({mainCell:".bd ul",autoPlay:false,effect:'left'});
</script>
</body>
</html>