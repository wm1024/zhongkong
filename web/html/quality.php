<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="sub_cont">
    	<div class="container">
 			<div class="milestone_title quality_title">Quality No.1</div>
            <div class="quality_txt">
            	SUPCON has implemented an extensive Quality Assurance (QA) system to ensure the high quality<br />conforming to all processes and procedures required by ISO 9001 and 14001 standards.
            </div>
            <div class="quality_list">
            	<ul class="clearfix">
            		<li>
                    	<div class="quality_pic"><a href="quality_list.php"><img src="img/quality_pic1.jpg" width="116" height="75" /></a></div>
                        <div class="quality_list_tit">QA Management</div>
                        <div class="quality_list_txt">ISO 9001, ISO 14001, OHSMS 18000, CMMI<br />Level 5, etc.</div>
                    	<div class="ind_news_more quality_more"><a href="quality_list.php">Explore more<i class="ind_more_icon"></i></a></div>
                    </li>
                    <li class="line"></li>
            		<li>
                    	<div class="quality_pic"><a href="quality_list.php"><img src="img/quality_pic2.jpg" width="116" height="75" /></a></div>
                        <div class="quality_list_tit">Product Compliance</div>
                        <div class="quality_list_txt">CE, EAC, TUV SIL3, G3, Achilles Level II,<br />CCS, etc.</div>
                    	<div class="ind_news_more quality_more"><a href="quality_list.php">Explore more<i class="ind_more_icon"></i></a></div>
                    </li>
            	</ul>
            </div>
    	</div>
    </div>    
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</body>
</html>