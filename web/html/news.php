<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="news_banner">
		<a href="#">
			<div class="title">SUPCON DCS Applied in Gen-IV Nuclear 
Reactor System</div>
			<div class="date">Release time：2016-03-09</div>
			<div class="con">Dec 21, 2015 - SUPCON successfully won the contract of instrument control system design verification platform for Sodium-cooled Fast Reactor (SFR) of China ……</div>
		</a>
	</div>
	<div class="container">
		<div class="news1">
			<div class="news_tit">News Highlights<a href="#" class="fr">More<i></i></a></div>
			<ul class="news_list clearfix">
				<li><a href="#"><div class="ind_news_pic"><img src="img/news_pic1.jpg" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
					<div class="ind_news_name"><a href="#" title="SUPCON Awarded 2 Honors for Internet+ Industry">SUPCON Awarded 2 Honors for Internet+ Industry</a></div>
					<div class="ind_news_time">December 14, 2015</div>
					<div class="ind_news_info"><a href="#">Nov 12, SUPCON welcomed a 20-people delegation led by Zhang …</a></div>
				</li>	
				<li><a href="#"><div class="ind_news_pic"><img src="img/news_pic2.jpg" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
					<div class="ind_news_name"><a href="#" title="Vice Chairman of Zhejiang Association for Science and …">Vice Chairman of Zhejiang Association for Science and …</a></div>
					<div class="ind_news_time">December 14, 2015</div>
					<div class="ind_news_info"><a href="#">Nov 4, SUPCON welcomed the visit of Zhu Guangyu, Director of …</a></div>
				</li>	    
				<li><a href="#"><div class="ind_news_pic"><img src="img/news_pic3.jpg" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
					<div class="ind_news_name"><a href="#" title="Vice Chairman of Zhejiang Association for Science and …">Vice Chairman of Zhejiang Association for Science and …</a></div>
					<div class="ind_news_time">December 14, 2015</div>
					<div class="ind_news_info"><a href="#">Nov 11, SUPCON welcomed Zhang Hongjian, Vice President of …</a></div>
				</li>	 
			</ul>
		</div>
		
		<div class="news2">
			<div class="news_tit">News<a href="#" class="fr">More<i></i></a></div>
			<a href="#" class="news2_hot">
				<img src="img/milestone_pic.jpg" class="fl"/>
				<div class="main fl">
					<div class="title">SUPCON HD5500 Series Isolated Barrier Passed BV SIL Certificate</div>
					<div class="date">Release time：2016-03-09</div>
					<div class="con">March 3, SUPCON was awarded the BV SIL Certificate (Bureau Veritas Safety Integrity Level Certificate) for its new generation of HD5500r…</div>
					<div class="more fr">More<i></i></div>
				</div>
			</a>
			<ul class="news_list clearfix">
				<li><a href="#"><div class="ind_news_pic"><img src="img/news_pic1.jpg" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
					<div class="ind_news_name"><a href="#" title="SUPCON Awarded 2 Honors for Internet+ Industry">SUPCON Awarded 2 Honors for Internet+ Industry</a></div>
					<div class="ind_news_time">December 14, 2015</div>
					<div class="ind_news_info"><a href="#">Nov 12, SUPCON welcomed a 20-people delegation led by Zhang …</a></div>
				</li>	
				<li><a href="#"><div class="ind_news_pic"><img src="img/news_pic2.jpg" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
					<div class="ind_news_name"><a href="#" title="Vice Chairman of Zhejiang Association for Science and …">Vice Chairman of Zhejiang Association for Science and …</a></div>
					<div class="ind_news_time">December 14, 2015</div>
					<div class="ind_news_info"><a href="#">Nov 4, SUPCON welcomed the visit of Zhu Guangyu, Director of …</a></div>
				</li>	    
				<li><a href="#"><div class="ind_news_pic"><img src="img/news_pic3.jpg" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
					<div class="ind_news_name"><a href="#" title="Vice Chairman of Zhejiang Association for Science and …">Vice Chairman of Zhejiang Association for Science and …</a></div>
					<div class="ind_news_time">December 14, 2015</div>
					<div class="ind_news_info"><a href="#">Nov 11, SUPCON welcomed Zhang Hongjian, Vice President of …</a></div>
				</li>	 
			</ul>
		</div>
	</div>
	<div class="news3">
		<div class="container"><div class="news_tit">Newsletter<a href="#" class="fr">More<i></i></a></div></div>
		<div class="main">
			<a href="#">March 2016</a>
			<div class="news_subscribe"><input type="text" class="text fl" placeholder="Enter your e-mail" id="email"/><input type="submit" class="btn fl" value="Subscribe" onclick="return subscribeForm()"/></div>
		</div>
	</div>
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/form.js"></script>

</body>
</html>