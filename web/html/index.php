<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>

<div class="mainbody">
	<div class="ind_video">
		<div class="ind_video_layer"></div>
        <div class="ind_video_main"><div id="a1"></div></div>
	</div>
	<div class="head_fixed">
    	<div class="container clearfix">
    		<div class="logo fl"><a href="index.php"><img src="images/logo.png" width="154" height="25" /></a></div>
            <div class="head_nav fr">
            	<ul class="clearfix">
                	<li><a href="about.php">About SUPCON</a></li>
                    <li><a href="news.php">News</a></li> 
                    <li><a href="product.php">Products</a></li>
                    <li><a href="parner.php">Partner</a></li>
                    <li style="margin-right:0;">
                    	<div class="head_search">
                        	<input type="submit" value="" class="search_text" />
                      	</div>
                    	<div class="head_search_word">
                        	<input type="text" value="Search …" _reg="Search …" class="search_word" />
                            <i class="head_search_close"></i>
                        </div>
                    </li>
                    <li style="margin-right:0;"><div class="head_menu"><i class="head_menu_icon"></i></div></li>
                </ul>
            </div>
    	</div>
    </div>
    <div class="head_fixed_erify">
    	<ul>
        	<li class="close">Menu<i class="erify_close"></i></li> 
    		<li><div class="fixed_tabs"><i class="erify_right"></i></div>
            	<a class="fixed_nav" href="about.php">About us</a>
            	<div class="fixed_sub_menu">
            		<a href="#">Company overview</a>
            		<a href="#">Milestone</a>
                    <a href="#">Quality assurance</a>
                    <a href="#">Careers</a>
                    <a href="#">Contact us</a>
            	</div>
            </li>
            <li><div class="fixed_tabs"><i class="erify_right"></i></div>
            	<a class="fixed_nav" href="product.php">Products</a>
            	<div class="fixed_sub_menu">
            		<a href="#">SPlant Total Solution</a>
            		<a href="#">Instruments</a>
                    <a href="#">PCS</a>
                    <a href="#">APC & MES</a>
                    <a href="#">ERP</a>
            	</div>
            </li>
            <li><a class="fixed_nav" href="industries.php">Industries</a></li>
            <li><a class="fixed_nav" href="parner.php">Partner</a></li>
            <li><div class="fixed_tabs"><i class="erify_right"></i></div>
            	<a class="fixed_nav" href="service_life.php">Service</a>
            	<div class="fixed_sub_menu">
            		<a href="#">Life-cycle service</a>
            		<a href="#">Submit your requirements</a>
            	</div>
            </li>
            <li><div class="fixed_tabs"><i class="erify_right"></i></div>
            	<a class="fixed_nav" href="resources.php">Resources</a>
            	<div class="fixed_sub_menu">
            		<a href="#">Industry Solution</a>
            		<a href="#">Catalog</a>
                    <a href="#">Brochure</a>
                    <a href="#">Success Story</a>
                    <a href="#">Newsletter</a>
                    <a href="#">Video</a>
            	</div>
            </li>
            <li><div class="fixed_tabs"><i class="erify_right"></i></div>
            	<a class="fixed_nav" href="news.php">News</a>
            	<div class="fixed_sub_menu">
            		<a href="#">Highlights</a>
            		<a href="#">News</a>
                    <a href="#">Newsletter</a>
            	</div>
                <i class="erify_right"></i>
            </li>
    	</ul>
    </div>
    <div class="banner">
        <div class="hd">
            <ul class="clearfix"></ul>
        </div>
        <div class="bd">
            <ul>
                <?php for($i=0;$i<4;$i++) { ?>	
                <li><a href="#" style="background-image:url(img/banner.jpg);">
                    <div class="banner_txt">
                        <div class="banner_title">VISION</div>
                        <div class="banner_subtit">A World's Leading Smart Enterprise</div>
                        <div class="banner_info">We bring flexible technologies and solutions to help boost your business, increase your plant’s efficiency and ultimately create you with new benefits.</div>
                    </div>
                </a></li>
                <?php } ?>		
            </ul>
        </div>
        <div class="ind_header">
        	<div class="container">
        		<div class="ind_top_logo"><a href="index.php"><img src="images/logo.png" width="154" height="25" /></a></div>
        		<div class="ind_top_lan">
                	<ul class="clearfix">
                    	<li><a href="##" class="lan_ch">中文</a></li>
                        <li class="line"></li>
                        <li><a href="##" class="lan_en">EN</a></li>
                    	<li class="add_adv"><a href="javascript:;" onclick="join_favorite(window.location,document.title)"><i class="add_adv_icon"></i>Add bookmark</a></li>
                    </ul>
                </div>
                <div class="clearfix">
                    <div class="ind_head_nav fl">
                        <ul class="clearfix">
                            <li><a href="about.php">About SUPCON</a></li>
                            <li><a href="news.php">News</a></li> 
                            <li><a href="product.php">Products</a></li>
                            <li><a href="parner.php">Partner</a></li>
                    	</ul>
                    </div>
                    <div class="ind_head_right fr">
                    	<div class="clearfix">
                        	<div class="ind_head_search fl">
                                <input type="text" value="Search Now" _reg="Search Now" class="ind_head_word" />
                                <input type="submit" value="" class="ind_search" />
                            </div>
                            <div class="ind_head_menu fr"><i class="head_menu_icon"></i>Menu</div>
                        </div> 
                    </div>
                </div>
        	</div>
        </div>
        <div class="banner_video"><a href="javascript:;" onclick="openVideo();">Corporate Video</a></div>
    </div>
	<div class="ind_about">
    	<div class="container">
        	<div class="ind_title">ABOUT SUPCON</div>
            <div class="ind_about_txt">
                Founded in March 1993, SUPCON is one of China's leading providers of automation and information technology,<br /> 
                products and solutions. SUPCON has grown rapidly on the back of cutting-edge systems for the development of innovative<br />
                technology products, and to date has delivered over 17,000 control systems to a wide range of process industries…
            </div>	
        </div>
    </div>
    <div class="ind_news">
    	<div class="container">
        	<div class="ind_title ind_news_title">NEWS</div>
        	<ul class="news_list clearfix">
            	<li><a href="news_det.php"><div class="ind_news_pic"><img src="img/news_pic1.jpg" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
                	<div class="ind_news_name"><a href="news_det.php" title="SUPCON Awarded 2 Honors for Internet+ Industry">SUPCON Awarded 2 Honors for Internet+ Industry</a></div>
                    <div class="ind_news_time">December 14, 2015</div>
                    <div class="ind_news_info"><a href="news_det.php">Nov 12, SUPCON welcomed a 20-people delegation led by Zhang …</a></div>
                </li>	
            	<li><a href="news_det.php"><div class="ind_news_pic"><img src="img/news_pic2.jpg" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
                	<div class="ind_news_name"><a href="news_det.php" title="Vice Chairman of Zhejiang Association for Science and …">Vice Chairman of Zhejiang Association for Science and …</a></div>
                    <div class="ind_news_time">December 14, 2015</div>
                    <div class="ind_news_info"><a href="news_det.php">Nov 4, SUPCON welcomed the visit of Zhu Guangyu, Director of …</a></div>
                </li>	    
            	<li><a href="news_det.php"><div class="ind_news_pic"><img src="img/news_pic3.jpg" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
                	<div class="ind_news_name"><a href="news_det.php" title="Vice Chairman of Zhejiang Association for Science and …">Vice Chairman of Zhejiang Association for Science and …</a></div>
                    <div class="ind_news_time">December 14, 2015</div>
                    <div class="ind_news_info"><a href="news_det.php">Nov 11, SUPCON welcomed Zhang Hongjian, Vice President of …</a></div>
                </li>	 	
            </ul>
            <div class="ind_news_more"><a href="news.php">Explore more<i class="ind_more_icon"></i></a></div>
        </div>
    </div>
	<div class="ind_product" style="background:url(img/ind_product_bg.jpg) no-repeat center top;">
		<div class="container clearfix">
    		<div class="ind_product_left fl">
            	<div class="ind_product_name">SUPCON SPlant</div>
            	<div class="ind_product_info">Total Solution for Process Automation</div>
                <div class="ind_news_more ind_product_more"><a href="#">Product porfolio<i class="ind_more_icon"></i></a></div>
            </div>
            <div class="ind_product_sildeBox fr">
            	<div class="hd">
                	<ul class="clearfix"></ul>
                </div>
                <div class="bd">
                	<ul>
                    	<?php for($i=0;$i<5;$i++) { ?>
                    	<li><a href="##"><img src="img/ind_product_pic.png" width="200" height="451" /></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
    	</div>
    </div>
	<div class="ind_partner">
    	<div class="container">
            <div class="ind_title ind_partner_title">PARTNER</div>
            <div class="ind_partner_txt">JOIN & SUCCEED WITH SUPCON GLOBAL PARTNERSHIP PROGRAM</div>
            <div class="ind_news_more ind_partner_more"><a href="parner.php">Explore more<i class="ind_more_icon"></i></a></div>
        </div>
    </div>
<?php include("include/footer.php")?>
</div>

<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script src="ckplayer/ckplayer.js" type="text/javascript"></script>
<script type="text/javascript">
	var flashvars={
		p:0,
		e:1,
		ht:'20',
		hr:'http://www.ckplayer.com'
		};
	var video=['http://movie.ks.js.cn/flv/other/1_0.mp4->video/mp4'];
	var support=['all'];
	CKobject.embedHTML5('a1','ckplayer_a1',600,400,video,flashvars,support);	
	

$(document).ready(function() {
	
	var length=$(".banner .hd ul li").length;
	if(length==1){
		$(".banner .hd ul").hide();
	}

}); 


</script>

</body>
</html>