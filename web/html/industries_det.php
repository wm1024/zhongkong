<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<div class="ind_video">
		<div class="ind_video_layer"></div>
        <div class="ind_video_main"><div id="a1"></div></div>
	</div>
    <div class="indust_ccr_pop">
    	<div class="indust_ccr_layer"></div>
    </div>
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="indust_det_top" style="background:url(img/industries_det_top.jpg) no-repeat center top;">
    	<div class="container">
    		<div class="indust_det_top_title">Petrochemical &<br />Refinery</div>
    		<div class="indust_det_top_info">As a leader in the Petrochemical & Refinery industry, our industry-specific solutions create a mode of intelligent plant to increase efficiency and reduce hidden risks for your plant profitability. We worked with tens of million-ton refineries to offer plant-wide automation solutions based on WebFieldTM ECS-700 control systems and intelligent instruments, our customers include SINOPEC, CNPC, CNOOC and a number of other oil giants over the world.</div>
        </div>
    </div>
    <div class="indust_tabs">
    	<div class="indust_button">
    		<a class="prev" rel="0"><i class="indust_prev"></i></a>
    		<a class="next" rel="0"><i class="indust_next"></i></a>
    	</div>
        <div class="container indust_det_cont">
            <div class="indust_nav">
                <div class="indust_det_list">
            		<div class="indust_det_list_name">CCR Large Screen Solution</div>
            		<div class="indust_ccr_sideBox">	
            			<div class="hd"><ul class="clearfix"></ul></div>
                    	<div class="bd">
                    		<div class="ulWrap">
                            	<?php for($j=0;$j<5;$j++) { ?>
                            	<ul>
                                	<?php for($i=0;$i<10;$i++) { ?>
                                	<li><a href="javascript:;" onclick="showBigpic(this);"><div class="indust_ccr_pic"><img src="img/industries_ccr_pic.jpg" height="188" /></div>
                                    	<div class="indust_yin"><i class="indust_big"></i></div>
                                    </a></li>
                                	<?php } ?>
                                </ul>
                                <?php } ?>
                            </div>
                    	</div>
                    </div>	
            	</div>
                <div class="indust_det_list">
            		<div class="indust_det_list_name">Typical Clients</div>
            		<div class="indust_logo_sideBox">	
            			<div class="hd"><ul class="clearfix"></ul></div>
                    	<div class="bd">
                    		<div class="ulWrap">
                            	<?php for($j=0;$j<5;$j++) { ?>
                            	<ul>
                                	<?php for($i=0;$i<12;$i++) { ?>
                                	<li><a href="javascript:;"><img src="img/indust_logo_pic.jpg" width="220" height="90" />
                                    </a></li>
                                	<?php } ?>
                                </ul>
                                <?php } ?>
                            </div>
                    	</div>
                    </div>	
            	</div>
            </div>	
        </div>
    </div>
	<div class="indust_video">
    	<div class="indust_video_title">Video</div>    
		<div class="indust_video_main">
			<div class="indust_video_pic">
				<img src="img/indust_video_pic.jpg" width="834" height="467" />
                <div class="indust_video_pic_yin"><a href="javascript:;" onclick="openVideo()"></a></div>
			</div>
		</div>
	</div>
	<div class="indust_more" style="background:url(img/indust_more_bg.jpg) no-repeat center 200px;">
    	<div class="indust_more_cont">
			<?php include("include/explore_more.php")?>
        </div>
    </div>
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script src="ckplayer/ckplayer.js" type="text/javascript"></script>
<script type="text/javascript">
	var flashvars={
		p:0,
		e:1,
		ht:'20',
		hr:'http://www.ckplayer.com'
		};
	var video=['http://movie.ks.js.cn/flv/other/1_0.mp4->video/mp4'];
	var support=['all'];
	CKobject.embedHTML5('a1','ckplayer_a1',600,400,video,flashvars,support);	
</script>
</body>
</html>