<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
	<?php include("include/mycenter_top.php") ?>  	
    <div class="mycenter_cont">
    	<div class="container">
    		<?php include("include/mycenter_left.php") ?>	
    		<div class="mycenter_right fr">
    			<div class="mycenter_right_title">Edit profile</div>
    			<div class="editprofile_tb">
					<table width="100%">
						<tr>
                        	<td style="height:30px;">You name</td>
                        </tr>
                        <tr>
                        	<td><input type="text" value="" class="edit_text" id="edit_name" /></td>
                        </tr>
                        <tr><td style="height:7px;"></td></tr>
						<tr>
                        	<td style="height:30px;">Country / region</td>
                        </tr>
                        <tr>
                        	<td><input type="text" value="" class="edit_text" id="edit_country" /></td>
                        </tr>
                        <tr><td style="height:7px;"></td></tr>
						<tr>
                        	<td style="height:30px;">Company</td>
                        </tr>
                        <tr>
                        	<td><input type="text" value="" class="edit_text" id="edit_company" /></td>
                        </tr>
                        <tr><td style="height:7px;"></td></tr>
						<tr>
                        	<td style="height:30px;">Phone number</td>
                        </tr>
                        <tr>
                        	<td><input type="text" value="" class="edit_text" id="edit_phone" /></td>
                        </tr>
                        <tr><td style="height:56px;"></td></tr>
                        <tr>
                        	<td><input type="submit" value="Submit" class="changeBtn editBtn" onclick="return editForm();" /></td>
                        </tr>
                	</table>
                </div>            
    		</div>
    	</div>
    </div>
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/form.js"></script>

</body>
</html>