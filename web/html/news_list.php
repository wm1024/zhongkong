<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="sub_cont">
		<div class="container">
			<div class="newsList_title">News Highlights</div>
			<div class="newsList_search">
				<input type="text" placeholder="Keyword" class="text fl"/><input type="submit" value="Search" class="btn fr"/>
			</div>
			<ul class="newsList">
				<?php for($i=0;$i<5;$i++){?>
				<li><a href="news_det.php" class="clearfix">
					<div class="date fl">
						<div class="day">21</div>
						<div class="mon">Dec.</div>
					</div>
					<div class="main fl">
						<div class="title">SUPCON Fluid Awarded Hangzhou City Enterprise Technology Center</div>
						<div class="con">March 4, SUPCON Fluid Technology Co., Ltd. was awarded “Hangzhou City Enterprise Technology Center” in the Hangzhou Enterprise Technology Innovation and Development Conference. 
The conference was held by Hangzhou Economy and Information Technology Commission. Over 200 government leaders.…</div>
					</div>
				</a></li>
				<?php }?>
			</ul>
		</div>
	</div>
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</body>
</html>