<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="sub_banner" style="background:url(img/resources_banner.jpg) no-repeat center top;">
    	<div class="container"><div class="sub_banner_txt">Resources</div></div>
    </div> 
	<div class="resource_cont">
		<div class="container">
    		<div class="sub_top_search clearfix">
    			<input type="text" value="Search Now" _reg="Search Now" class="keyword_text fl" /> 
    			<input type="submit" value="Search" class="subBtn fl" />
                <p class="fl">or</p>
                <a class="fr" href="advanced_search.php">Advanced Search</a>
    		</div>
    		<div class="resource_list">
            	<ul class="clearfix">
            		<li><a href="resource_list.php">	
            			<div class="resource_type">
                        	<div class="resource_icon1"></div>
                            <div class="type_nmae">All</div>
                        </div>
                        <div class="resourec_yin"></div>
            		</a></li>
            		<li><a href="resource_list.php">	
            			<div class="resource_type">
                        	<div class="resource_icon2"></div>
                            <div class="type_nmae">Brochure</div>
                        </div>
                        <div class="resourec_yin"></div>
            		</a></li>
            		<li><a href="resource_list.php">	
            			<div class="resource_type">
                        	<div class="resource_icon3"></div>
                            <div class="type_nmae">Product Catalog</div>
                        </div>
                        <div class="resourec_yin"></div>
            		</a></li>
            		<li><a href="resource_list.php">	
            			<div class="resource_type">
                        	<div class="resource_icon4"></div>
                            <div class="type_nmae">Typical Reference</div>
                        </div>
                        <div class="resourec_yin"></div>
            		</a></li>
            		<li><a href="resource_list.php">	
            			<div class="resource_type">
                        	<div class="resource_icon5"></div>
                            <div class="type_nmae">Newsletter</div>
                        </div>
                        <div class="resourec_yin"></div>
            		</a></li>
            		<li><a href="video.php">	
            			<div class="resource_type">
                        	<div class="resource_icon6"></div>
                            <div class="type_nmae">Video</div>
                        </div>
                        <div class="resourec_yin"></div>
            		</a></li>
            	</ul>
            </div>
    	</div>
    </div>    
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</body>
</html>