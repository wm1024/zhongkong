<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
    <?php include("include/top_link.php")?>
	<div class="sub_cont">
    	<div class="container">
    		<div class="sitmap_title">Sitemap</div>
    		<div class="sitmap_list">
    			<ul class="clearfix">
        			<li>
                    	<a class="top" href="about.php">About us</a>
                    	<a href="about.php">Company Overview<i class="sitmap_arrow"></i></a>
                        <a href="milestone.php">Milestone<i class="sitmap_arrow"></i></a>
                        <a href="quality.php">Quality Assurance<i class="sitmap_arrow"></i></a>
                        <a href="careers.php">Careers<i class="sitmap_arrow"></i></a>
                        <a href="contact.php">Contact us<i class="sitmap_arrow"></i></a>
                    </li>
                   	<li>
                    	<a class="top" href="product.php">Products</a>
                        <a href="##">SPlant Total Solution<i class="sitmap_arrow"></i></a>
                        <a href="##">Instruments<i class="sitmap_arrow"></i></a>
                        <a href="##">PCS<i class="sitmap_arrow"></i></a>
                        <a href="##">APC & MES<i class="sitmap_arrow"></i></a>
                        <a href="##">ERP<i class="sitmap_arrow"></i></a>
                    </li>
                    <li style="margin-right:0;">
                    	<a class="top" href="industries.php">Industries</a>
                        <a href="industries.php">Industries<i class="sitmap_arrow"></i></a>
                    	<a style="margin-top:40px;" class="top" href="parner.php">Partner</a>
                        <a href="parner.php">Partner<i class="sitmap_arrow"></i></a>
                    </li>
                    <li>
                    	<a class="top" href="service_life.php">Service</a>
                        <a href="service_life.php">Life-cycle service<i class="sitmap_arrow"></i></a>
                        <a href="service_request.php">Submit your requirements<i class="sitmap_arrow"></i></a>
                    </li>
                    <li>
                    	<a class="top" href="resources.php">Resources</a>
                        <a href="##">Industry Solution<i class="sitmap_arrow"></i></a>
                        <a href="##">Catalog<i class="sitmap_arrow"></i></a>
                        <a href="##">Brochure<i class="sitmap_arrow"></i></a>
                        <a href="##">Success Story<i class="sitmap_arrow"></i></a>
                        <a href="##">Newsletter<i class="sitmap_arrow"></i></a>
                        <a href="video.php">Video<i class="sitmap_arrow"></i></a>
                    </li>
                    <li style="margin-right:0;">
                    	<a class="top" href="news.php">News</a>
                        <a href="##">Highlights<i class="sitmap_arrow"></i></a>
                        <a href="##">News<i class="sitmap_arrow"></i></a>
                        <a href="##">Newsletter<i class="sitmap_arrow"></i></a>
                    </li>
        		</ul>
        	</div>
    	</div>
    </div>
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</body>
</html>