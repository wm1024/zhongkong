<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<div class="ind_video">
		<div class="ind_video_layer"></div>
        <div class="ind_video_main"><div id="a1"></div></div>
	</div>
	<?php include("include/login_pop.php")?>
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="sub_cont">
    	<div class="container">
        	<div class="clearfix">	
        		<a class="resource_login video_login fr" href="javascript:;" onclick="openLogin();"><i class="login"></i>Log in</a>
            </div>
 			<div class="milestone_title video_title">Video</div>
            <div class="sub_top_search video_top_search clearfix">
    			<input type="text" value="Search Now" _reg="Search Now" class="keyword_text fl" /> 
    			<input type="submit" value="Search" class="subBtn fl" />
                <p class="fl">or</p>
                <a class="fr" href="advanced_search.php">Advanced Search</a>
    		</div>
            <div class="video_list">
            	<ul class="clearfix">
                	<?php for($i=0;$i<6;$i++) { ?>
                	<li>
                    	<div class="video_year">2015</div>
						<div class="video_date fl">Feb. 04</div>
                        <div class="clearfix"></div>
                        <div class="video_pic"><a href="#"><img src="img/video_pic.jpg" width="294" height="162" /></a></div>
                        <div class="video_name"><a href="#">SUPCON Regulating valve Control valve</a></div>	
                        <div class="video_size">File size : 16.2M</div>
						<div class="resource_download video_watch clearfix">
                            <a class="download fl" href="javascript:;" onclick="openvideo(this);">Watch Online</a>
                            <div class="resourse_share fl">
                                <div class="bdsharebuttonbox">
                                    <a title="分享到一键分享" href="javascript" class="bds_mshare" data-cmd="mshare">Share</a>
                                </div> 
                            </div>
                    	</div>                
                	</li>
                    <?php } ?>
                </ul>
            </div>
    	</div>
    </div>    
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/form.js"></script>
<script type="text/javascript" src="ckplayer/ckplayer.js"></script>
<script type="text/javascript">


//媒体中心视频
function openvideo(obj){

	//var url=$(obj).attr("rel");
	var url="http://movie.ks.js.cn/flv/other/1_0.mp4";
	$(".ind_video").show();

	function loadedHandler(){
			if(CKobject.getObjectById('ckplayer_a1').getType()){//说明使用html5播放器
				CKobject.getObjectById('ckplayer_a1').addListener('play',playHandler);
				CKobject.getObjectById('ckplayer_a1').addListener('pause',pauseHandler);
			}
			else{
				CKobject.getObjectById('ckplayer_a1').addListener('play','playHandler');
				CKobject.getObjectById('ckplayer_a1').addListener('pause','pauseHandler');
			}
		}
		function playHandler(){
		
		}
		function pauseHandler(){
	
		}
	
		function playvideo(){
			
			var flashvars={
				f:url,
				c:0,
				p:1,
				e:0,
				loaded:'loadedHandler'
				};
			CKobject.embed('ckplayer/ckplayer.swf','a1','ckplayer_a1',600,400,false,flashvars,'http://movie.ks.js.cn/flv/other/1_0.mp4->video/mp4');
	
		}
	
		playvideo();
		
}	
</script>
</body>
</html>