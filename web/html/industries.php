<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="sub_banner" style="background:url(img/industries_banner.jpg) no-repeat center top;">
    	<div class="container"><div class="sub_banner_txt">Industries</div></div>
    </div> 
	<div class="indust_cont">
    	<div class="container">
    		<ul class="indust_list clearfix">
    			<li><a href="industries_det.php">
                	<div class="indust_pic">
                    	<img src="img/indust_pic1.png" width="72" />
                    </div>
                    <div class="indust_txt">Petrochemical<br />& Refinery</div>
                </a></li>
    			<li><a href="industries_det.php">
                	<div class="indust_pic">
                    	<img src="img/indust_pic2.png" width="72" />
                    </div>
                    <div class="indust_txt">Oil & Gas</div>
                </a></li>
    			<li><a href="industries_det.php">
                	<div class="indust_pic">
                    	<img src="img/indust_pic3.png" width="72" />
                    </div>
                    <div class="indust_txt">Chemical</div>
                </a></li>
    			<li><a href="industries_det.php">
                	<div class="indust_pic">
                    	<img src="img/indust_pic4.png" width="72" />
                    </div>
                    <div class="indust_txt">Power</div>
                </a></li>
    			<li><a href="industries_det.php">
                	<div class="indust_pic">
                    	<img src="img/indust_pic5.png" width="72" />
                    </div>
                    <div class="indust_txt">Building Material</div>
                </a></li>
    			<li><a href="industries_det.php">
                	<div class="indust_pic">
                    	<img src="img/indust_pic6.png" width="72" />
                    </div>
                    <div class="indust_txt">Pulp & Paper</div>
                </a></li>
    			<li><a href="industries_det.php">
                	<div class="indust_pic">
                    	<img src="img/indust_pic7.png" width="72" />
                    </div>
                    <div class="indust_txt">Metallurgy</div>
                </a></li>
    			<li><a href="industries_det.php">
                	<div class="indust_pic">
                    	<img src="img/indust_pic8.png" width="72" />
                    </div>
                    <div class="indust_txt">Others</div>
                </a></li>
    		</ul>
    	</div>
    </div>    
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</body>
</html>