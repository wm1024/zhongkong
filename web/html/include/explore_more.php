<div class="explore_more">
				<div class="tit">EXPLORE MORE</div>
				<ul class="explore_nav clearfix">
					<li class="on"><i class="icon1"></i>Products</li>
					<li><i class="icon2"></i>Industries</li>
					<li><i class="icon3"></i>Download</li>
					<li><i class="icon4"></i>News</li>
				</ul>
				<div class="explore_main">
					<div class="path" style="display:block;">
						<ul class="explore_pro clearfix">
							<?php for($i=0;$i<4;$i++){?>
							<li><a href="#">
								<div class="imgBox">
									<div class="img"><img src="img/ind_product_pic.png" /></div>
								</div>
								<div class="main">
									<div class="title ellipsis" title="Webfield ECS-700">Webfield ECS-700</div>
									<div class="type">PCS — DCS / FCS</div>
									<div class="con">
										<p><strong>High accuracy :</strong> up to 0.04%</p>
										<p><strong>Stability :</strong> ±0.1%/10 years</p>
										<p>CE Certificate</p>
									</div>
								</div>
							</a></li>
							<?php }?>
						</ul>
					</div>
					<div class="path">
						<ul class="explore_ind clearfix">
							<li><a href="#">
								<i class="fl icon1"></i>
								<div class="main fr">
									<div class="title ellipsis" title="Petrochemical & Refinery">Petrochemical & Refinery</div>
									<div class="con">As a leader in the Petrochemical & Refinery industry, our industry-specific solutions create a mode of intelligent plant to increase efficiency and reduce hidden risks…</div>
								</div>
							</a></li>
							<li><a href="#">
								<i class="fl icon2"></i>
								<div class="main fr">
									<div class="title ellipsis" title="Petrochemical & Refinery">Oil & Gas</div>
									<div class="con">As a leader in the Petrochemical & Refinery industry, our industry-specific solutions create a mode of intelligent plant to increase efficiency and reduce hidden risks…</div>
								</div>
							</a></li>
						</ul>
					</div>
					<div class="path">
						<ul class="explore_down clearfix">
							<?php for($i=0;$i<4;$i++){?>
							<li><a href="#">
								<img src="img/resourece_pic.jpg" />
								<div class="type_icon">JPG</div>
								<i class="lock_icon"></i>
								<p>SUPCON Newsletter December 2010</p>
							</a></li>
							<?php }?>
						</ul>
					</div>
					<div class="path">
						<ul class="explore_news clearfix">
							<?php for($i=0;$i<3;$i++){?>
							<li><a href="#">
								<img src="img/img.jpg" />
								<div class="main">
									<div class="title">SUPCON Awarded 2 Honors for 
Internet+ Industry</div>
									<div class="date">March 09, 2016</div>
								</div>
							</a></li>
							<?php }?>
						</ul>
					</div>
				</div>
			</div>