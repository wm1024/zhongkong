<div class="header">
	<div class="container clearfix">
		<div class="logo fl"><a href="index.php"><img src="images/logo.png" width="154" height="25" /></a></div>
		<div class="head_sub_nav fl">
        	<ul class="clearfix">
            	<li class="on"><a href="about.php">About us</a></li>
            	<li><a href="product.php">Products</a></li>
                <li><a href="industries.php">Industries</a></li>
                <li><a href="service_life.php">Service</a></li>
                <li><a href="resources.php">Resources</a></li>
                <li><a href="parner.php">Partner</a></li>
                <li><a href="news.php">News</a></li>
            </ul>
        </div>
        <div class="head_sub_right clearfix">
            <div class="head_sub_search fl"><input type="submit" value="" class="search_sub_text" /></div>
            <a href="javascript:;" class="head_sub_add fl" onclick="join_favorite(window.location,document.title)"></a>
            <div class="head_sub_lan clearfix">
                <a href="##" class="head_sub_ch fl">中文</a>
                <div class="head_sub_lan_line fl"></div>
                <a href="##" class="head_sub_en fr">EN</a>
            </div>
        </div>
	</div>

	<div class="header_nav_erify">    
    	<div class="container">	
        	<!--产品下拉-->
			<div class="nav_erify_list clearfix">
				<div class="head_erify_left fl">
    				<ul>
    					<li><a href="#">Total solution</a></li>
    					<li><a href="#">Instruments</a></li>
                        <li><a href="#">PCS</a></li>
                        <li><a href="#">APC & MES</a></li>
                        <li><a href="#">ERP</a></li>
    				</ul>
    			</div>
                <div class="head_erify_right clearfix fr">
                    <div class="head_erify_left_list fl">
                        <ul>
                            <li><a href="#">Recorder</a></li>
                            <li><a href="#">Transmitter</a></li>
                            <li><a href="#">Calibrator</a></li>
                            <li><a href="#">Controller</a></li>
                            <li><a href="#">Flowmeter</a></li>
                            <li><a href="#">Isolator</a></li>
                            <li><a href="#">Control valves</a></li>
                        </ul>
                    </div>
                    <div class="head_erify_list fr">
                    	<?php for($i=0;$i<5;$i++) { ?>
                        <div class="head_erify_right_list">
                            <div class="head_erify_txt fl">
                                <h1>S2</h1>
                                <div class="head_erify_info">
                                    SUPCON S2 series enterprise manage -ment software mainly covers four business fields, provides ERP, CRM and other specific.
                                </div>
                                <a href="#" class="head_erify_link">Explore more<i class="erify_link_icon"></i></a>
                            </div>
                            <div class="head_erify_pic fr">
                                <a href="#" class="clearfix">
                                    <div class="head_product_info fl">
                                        <h2>ECS-700</h2>
                                        <h3>Now with enhanced functionality</h3>
                                    </div>
                                    <div class="head_product_pic fr">
                                        <div class="img">
                                            <img src="img/head_pro_pic.png" />
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <?php } ?>
					</div>

    			</div>
    		</div>
            <!--服务下拉-->
            <div class="nav_erify_list clearfix">
            	<div class="head_erify_left service_erify_left fl">
    				<ul>
    					<li><a href="#">Life-cycle service</a></li>
    					<li><a href="#">Submit your requirements</a></li>
    				</ul>
    			</div>
                <div class="head_erify_right service_erify_right fr">
					<div class="head_service_erify_tit">Need Service Now?</div>	
                  	<div class="service_erify_cont clearfix">  
                    	<div class="service_erify_list fl">
                    		<div class="service_erify_txt">
                        		Call SUPCON<br/>24/7 service hotline:
                                <i class="service_head_icon1"></i>
                        	</div>
                            <div class="service_erify_tel">400-887-6000</div>
                        </div>
                        <div class="service_erify_line fl">Or<div class="service_erify_mid_line"></div></div>
                    	<div class="service_erify_list fr" style="width:286px;">
                    		<div class="service_erify_txt">
                        		Submit your<br /> requirements online
                                <i class="service_head_icon2"></i>
                        	</div>
                            <a href="#" class="head_erify_link">Online Support Center<i class="erify_link_icon"></i></a>
                        </div>
                	</div>    	
                </div>
           	</div> 
            <!--新闻下拉-->
            <div class="nav_erify_list clearfix">
            	<div class="head_erify_left fl">
    				<ul>
    					<li><a href="#">News Highlights</a></li>
    					<li><a href="#">News</a></li>
                        <li><a href="#">Newsletter</a></li>
    				</ul>
    			</div>
                <div class="head_erify_right fr">
 					<div class="news_erify_cont clearfix">
                    	<div class="news_erify_info fl">
                    		<h2><a href="#">Vice Chairman of Zhejiang Association for Science and Technology visits ...</a></h2>
							<h3>Release time：2015-12-31</h3>
                        	<div class="news_erify_txt"><a href="#">
                        		March 4, SUPCON Fluid Technology Co., Ltd. was awarded “Hangzhou City Enterprise Technology Center” in the Hangzhou Enterprise Technology Innovation and  …
                        	</a></div>
                        </div>
                    	<div class="news_erify_pic fr"><a href="#"><img src="img/head_news_pic.jpg" /></a></div>
                    </div>
                </div>
           	</div> 
    	</div>
	</div>    
</div>