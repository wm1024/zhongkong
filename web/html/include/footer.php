<div class="footer">
    <div class="foot_top">
    	<div class="container clearfix">
    		<div class="foot_top_left fl">
            	<div class="foot_top_nav">
                	<ul class="clearfix">
                		<li style="padding-left:0;"><a href="#">About us</a></li>
                        <li><a href="#">Products</a></li>
                        <li><a href="#">Solutions</a></li>
                        <li><a href="#">Service</a></li>
                        <li><a href="#">Resources</a></li>
                        <li><a href="#">Partner</a></li>
                        <li style="border:none;"><a href="#">News</a></li>
                	</ul>
                </div>
                <div class="foot_top_share">
					<div class="bdsharebuttonbox">
                    	<a title="分享到Facebook" href="#" class="bds_fbook" data-cmd="fbook"></a>
                        <a title="分享到linkedin" href="#" class="bds_linkedin" data-cmd="linkedin"></a>
                        <a title="分享到Twitter" href="#" class="bds_twi" data-cmd="twi"></a>
                        <a title="分享到一键分享" href="#" class="bds_mshare" data-cmd="mshare"></a>
                	</div>              
                </div>
                <div class="foot_top_link">
                	<ul class="clearfix">
                		<li><a href="sitmap.php"><i class="foot_map"></i>Sitemap</a></li>
                        <li><a href="overseas@supcon.com"><i class="foot_email"></i>E-mail</a></li>
                        <li><a href="javascript:;" onclick="pagePrint();"><i class="foot_print"></i>Print</a></li>
                	</ul>
                </div>
            </div>
    		<div class="foot_top_right fr">
            	<div class="foot_right_tit">Related Websites</div>
                <div class="foot_right_nav">
                	<p>Supcon.com</p>
                    <i class="foot_arrow"></i>
                	<ul>
                    	<?php for($i=0;$i<5;$i++) { ?>
                    	<li><a href="##" target="_blank">Supcon.com</a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
    	</div>
    </div>
    <div class="foot_bot"><div class="container">Copyright©2016 SUPCON All Rights Reserved</div></div>
</div>