<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<div class="honor_pop">
		<div class="honor_layer"></div>
        <div class="honor_main">
        	<div class="honor_pop_close"></div>
            <div class="honor_pop_pic"><img src="img/honor_pic2.jpg" /></div>
            <div class="honr_pop_txt">Achilles Level II Certificate for ECS-700 FCU712-S01</div>	
        </div>
	</div>
	<?php include("include/header.php")?>
  	<div class="sub_top_nav">
    	<div class="container clearfix">
        	<div class="sub_top_link fl">
        		<a href="##"><i class="subtop_icon"></i>About us<i class="subtop_next"></i></a>
        	</div>
            <div class="sub_top_navs fl">
            	<a href="quality.php">Quality No.1<i class="subtop_next"></i></a>
            </div>
            <div class="sub_top_navs fl">
            	<a href="quality_list.php">QA Management</a>
            </div>
            <div class="sub_top_share"></div>
        </div>
    </div>
	<div class="sub_cont">
    	<div class="container">
 			<div class="milestone_title quality_title">QA Management</div>
            <div class="honor_list">
            	<ul class="clearfix">
                	<?php for($i=0;$i<8;$i++) { ?>
            		<li><a href="javascript:;" onclick="openHonor(this);">
                    	<div class="honor_pic" rel="img/honor_pic<?php echo($i+1); ?>.jpg" style="background:url(img/honor_pic<?php echo($i+1); ?>.jpg) no-repeat center center;"></div>
                    	<div class="honor_txt">CE Certificate for ECS-100 (EMC)</div>
            		</a></li>
                    <?php } ?>
            	</ul>
            </div>
    	</div>
    </div>    
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</body>
</html>