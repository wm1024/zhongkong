<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="sub_cont">
    	<div class="container">
 			<div class="milestone_title job_title">Job opportunities</div>
            <div class="job_list">
            	<ul class="clearfix">
                	<?php for($i=0;$i<3;$i++) { ?>
                	<li>
                		<div class="job_position">Engineers</div>
                        <div class="job_info"><span>Release date :</span> Jul.02,2015&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Location :</span> Hangzhou</div>
                        <div class="ind_news_more job_more"><a href="job_det.php">Explore more<i class="ind_more_icon"></i></a></div>
                	</li>
                	<li>
                		<div class="job_position">Overseas Project Engineer</div>
                        <div class="job_info"><span>Release date :</span> Jul.02,2015&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Location :</span> Hangzhou</div>
                        <div class="ind_news_more job_more"><a href="job_det.php">Explore more<i class="ind_more_icon"></i></a></div>
                	</li>
                    <?php } ?>
            	</ul>
            </div>
    	</div>
    </div>    
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</body>
</html>