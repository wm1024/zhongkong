$(document).ready(function() {
    var nowYear=new Date().getFullYear();
    for(var i=(nowYear-30);i<(nowYear+30);i++){
		$('.year').append('<li>'+i+'</li>');
		$('.to_year').append('<li>'+i+'</li>');
	}//年份
	
	for(var i=1;i<13;i++){
		$('.month').append('<li>'+i+'</li>');
		$('.to_month').append('<li>'+i+'</li>');
	}//月份


	
    $('.selectbox p').click(function(){
		$(this).siblings().stop().slideDown(200).end().parent().siblings().find('ul').stop().slideUp(200);
	});//弹出下拉框
	
	$('.options').on('click','li',function(){
		var text=$(this).text();
		$(this).parent().parent().find('p').text(text);
		
		if($(this).parent().hasClass('year'))
		{
			$('input[name="year"]').val(text);
		}
		else if($(this).parent().hasClass('month'))
		{
			$('input[name="month"]').val(text);
		}
		else if($(this).parent().hasClass('day'))
		{
			$('input[name="day"]').val(text);
		}	
		else if($(this).parent().hasClass('to_year'))
		{
			$('input[name="to_year"]').val(text);
		}
		else if($(this).parent().hasClass('to_month'))
		{
			$('input[name="to_month"]').val(text);
		}
		else if($(this).parent().hasClass('to_day'))
		{
			$('input[name="to_day"]').val(text);
		}			
		
		$(this).parent().stop().slideUp(200);
	});//选择下拉框
	$('.options').mouseleave(function(){
		$(this).stop().slideUp(200);
	});
	
	//选择月份后弹出日期
	$('#day').click(function(){
		//$('.day li').remove();
		$(this).siblings('.day').find('li').remove();
		if($('#year').text()!='Year' && $('#month').text()!='Month')
		{
			var mm=$('#month').text();
			var yy=$('#year').text();
	        if(mm==2)
			{
				if(0 == yy%4 && (yy%100 !=0 || yy%400 == 0))
				{
					   for(var i=1;i<30;i++){
						$('.day').append('<li>'+i+'</li>');
					   }//大月日期
				}
				else{
					for(var i=1;i<29;i++){
						$('.day').append('<li>'+i+'</li>');
					   }//大月日期
				}
			}
			else
			{
				if(mm==1 || mm==3 || $(this).text()==5 || mm==7 || mm==8 || mm==10 || mm==11 )
				{
				
			    	for(var i=1;i<32;i++){
						$('.day').append('<li>'+i+'</li>');
					}//大月日期
				}
				else
				{
					for(var i=1;i<31;i++){
						$('.day').append('<li>'+i+'</li>');
					}//大月日期
				}
			}

		}
		else{
			$(this).siblings().stop().slideUp();
			alert('Please slect Year and Month！')
		}
		    

	});
	

	//选择月份后弹出日期
	$('#to_day').click(function(){
		$(this).siblings('.to_day').find('li').remove();
		//$('.day li').remove();
		if($('#to_year').text()!='Year' && $('#to_month').text()!='Month')
		{
			var mm=$('#to_month').text();
			var yy=$('#to_year').text();
	        if(mm==2)
			{
				if(0 == yy%4 && (yy%100 !=0 || yy%400 == 0))
				{
					   for(var i=1;i<30;i++){
						$('.to_day').append('<li>'+i+'</li>');
					   }//大月日期
				}
				else{
					for(var i=1;i<29;i++){
						$('.to_day').append('<li>'+i+'</li>');
					   }//大月日期
				}
			}
			else
			{
				if(mm==1 || mm==3 || $(this).text()==5 || mm==7 || mm==8 || mm==10 || mm==11 )
				{
				
			    	for(var i=1;i<32;i++){
						$('.to_day').append('<li>'+i+'</li>');
					}//大月日期
				}
				else
				{
					for(var i=1;i<31;i++){
						$('.to_day').append('<li>'+i+'</li>');
					}//大月日期
				}
			}

		}
		else{
			$(this).siblings().stop().slideUp();
			alert('Please slect Year and Month！')
		}
		    

	});	
	
});
	