
//简历
function applayForm(){
	
	var applay_name=$("#applay_name").val();
	var applay_area=$("#applay_area").val();
	var applay_phone=$("#applay_phone").val();
	var applay_email=$("#applay_email").val();
	var applay_docname=$("#applay_docname").val();
	
	if(applay_name==''){
		
		alert("Please enter your name！")
		return false;	
	}
	if(applay_area==''){
		
		alert("Please input your address！")
		return false;	
	}
	if(applay_phone==''){
		
		alert("Please enter your phone number！")
		return false;	
	}		
	else if(!phoneReg(applay_phone)){
		
		alert("Please enter a correct phone number！")
		return false;		
	}
	if(applay_email==''){
		
		alert("Please enter your e-mail！")
		return false;	
	}
	else if(!emailReg(applay_email)){
		
		alert("Please enter a correct e-mail！")
		return false;		
	}
	if(applay_docname==''){
		
		alert("Please upload your resume！")
		return false;	
	}
}

//登录
function loginForm(){

	var login_name=$("#login_name").val();
	var reg_name=$("#login_name").attr("_reg");
	var login_pwd=$("#login_pwd").val();
	
	if(login_name==reg_name){
	
		alert("Please enter your e-mail!");
		return false;	
	}	
	else if(!emailReg(login_name)){
		
		alert("Please provide a valid email!");
		return false;		
	}
	if(login_pwd==''){
	
		alert("Please enter your password!");
		return false;	
	}
}

//忘记密码提交
function forgetForm(){

	var forget_email=$("#forget_email").val();
	var reg_email=$("#forget_email").attr("_reg");
	
	if(forget_email==reg_email){
		
		$(".tips").text("Please enter your email");
		return false;	
	}
	else if(!emailReg(forget_email)){
		
		$(".tips").text("Please provide a valid email");
		return false;	
	}
		
}

//注册
function signForm(){
	
	var sign_email=$("#sign_email").val();
	var reg_email=$("#sign_email").attr("_reg");
	var sign_pwd=$("#sign_pwd").val();
	var reg_pwd=$("#sign_pwd").attr("_reg");
	var repeat_pwd=$("#repeat_pwd").val();
	var reg_repeat=$("#repeat_pwd").attr("_reg");
	var sign_name=$("#sign_name").val();
	var sign_country=$("#sign_country").val();
	var sign_conpany=$("#sign_conpany").val();
	var sign_phone=$("#sign_phone").val();

	if(sign_email==reg_email){
		
		alert("Please enter your email!");
		return false;	
	}
	else if(!emailReg(sign_email)){
		
		alert("Please provide a valid email!");
		return false;			
	}
	if(sign_pwd==reg_pwd){
	
		alert("Please enter your password!");
		return false;	
	}
	if(repeat_pwd==reg_repeat){
		
		alert("Please repeat your password!");
		return false;
	}
	if(sign_pwd!=repeat_pwd){
		
		alert("The passwords entered are not consistent!");
		return false;
	}
	if(sign_name==''){
	
		alert("Please enter your name!");
		return false;	
	}
	if(sign_country==''){
	
		alert("Please enter your country or region!");
		return false;	
	}	
	if(sign_conpany==''){
		
		alert("Please enter your company!");
		return false;	
	}	
	if(sign_phone==''){
		
		alert("Please enter your phone number!");
		return false;	
	}
	else if(!phoneReg(sign_phone)){
	
		alert("Please provide a valid phone number!");
		return false;	
	}

}

//修改密码
function changeForm(){

	var old_pwd=$("#old_pwd").val();
	var new_pwd=$("#new_pwd").val();
	var again_pwd=$("#again_pwd").val();
	
	if(old_pwd==''){
		
		alert("Please enter your password!");
		return false;	
	}
	if(new_pwd==''){
	
		alert("Please enter your new password!");
		return false;	
	}
	if(again_pwd==''){
		
		alert("Please enter your new password again!");
		return false;
	}
	else if(again_pwd!=new_pwd){
		
		alert("The passwords entered are not consistent!");
		return false;
	}
		
}

//编辑资料
function editForm(){

	var edit_name=$("#edit_name").val();
	var edit_country=$("#edit_country").val();
	var edit_company=$("#edit_company").val();
	var edit_phone=$("#edit_phone").val();
	
	if(edit_name==''){
		
		alert("Please enter your name!");
		return false;	
	}
	if(edit_country==''){
		
		alert("Please enter your Country or region!");
		return false;	
	}	
	if(edit_company==''){
		
		alert("Please enter your company!");
		return false;	
	}
	if(edit_phone==''){
		
		alert("Please enter your phone number!");
		return false;	
	}		
	else if(!phoneReg(edit_phone)){
	
		alert("Please provide a valid phone number!");
		return false;
	}
}

//服务表单
function serviceForm(){

	var service_name=$("#service_name").val();
	var service_email=$("#service_email").val();
	var service_country=$("#service_country").val();
	var service_company=$("#service_company").val();
	var service_address=$("#service_address").val();
	var service_phone=$("#service_phone").val();
	var service_project=$("#service_project").val();
	var service_message=$("#service_message").val();
	
	if(service_name==''){
		
		alert("Please enter your name!");
		return false;	
	}
	if(service_email==''){
		
		alert("Please enter your email!");
		return false;	
	}
	else if(!emailReg(service_email)){
		
		alert("Please provide a valid email!");
		return false;		
	}
	if(service_country==''){
		
		alert("Please enter your country or region!");
		return false;		
	}	
	if(service_company==''){
		
		alert("Please enter your company!");
		return false;		
	}
	if(service_address==''){
		
		alert("Please enter your address!");
		return false;		
	}		
	if(service_phone==''){
		
		alert("Please enter your phone number!");
		return false;		
	}
	else if(!phoneReg(service_phone)){
		
		alert("Please provide a valid phone number!");
		return false;		
	}	
	if(service_project==''){
		
		alert("Please enter your project name!");
		return false;		
	}	
	if(service_message==''){
		
		alert("Please enter your message!");
		return false;		
	}	
}

//合作伙伴
function parnerForm(){

	var parner_name=$("#parner_name").val();
	var parner_email=$("#parner_email").val();
	var parner_phone=$("#parner_phone").val();
	
	if(parner_name==''){
	
		alert("Please enter your name!");
		return false;	
	}
	if(parner_email==''){
	
		alert("Please enter your email!");
		return false;	
	}
	else if(!emailReg(parner_email)){
	
		alert("Please provide a valid name!");
		return false;	
	}
	if(parner_phone==''){
	
		alert("Please enter your phone number!");
		return false;	
	}
	if(!phoneReg(parner_phone)){
	
		alert("Please provide a valid phone number!");
		return false;	
	}	
}

function subscribeForm(){
	var email=$('#email').val();
	if(email==''){
		alert('Please enter your e-mail');
		return false;
	}
	else if(!emailReg(email)){
		alert('Please provide a valid email');
		return false;
	}
}

//电话正则验证
function telReg(tel){
	var reg=/^[\@A-Za-z0-9\!\#\$\%\^\&\*\.\~]{6,20}$/;
	var result=reg.test(tel)? true:false ;
	return result;
}
//手机号正则验证
function phoneReg(phone){
	var reg=/^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
	var result=reg.test(phone)? true:false ;
	return result;
}
//邮箱正则验证
function emailReg(email){
	var reg=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
	var result=reg.test(email)? true:false ;
	return result;
}
//QQ正则验证
function qqReg(qq){
	var reg=/^\d[1-9]{5,10}$/; 
	var result=reg.test(qq)? true:false ;
	return result;
}
