<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="sub_cont">
		<div class="container">
			<div class="newsDet clearfix">
				<div class="newsDet_left fl">
					<div class="title">SUPCON HD5500 Series Isolated Barrier Passed BV SIL Certificate</div>
					<div class="date">Release time：2016-03-09</div>
					<div class="con">
						<img src="img/img.jpg" />
						<p>March 3, SUPCON was awarded the BV SIL Certificate (Bureau Veritas Safety Integrity Level Certificate) for its new generation of HD5500 series isolated barrier, by Mr. Gilles Fan, Director of Inspection Department and General Manager of BV-Fairweather, BV Industry and Facilities Division. </p>
						<br />
						<p>The HD5500 series, from product planning, design, R&D to verification, is strictly  compatible with the latest IEC 61508 standard. All products related to AI/AO and DI/DO functions reached SIL 3 level, and the other two temperature transmission type product reached SIL 2 level.</p>
						<br />
						<p>The assignment work has lasted for nearly two years with the full involvement of BV expert team throughout the whole process from company Quality Management System evaluation, product design material evaluation, Safety Integrity Level parameter calculation, verification and validation to on-site examination. The team commended that the quality of HD5500 series products have reached the world advanced level.</p>
						<br />
						<p>Links:</p>
						<p>Founded in 1828, Bureau Veritas is a global leader in Testing, Inspection and Certification (TIC), delivering high quality services to help clients meet the growing challenges of quality, safety, environmental protection and social responsibility. To the end of 2015, BV has totally 66,000 employees and 1,400 offices and laboratories in 140 countries.</p>
					</div>
				</div>
				<div class="newsDet_right fr">
					<div class="newsDet_search">
						<div class="tit">Search :</div>
						<div class="main clearfix">
							<input type="text" class="text fl" placeholder="Enter the content you want to search"/><input type="submit" value="" class="btn fl"/>
						</div>
					</div>
					<div class="news_page">
						<p>Previous : </p>
						<a href="#">Vice Chairman of Zhejiang Association for Science and Technology visits SUPCON</a>
						<p>Next : </p>
						<a href="#">Vice Chairman of Zhejiang Association for Science and Technology visits SUPCON</a>
					</div>
				</div>
			</div>
			
			<?php include("include/explore_more.php")?>
		</div>
	</div>
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</body>
</html>