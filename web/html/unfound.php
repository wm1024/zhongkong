<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
  	<div class="unfound_cont">  
    	<div class="container">
    		<div class="unfound_pic"><img src="img/unfound_pic.jpg" width="350" height="155" /></div>
            <div class="unfound_title">Oops! We cannot find the page.</div>
            <div class="unfound_txt">The page you’re looking for can’t be found. </div>
            <div class="unfound_search">
				<div class="unfound_search_top clearfix">
					<input type="text" value="Enter the content you want to search" _reg="Enter the content you want to search" class="unfound_text" />
                    <input type="submit" value="Search" class="unfoundBtn" />
				</div>
				<div class="unfound_search_title">Are you looking for these pages?</div>
                <div class="unfound_list">
                	<ul class="clearfix">
                    	<li><a href="#">ECS-700<i class="unfond_arrow"></i></a></li>
                        <li><a href="#">TCS-900<i class="unfond_arrow"></i></a></li>
                        <li><a href="#">JX-300XP<i class="unfond_arrow"></i></a></li>
                    </ul>
                </div>
            </div>
    	</div>
    </div>
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</body>
</html>