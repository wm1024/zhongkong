<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
  	<div class="sub_top_nav">
    	<div class="container clearfix">
        	<div class="sub_top_link fl">
        		<a href="search.php"><i class="subtop_icon"></i>Search<i class="subtop_next"></i></a>
        	</div>
            <div class="sub_top_share">
            	<div class="bdsharebuttonbox">
                	<a title="分享到一键分享" href="javascript" class="bds_mshare" data-cmd="mshare"></a>
            	</div>  
        	</div>
        </div>
    </div>
	<div class="sub_cont">
    	<div class="container">
 			<div class="milestone_title search_title">Search</div>
            <div class="search_subtit">Search results</div>
            
            <div class="sub_top_search search_top_results clearfix">
    			<input type="text" value="apple" class="keyword_text fl" /> 
    			<input type="submit" value="Search" class="subBtn fr" />
    		</div>
            <div class="search_results_info">5 results found for "apple"</div>
            <div class="search_nav">
            	<ul>
            		<li class="on">All(5)</li>
                    <li>News(2)</li>
                    <li>Products(3)</li>
            	</ul>
            </div>
            <div class="search_tabs">
            	<div class="search_result_list">
                	<div class="search_subtit">All</div>
                	<ul>
                    	<?php for($i=0;$i<4;$i++) { ?>
                    	<li><a href="##">
                        	<div class="search_result_top clearfix">
								<div class="search_result_name fl">SUPCON Fluid Awarded Hangzhou City Enterprise Technology Center</div>
								<div class="search_result_date fr"><i class="result_date"></i>2016-05-29</div>
							</div>
		                    <div class="search_result_txt">
                            	March 4, SUPCON Fluid Technology Co., Ltd. was awarded “Hangzhou City Enterprise Technology Center” in the Hangzhou Enterprise Technology Innovation and Development Conference. <br />
The conference was held by Hangzhou Economy and Information Technology Commission. Over 200 government leaders.…
							</div>
  
                        </a></li>
                    	<?php } ?>
                    </ul>
                </div>
            	<div class="search_result_list">
                	<div class="search_subtit">News</div>
						<ul>
                        <?php for($i=0;$i<4;$i++) { ?>
                        <li><a href="##">
                            <div class="search_result_top clearfix">
                                <div class="search_result_name fl">SUPCON Fluid Awarded Hangzhou City Enterprise Technology Center</div>
                                <div class="search_result_date fr">2016-05-29</div>
                            </div>
                            <div class="search_result_txt">
                                March 4, SUPCON Fluid Technology Co., Ltd. was awarded “Hangzhou City Enterprise Technology Center” in the Hangzhou Enterprise Technology Innovation and Development Conference. <br />
The conference was held by Hangzhou Economy and Information Technology Commission. Over 200 government leaders.…
                            </div>
  
                        </a></li>
                        <?php } ?>
                    </ul>
                </div>
            	<div class="search_result_list">
                	<div class="search_subtit">Products</div>
                    <ul>
                    	<?php for($i=0;$i<4;$i++) { ?>
                    	<li><a href="##">
                        	<div class="search_result_top clearfix">
								<div class="search_result_name fl">SUPCON Fluid Awarded Hangzhou City Enterprise Technology Center</div>
								<div class="search_result_date fr">2016-05-29</div>
							</div>
		                    <div class="search_result_txt">
                            	March 4, SUPCON Fluid Technology Co., Ltd. was awarded “Hangzhou City Enterprise Technology Center” in the Hangzhou Enterprise Technology Innovation and Development Conference. <br />
The conference was held by Hangzhou Economy and Information Technology Commission. Over 200 government leaders.…
							</div>
  
                        </a></li>
                    	<?php } ?>
                    </ul>
                </div>
            </div>
    	</div>
    </div>    
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</body>
</html>