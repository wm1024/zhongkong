<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
	<?php include("include/mycenter_top.php") ?>  	
    <div class="mycenter_cont">
    	<div class="container">
    		<?php include("include/mycenter_left.php") ?>	
    		<div class="mycenter_right fr">
    			<div class="mycenter_right_title">Download History</div>
    			<div class="download_list">
                	<ul class="clearfix">
                		<?php for($i=0;$i<3;$i++) { ?>
                        <li><div class="resource_left"><a href="##"><img width="156" height="205" src="img/resourece_pic.jpg">
                        	<div class="type_icon">JPG</div>
                        	<i class="lock_icon"></i>
                            <div class="view_yin"></div>
                        </a></div>
                        	<div class="download_name"><a href="##">SUPCON Automation Solution For …</a></div>
                            <div class="download_date">Feb. 04  2015</div>
							<div class="download_bot clearfix">
                            	<a class="download fl" href="#">Download</a>
                                <div class="resourse_share fl">
                                    <div class="bdsharebuttonbox">
                                        <a title="分享到一键分享" href="javascript" class="bds_mshare" data-cmd="mshare"></a>
                                    </div> 
                                </div>
                            </div>
                		</li>
                        <?php } ?>
                	</ul>
                    <ul class="clearfix">
                		<?php for($i=0;$i<3;$i++) { ?>
                        <li><div class="resource_left"><a href="##"><img width="156" height="205" src="img/resourece_pic.jpg">
                        	<div class="type_icon">JPG</div>
                        	<i class="lock_icon"></i>
                            <div class="view_yin"></div>
                        </a></div>
                        	<div class="download_name"><a href="##">SUPCON Automation Solution For …</a></div>
                            <div class="download_date">Feb. 04  2015</div>
							<div class="download_bot clearfix">
                            	<a class="download fl" href="#">Download</a>
                                <div class="resourse_share fl">
                                    <div class="bdsharebuttonbox">
                                        <a title="分享到一键分享" href="javascript" class="bds_mshare" data-cmd="mshare"></a>
                                    </div> 
                                </div>
                            </div>
                		</li>
                        <?php } ?>
                	</ul>
                </div>

                <div class="page">
                	<div class="page_cont">
                	<a href="#" class="prev"><i class="page_prev"></i></a>
                	<a class="on" href="##">1</a>
                	<a href="##">2</a>
                    <a href="##">3</a>
                	<a href="##" class="next"><i class="page_next"></i></a>
                	</div>
                </div>
    		</div>
    	</div>
    </div>
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/form.js"></script>

</body>
</html>