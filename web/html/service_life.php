<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
    <?php include("include/top_link.php")?>
	<div class="service_top_banner" style="background:url(img/service_banner.jpg) no-repeat center top;">
    	<div class="container">
            <div class="service_banner_title">Increase your performance<br />with SUPCON Services Solution</div> 
            <div class="service_banner_txt">
                SUPCON provides a comprehensive portfolio of life cycle services to meet 
                the specific needs of each customer. We deliver services on the promise of 
                improving our customers’ competitive advantage and profitable results 
                through our global network of local and factory specialists.
            </div>
        </div>
    </div>
    <div class="service_allday">
    	<div class="container">
        	<div class="allday_title">All-day Technical Support</div>
        	<div class="allday_info">
            	<p>As a partner of manufacturing and process industries, we have a global network of experts at our disposal.</p> 
				<p>Right from day one, you will benefit from our comprehensive technology know-how and the industry-specific competence</p> 
				<p>of our service experts – 24/7 on site whenever and wherever you need us.</p>
           	</div>
            <div class="allday_list">
            	<ul class="clearfix">
            		<li><div class="allday_list_cont">
                            <i class="allday_break1"></i>
                            <div class="allday_list_title">Reginal service centers</div>
                        </div>
                    	<div class="allday_list_info">
                        	<div class="allday_info_tit">Reginal service centers</div>
                        	<div class="allday_info_txt">
                            	<p>set up for the particular needs</p>
                                <p>and prompt response of nearby</p>	
                                <p>customersand local</p>
                            </div>
                        </div>
                    </li>
            		<li><div class="allday_list_cont">
                            <i class="allday_break2"></i>
                            <div class="allday_list_title">24/7 hotline service</div>
                        </div>
                    	<div class="allday_list_info">
                        	<div class="allday_info_tit">24/7 hotline service</div>
                        	<div class="allday_info_txt">
                            	<p>provides a one-step tec</p>
                                <p>support platform to solve</p>	
                                <p>customers’ problems fast</p>
                                <p>and conveniently</p>
                            </div>
                        </div>
                    </li>
            		<li><div class="allday_list_cont">
                            <i class="allday_break3"></i>
                            <div class="allday_list_title">Online support</div>
                        </div>
                    	<div class="allday_list_info">
                        	<div class="allday_info_tit">Online support</div>
                        	<div class="allday_info_txt">
                            	<p>our service website will help you</p>
                                <p>obtain more service resource</p>	
                                <p>and information</p>
                            </div>
                        </div>
                    </li>
            	</ul>
            </div>
        </div>
    </div>
   	<div class="service_allround">
    	<div class="container">
        	<div class="allround_title">All-Round Technical Service</div>
        	<div class="allround_list">
            	<ul class="clearfix">
            		<li><div class="allround_list_title">Spare parts service</div>
                    	<div class="allround_list_txt">	
                    		<p>keeps a defined range of parts ready for your</p>
                            <p>plant, and provides within agreed-upon lead time</p>
                    	</div>
                        <i class="allround_break1"></i>
                    </li>
            		<li class="right"><div class="allround_list_title">Site service</div>
                    	<div class="allround_list_txt">	
                    		<p>ensure regular service, implement changes in plant</p>
                            <p>operations and assist you at planned outages</p>
                    	</div>
                        <i class="allround_break2"></i>
                    </li>
            		<li><div class="allround_list_title">Repair service</div>
                    	<div class="allround_list_txt">	
                    		<p>covers all physical operations necessary to eliminate</p>
                            <p>investment in your own service infrastructure</p>
                    	</div>
                        <i class="allround_break3"></i>
                    </li>
            		<li class="right"><div class="allround_list_title">Equipment migration service</div>
                    	<div class="allround_list_txt">	
                    		<p>a full range of plant and equipment migration services</p>
                            <p>or a retained investment and enhanced productivity</p>
                    	</div>
                        <i class="allround_break4"></i>
                    </li>
            		<li><div class="allround_list_title">Updates and upgrades</div>
                    	<div class="allround_list_txt">	
                    		<p>our professional and expert advice to preserve</p>
                            <p>your system integrity and routine maintenance</p>
                    	</div>
                        <i class="allround_break5"></i>
                    </li>
            		<li class="right"><div class="allround_list_title">Proactive maintenance service</div>
                    	<div class="allround_list_txt">	
                    		<p>proactive reminding and regular return visits to</p>
                            <p>ensure your plant security and availability</p>
                    	</div>
                        <i class="allround_break6"></i>
                    </li>
            	</ul>
            </div>		
        </div>
    </div>
	<div class="service_train">    
    	<div class="container">
        	<div class="train_title">Comprehensive Technical Trainings</div>
         	<div class="train_cont clearfix" style="background:url(img/train_bg.jpg) no-repeat center top;">
            	<div class="train_left fl">
                	<div class="train_nav">
                        <ul class="clearfix">
                            <li class="on"><i class="train_break1"></i>
                                <div class="train_left_line"></div>
                                <div class="train_left_tit">Professional training<br />technicians</div>
                            </li>
                            <li><i class="train_break2"></i>
                                <div class="train_left_line"></div>
                                <div class="train_left_tit">Advanced training<br />facilities</div>
                            </li>
                            <li><i class="train_break3"></i>
                                <div class="train_left_line"></div>
                                <div class="train_left_tit">Flexible training <br />programs</div>
                            </li>
                            <li><i class="train_break4"></i>
                                <div class="train_left_line"></div>
                                <div class="train_left_tit">ailored training <br />courses</div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="train_right fr">
                	<div class="train_list">
                		<i class="train_break1"></i>
                        <div class="train_list_tit">Professional training technicians</div>
                        <div class="train_list_txt">
                        	Your staff will learn from full-time, factory certified SUPCON training service trainers who can share practical knowledge and expertise in process automation products and systems， build skills with hands-on experience. 
                        </div>
                	</div>
                	<div class="train_list">
                		<i class="train_break2"></i>
                        <div class="train_list_tit">Advanced training facilities</div>
                        <div class="train_list_txt">
							In order to gain the first-hand experience within a short period of time, your staffs will be trained with a series of simulation experiments at DCS, ECS, OpenSys simulation labs and a variety of field instrument and transmitters, covering each and every process of your production, to help your staff practice real field operation in advance. 
                        </div>
                	</div>
                	<div class="train_list">
                    	<i class="train_break3"></i>
                    	<div class="train_list_tit">Flexible training programs</div>
                    	<div class="train_list_txt">
						SUPCON conducts training, wherever and whenever it works best for you, at one of our regional training centers, on-site at your facility or at a nearby off-site setting. Convenient training schedules 							provide great flexibility, so your staff can participate at a time that minimizes business disruptions.
                    	</div>
                    </div>
                	<div class="train_list">
                		<i class="train_break4"></i>
                        <div class="train_list_tit">Tailored training courses</div>
                        <div class="train_list_txt">
                        	Operator Training Courses<br />
                            DCS Engineer Training Course<br />
                            SIS Engineer Training Course<br />
                            DCS Engineer Training Course<br />
                            Industrial Total Solution Engineer Training Course<br />
                            System Maintenance Training Course<br />
                            Engineer Certification Training Course
                        </div>
                	</div>
                </div>
            </div>	
       </div> 
    </div>    
    <div class="service_need">
    	<div class="container">
    		<div class="need_icon"></div>
            <div class="need_tit">Need service now?</div>
            <div class="need_txt">
            	Our 24/7 service engineer team is committed to providing you with quality and comprehensive<br />full life cycle service via: 
			</div>
            <div class="need_bot clearfix">
            	<div class="need_number fl">
            		<div class="need_number_tit">SUPCON Response Center:</div>
                    <div class="need_number_tel">+86-400-887-6000</div>
                </div>	
            	<div class="need_link fr">
            		<div class="need_number_tit">Online support center</div>
                    <div class="need_number_tel"><a href="service_request.php">Submit your request<i class="tel_more"></i></a></div>
                </div>	
            </div>
    	</div>
    </div>
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</body>
</html>