<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<div class="ind_video">
		<div class="ind_video_layer"></div>
        <div class="ind_video_main"><div id="a1"></div></div>
	</div>
	<?php include("include/header.php")?>
    <?php include("include/top_link.php")?>
  	<div class="jx_top" style="background:url(img/jx_top_pic.jpg) no-repeat center top;">
		<div class="container">
    		<div class="jx_top_tit1">Increase your performance with</div>
            <div class="jx_top_tit2">Webfield<sup>®</sup>JX-300XP</div>
    	</div>
    </div>
    <div class="jx_main">
    	<div class="container">
        	<div class="jx_main_tit">Main Features</div>
            <div class="jx_main_info">JX-300XP brings in a friendly operating environment for operators that keep<br />every operator clearly informed, and easy to control the process.</div>
            <div class="jx_cont">
                <ul>
                    <li class="on">
                        <div class="jx_cont_title">Safe and Intelligent Plant Operation<div class="jx_line"></div></div>
                        <div class="jx_erify">
                            User Authority Management to ensure the safety of operation; enhanced design of JX-300XP, makes your process immune to virus and other internet risks. With a variety of interconnection facilities(OPC, VBA, TCP/IP), JX-300XP provides you a very open application environment, and makes your operator easily can touch total plant information. Embedded numerous dedicated process functions help your operator to operate the process intelligently.
                        </div>
                    </li>
                    <li>
                        <div class="jx_cont_title">Safe and Efficient Project Management <div class="jx_line"></div></div>
                        <div class="jx_erify">
                            JX-300XP is reliable and efficient platform of process control, provides your engineer a easy integration solution of: DCS, SIS, RTU, SCADA, MMS, LDS, TGS etc.
                        </div>
                    </li>
                    <li>
                        <div class="jx_cont_title">Non-Stop Improvement of Maintenance <div class="jx_line"></div></div>
                        <div class="jx_erify">
                            JX-300XP improves personnel’s handling ability of emergency with powerful operation and maintenance functions such as the friendly operating environment, alarm management, real- time database platform, remote system diagnose assistance for SUPCON after sales team.
                        </div>
                    </li>
                    <li>
                        <div class="jx_cont_title">Reliable Production Control Platform <div class="jx_line"></div></div>
                        <div class="jx_erify">
                            JX-300XP delivers high reliable and availability of the control system. The stable operation throughout the whole process is completely free from failure of any single component.
                        </div>
                    </li>
                </ul>
                <div class="jx_main_pic"><img src="img/jx_main_pic.png" width="308" height="633" /></div>
            </div>
        </div>
    </div>
    <div class="jx_typical">
    	<div class="container">
    		<div class="typical_tit1">Typical Architecture of</div>
    		<div class="typical_tit2">JX-300XP</div>	
            <div class="typical_info">
            	JX-300XP has an architecture consisting of human machine interfaces, field control stations,<br />
             	and a control network. These three basic components provide for flexible scalability from small<br />
             	scale to large and complex facilities.
             </div>
            <div class="typical_pic"><img src="img/jx_typical_pic.jpg" width="944" height="570" /></div>
    	</div>
    </div>
    <div class="jx_highly">
    	<div class="container">
    		<div class="highly_tit">Highly Reliable and<br />Flexible Product</div>
    		<div class="highly_info">With professional and endeavored design, JX-300XP maintains availability of 99.9999%.</div>
    		<div class="highly_cont clearfix">
    			<div class="highly_left fl" style="background:url(img/jx_highly_bg.jpg) no-repeat left top;">
                	<div class="highly_tabs">
                		<div class="highly_list">
                        	<div class="highly_break1"></div>
                        	<div class="highly_list_tit">Openness and Integration</div>
                            <div class="highly_list_txt">
                            	Integrates various industry communication interfaces such as Modbus, Profibus, HART, EPA and OPC etc. which make it easy to connecting third-party system and various intelligence field instruments.
                            </div>
                        </div>
                		<div class="highly_list">
                        	<div class="highly_break2"></div>
                        	<div class="highly_list_tit">Security</div>
                            <div class="highly_list_txt">
                            	Designed in compliance with European Community EMC Directive II, and special anti- corrosion coating in compliance with ISA71.04 standard G3. System has EMC and LVD CE certificates.
                            </div>
                        </div>
                		<div class="highly_list">
                        	<div class="highly_break3"></div>
                        	<div class="highly_list_tit">Fault Diagnosis</div>
                            <div class="highly_list_txt">
                            	Capable of diagnosing failures concerning module, channel, and transmitter or transducer, failures such as open-circuits of thermocouple can be eliminated easily.
                            </div>
                        </div>
                		<div class="highly_list">
                        	<div class="highly_break4"></div>
                        	<div class="highly_list_tit">Real-Time Simulation</div>
                            <div class="highly_list_txt">
                            	Provides debugging and simulation environment offline, which can shortening commissioning on site and reducing risk during production.
                            </div>
                        </div>
                		<div class="highly_list">
                        	<div class="highly_break5"></div>
                        	<div class="highly_list_tit">High Accuracy</div>
                            <div class="highly_list_txt">
                            	I/O modules adopts the up to date high-accuracy Analog-to-Digital Sampling technology (Σ-A/D), advanced signal isolation technology, strictly proven hot-plugging technology, multi-layer board and surfaced mounting technology, which make high-accuracy analog signal measuring and more stable modules.
                            </div>
                        </div>
                		<div class="highly_list">
                        	<div class="highly_break6"></div>
                        	<div class="highly_list_tit">On-Line Download</div>
                            <div class="highly_list_txt">
                            	Allows engineers to perform on-line downloading after the completion of configuration modification compiling. A bumpless switchover between old and new configuration is realized under SUPCON patented technology.
                            </div>
                        </div>
                		<div class="highly_list">
                        	<div class="highly_break7"></div>
                        	<div class="highly_list_tit">Event Recording Function</div>
                            <div class="highly_list_txt">
                            	Provides powerful event recording functions including record of 1ms sequence of events(SOE), operator's operation recording, process parameter alarm records and so on, and is configured with corresponding event access, analysis, printing and recalling software, etc.
                            </div>
                        </div>
                	</div>
                </div>
                <div class="highly_right fr">
    				<ul>
                    	<li>Openness and Integration
                        	<div class="highly_erify" style="right:0;">Openness and Integration</div>
                        </li>
                        <li>Security
                        	<div class="highly_erify">Security</div>
                        </li>
                        <li>Fault Diagnosis
                        	<div class="highly_erify">Fault Diagnosis</div>
                        </li>
                        <li>Real-Time Simulation
                        	<div class="highly_erify">Real-Time Simulation</div>
                        </li>
                        <li>High Accuracy
                        	<div class="highly_erify">High Accuracy</div>
                        </li>
                        <li>On-Line Download
                        	<div class="highly_erify">On-Line Download</div>
                        </li>
                        <li>Event Recording Function
                        	<div class="highly_erify">Event Recording Function</div>
                        </li>
                    </ul>
                </div>
    		</div>
    	</div>
    </div>
	<div class="indust_video ecs_video">
    	<div class="indust_video_title">Video</div>    
		<div class="indust_video_main">
			<div class="indust_video_pic">
				<img src="img/product_video_pic.jpg" width="834" height="467" />
                <div class="indust_video_pic_yin"><a href="javascript:;" onclick="openVideo()"></a></div>
			</div>
		</div>
	</div>
	<div class="indust_more" style="background:url(img/indust_more_bg.jpg) no-repeat center 200px;">
    	<div class="indust_more_cont">
			<?php include("include/explore_more.php")?>
        </div>
    </div>
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script src="ckplayer/ckplayer.js" type="text/javascript"></script>
<script type="text/javascript">
	var flashvars={
		p:0,
		e:1,
		ht:'20',
		hr:'http://www.ckplayer.com'
		};
	var video=['http://movie.ks.js.cn/flv/other/1_0.mp4->video/mp4'];
	var support=['all'];
	CKobject.embedHTML5('a1','ckplayer_a1',600,400,video,flashvars,support);	
</script>
</body>
</html>