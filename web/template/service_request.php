<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(CP_BASE_TEMP."template/include/title.php")?>
<link href="<?php echo CP_BASE_STATIC;?>public/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo CP_BASE_STATIC;?>public/css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include(CP_BASE_TEMP."template/include/login_pop.php")?>
	<?php include(CP_BASE_TEMP."template/include/header.php")?>
    <?php include(CP_BASE_TEMP."template/include/top_link.php")?>
	<div class="sub_cont">
    	<div class="container">
 			<div class="milestone_title service_title">Submit Your Request</div>
            <div class="service_txt">Please complete this form, our technical experts will contact you for further service.</div>
			<div class="service_tb">
            	<table width="100%">
            		<tr>
                    	<td width="504"><div class="must">*</div>Your name</td>
                    	<td><div class="must">*</div>E-mail</td>
                    </tr>
                    <tr>
                    	<td><input type="text" value="" class="service_text" id="service_name" /></td>
                    	<td><input type="text" value="" class="service_text" id="service_email" /></td>
                    </tr>
                    <tr><td colspan="2" style="height:16px;"></td></tr>
            		<tr>
                    	<td><div class="must">*</div>Country/region</td>
                    	<td><div class="must">*</div>Company</td>
                    </tr>
                    <tr>
                    	<td><div class="select service_select">
                        		<p></p>
                                <ul>
                                    <li>China</li>
                                    <li>India</li>
                                    <li>Thailand</li>
                                    <li>Burma</li>
                        		</ul>
                                <input type="hidden" value="" id="service_country" />
                        	</div>
                        </td>
                    	<td><input type="text" value="" class="service_text" id="service_company" /></td>
                    </tr>
                    <tr><td colspan="2" style="height:16px;"></td></tr>
            		<tr>
                    	<td>Address</td>
                    	<td>Phone</td>
                    </tr>
                    <tr>
                    	<td><input type="text" value="" class="service_text" id="service_address" /></td>
                    	<td><input type="text" value="" class="service_text" id="service_phone" /></td>
                    </tr>
                    <tr><td colspan="2" style="height:16px;"></td></tr>
            		<tr>
                    	<td colspan="2">Project name</td>
                    </tr>
                    <tr>
                    	<td colspan="2"><input type="text" value="" class="service_text project_text" id="service_project" /></td>
                    </tr>
                    <tr><td colspan="2" style="height:16px;"></td></tr>
            		<tr>
                    	<td colspan="2"><div class="must">*</div>Your message</td>
                    </tr>
                    <tr>
                    	<td colspan="2"><textarea id="service_message"></textarea></td>
                    </tr>
                    <tr><td colspan="2" style="height:50px;"></td></tr>
                    <tr>
                    	<td colspan="2" align="right"><input type="submit" value="Submit" class="changeBtn serviceBtn" onclick="serviceForm();" /></td>
                    </tr>
            	</table>
            </div>            
    	</div>
    </div>    
	<?php include(CP_BASE_TEMP."template/include/footer.php")?>
</div>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/common.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/form.js"></script>

</body>
</html>