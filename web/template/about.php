<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(CP_BASE_TEMP."template/include/title.php")?>
<link href="<?php echo CP_BASE_STATIC;?>public/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo CP_BASE_STATIC;?>public/css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include(CP_BASE_TEMP."template/include/header.php")?>
    <?php include(CP_BASE_TEMP."template/include/top_link.php")?>
	<div class="about_top" style="background:url(img/about_top_bg.jpg) no-repeat center top;">
    	<div class="container">
    		<h1>A Leading Automation Supplier</h1>
    		<div class="about_top_txt">
            	Founded in March 1993,SUPCON is one of China's leading providers of automation and information<br /> 
				technology, products and solutions. SUPCON has grown rapidly on the back of cutting-edge systems for the<br /> 
				development of innovative technology products, and to date its automation products and<br /> 
				solutions are applied in over 30 countries and regions worldwide.
			</div>
            <div class="about_top_num">
            	<ul class="clearfix">
            		<li>
                    	<h2>No.<span class="supplier_num" num="1">9999</span></h2>
                        <h3>DCS supplier in China *</h3>
                    </li>
            		<li>
                    	<h2><span num="9000">1</span></h2>
                        <h3>global customers</h3>
                    </li>
                    <li>
                    	<h2><span num="19000">1</span></h2>
                        <h3>DCS installations</h3>
                    </li>
            	</ul>
            </div>
            <h4>* Data from ARC Advisory Group</h4>
            <div class="about_top_line"></div>
            <div class="about_top_tit">Industries Covered:</div>
            <div class="about_industry_list">
            	<ul class="clearfix">
            		<li><a href="##">
                    	<div class="about_indust_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/about_top_pic1.png" width="60" /></div>
                        <p>Oil & Gas</p>
                    </a></li>
            		<li><a href="##">
                    	<div class="about_indust_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/about_top_pic2.png" width="60" /></div>
                        <p>Refinery</p>
                    </a></li>
            		<li><a href="##">
                    	<div class="about_indust_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/about_top_pic3.png" width="60" /></div>
                        <p>Petrochemical</p>
                    </a></li>
            		<li><a href="##">
                    	<div class="about_indust_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/about_top_pic4.png" width="60" /></div>
                        <p>Chemical</p>
                    </a></li>
            		<li><a href="##">
                    	<div class="about_indust_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/about_top_pic5.png" width="60" /></div>
                        <p>Power</p>
                    </a></li>
            		<li><a href="##">
                    	<div class="about_indust_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/about_top_pic6.png" width="60" /></div>
                        <p>Building Material</p>
                    </a></li>
            		<li><a href="##">
                    	<div class="about_indust_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/about_top_pic7.png" width="60" /></div>
                        <p>Pulp & Paper</p>
                    </a></li>
            		<li><a href="##">
                    	<div class="about_indust_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/about_top_pic8.png" width="60" /></div>
                        <p>Metals</p>
                    </a></li>
            	</ul>
            </div>	
    	</div>
    </div>
    <div class="about_force">
    	<div class="container">
    		<h1>R&D Force</h1>
    		<div class="force_list">
            	<ul class="clearfix">
            		<li class="first">
                    	<div class="force_info">
                        	<h2>First 1:1 hot redundant DCS in China</h2>
                            <div class="force_txt">
                            	SUPCON launched JX-100, China’s first independently developed DCS with 1:1 hot redundant technology, and was successfully applied onsit
                            </div>
                        </div>
                    	<div class="force_year">1993</div>
                    </li>
            		<li class="right">
                    	<div class="force_info">
                        	<h2>EPA - China’s first international standard in automation</h2>
                            <div class="force_txt">
                            	SUPCON led the development of EPA (Ethernet for Plant Automation), which was announced by IEC as an international standard in 2007.
                            </div>
                        </div>
                    	<div class="force_year">2007</div>
                    </li>
            		<li class="last">
                    	<div class="force_info">
                        	<h2>China’s largest automation R&D center</h2>
                            <div class="force_txt">
                            	With an annual investment of 8% sales revenue, SUPCON owns China’s largest proprietary R&D center for automation.
                            </div>
                        </div>
                    	<div class="force_year">NOW</div>
                    </li>
            	</ul>
            </div>
            <div class="force_more"></div>
    	</div>
    </div>
    <div class="about_capacity" style="background:url(img/esc_main_bg.jpg) no-repeat center top;">
		<div class="container clearfix">
			<div class="capacity_left fl">
            	<div class="first_pass">
                	<h1>first-pass yield</h1>
                	<div class="first_pass_rate">
                    	<div class="first_pass_yin"></div>
                    </div>
                	<div class="first_pass_num"><span>98</span><sup>%</sup></div>
                </div>
            	<div class="pro_rework">
                	<h1>first-pass yield</h1>
                	<div class="pro_rework_rate">
                    	<div class="pro_rework_yin"></div>
                    </div>
                	<div class="pro_rework_num"><span>0.11</span><sup>%</sup></div>
                </div>
               	<div class="capacity_left_txt"> 
                	SUPCON has developed a lean production model incorporating leading standards – the SPS, i.e. SUPCON Production System,which was able to far exceed advanced international standards in the industry.
            	</div>
            </div>
			<div class="capacity_right fr">
            	<h1>Manufacturing Capacity</h1>
            	<div class="capacity_txt">SUPCON has built and maintains China’s largest control systems production base, and is able to fully meet demand with an annual supply capacity of 600,000 control modules. </div>
                <div class="capacity_txt">SUPCON follows the basic principles of ISO 9000, and has established and refined its own supplier management system with reference to the international supply chain operations reference model SCOR. Our suppliers include many renowned international companies, giving us extensive global supply channels that lay a solid foundation for achieving rapid supply and rapid integration. </div>
                <div class="capacity_bot"><a href="#">QA certificates<i class="move_link_icon"></i></a></div>
            </div>
		</div>
    </div>
    <div class="about_product">
    	<div class="container">
    		<div class="about_pro_top">
            	<div class="about_pro_top_bg"></div>
                	<h1>Products & Solutions</h1>
                	<div class="about_pro_txt">
                    	Based on the deep understandings of automation and information systems, SUPCON has developed its own<br /> 
						distinctive industrial total solution featuring integrated management and control and a four-layer structure to <br />
						fulfill your needs of controlling and managing the whole factory.
					</div>
                <div class="about_pro_bot_bg"></div>
            </div>
    		<div class="about_pro_list">
            	<ul class="clearfix">
        			<li><a href="##">
                    	<div class="about_pro_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/about_pro_pic1.png" width="60" /></div>
                    	<div class="about_pro_line"></div>
                        <p>Field Instrument</p>
                    </a></li>
                	<li><a href="##">
                    	<div class="about_pro_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/about_pro_pic2.png" width="60" /></div>
                    	<div class="about_pro_line"></div>
                        <p>PCS</p>
                    </a></li>
                    <li><a href="##">
                    	<div class="about_pro_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/about_pro_pic3.png" width="60" /></div>
                    	<div class="about_pro_line"></div>
                        <p>APC & MES</p>
                    </a></li>
                    <li><a href="##">
                    	<div class="about_pro_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/about_pro_pic4.png" width="60" /></div>
                    	<div class="about_pro_line"></div>
                        <p>ERP</p>
                    </a></li>
        		</ul>
        	</div>
            <div  class="about_pro_bot">
            	<a href="product_plist.php" class="lookfor_job">Products<i class="move_link_icon"></i></a>
                <a href="industries.php" class="lookfor_ship">Solutions<i class="move_link_icon"></i></a>
            </div>
    	</div>
    </div>
    <div class="about_mac" style="background:url(img/about_mac_bg.jpg) no-repeat center top;">
    	<div class="container">
        	<h1>MAC Strength</h1>
        	<div class="mac_txt">
            	Relying on its own propriety technology, products and resources together with expertise in aspects such as design <br />
				consulting, project implementation and integrated supply chains, SUPCON MAC solution will help you deal with all the <br />
				disturbing issues in a comprehensive project cycle, and as a result, save your project time, <br />
				lower your project risk and cost down millions of investment. 
			</div>
            <div class="mac_list">
            	<ul class="clearfix">
                	<li class="mac_line1"></li>
            		<li>Consulting</li>
                    <li class="mac_line2"></li>
            		<li>FEED</li>
                    <li class="mac_line3"></li>
                    <li>Detailed Design</li>
                    <li class="mac_line4"></li>
                    <li>Installation</li>
                    <li class="mac_line5"></li>
                    <li>Operation</li>
                    <li class="mac_line6"></li>
                    <li>Maintenance</li>
                    <li class="mac_line7"></li>
                    <li>Retirement</li>
                    <li class="mac_line1"></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="about_life">
    	<div class="container">
    		<h1>Life Cycle Service</h1>
    		<div class="life_txt">SUPCON has always adhered to a customer-centric business philosophy,<br />and is committed to providing its global customers with quality and<br />
comprehensive full life-cycle technical service.
			</div>
    		<div class="life_list">
            	<ul class="clearfix">
            		<li>
                    	<div class="life_list_top">24/7</div>
                    	<p>All-day<br />Technical Support</p>
                    </li>
                    <li class="line"></li>
            		<li>
                    	<div class="life_list_top">360°</div>
                    	<p>All-Round<br />Technical Service</p>
                    </li>
                    <li class="line"></li>
                    <li>
                    	<div class="life_list_top"><i class="life_technical"></i></div>
                    	<p>Comprehensive<br />Technical Trainings</p>
                    </li>
            	</ul>
            </div>
    	</div>
    </div>
	<?php include(CP_BASE_TEMP."template/include/footer.php")?>
</div>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/common.js"></script>

<script type="text/javascript">

$(function(){
		
		var suppliernum=0;
		var supplier_Maxnum=parseInt($(".supplier_num").text())
		var supplierTime=3000/addMaxNum;		
		
		supplier();
		
		var supplier_num;
		
		function supplier(){
		
			alert(1111);
			
			supplier_num=setInterval(function(){supplier_change(supplier_Maxnum)},100); 		
		}
		
		function supplier_change(maxNum){
			
				if(maxNum>1){
					maxNum--;
					supplier_Maxnum=maxNum;
					$(".supplier_num").text(supplier_Maxnum);
				}
				else{
				
					clearInterval(supplier_num);	
				}
			}		
			

});
	
			
</script>

</body>
</html>