<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/login_pop.php") ?>
	<?php include("include/header.php")?>
	<?php include("include/mycenter_top.php") ?>  	
    <div class="mycenter_cont">
    	<div class="container">
    		<?php include("include/mycenter_left.php") ?>	
    		<div class="mycenter_right fr">
    			<div class="mycenter_right_title">Change your password</div>
    			<div class="changepwd_tb">
					<table width="100%">
    					<tr>
                        	<td width="135">Old password<a href="javascript:;" onclick="showChange();">Forgot it？</a></td>
                            <td><input type="password" value="" class="change_text" id="old_pwd" /></td>
                        </tr>
                        <tr><td colspan="2" style="height:30px;"></td></tr>
                        <tr>
                        	<td>New password</td>
                            <td><input type="password" value="" class="change_text" id="new_pwd" /></td>
                        </tr>
                        <tr><td colspan="2" style="height:30px;"></td></tr>
                        <tr>
                        	<td>Type it again</td>
                            <td><input type="password" value="" class="change_text" id="again_pwd" /></td>
                        </tr>
                        <tr>
                        	<td colspan="2" style="height:65px;background:url(images/change_pwd_bg.png) no-repeat left bottom;"></td>
                        </tr>
                        <tr>
                        	<td colspan="2" style="height:32px;"></td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                            <td><input type="reset" value="Cancel" class="changeCancel" />
                            	<input type="submit" value="Change Password" class="changeBtn" onclick="return changeForm();" />
                            </td>
                        </tr>
                	</table>
                </div>            
    		</div>
    	</div>
    </div>
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/form.js"></script>

</body>
</html>