<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(CP_BASE_TEMP."template/include/title.php")?>
<link href="<?php echo CP_BASE_STATIC;?>public/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo CP_BASE_STATIC;?>public/css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<div class="ind_video">
		<div class="ind_video_layer"></div>
        <div class="ind_video_main"><div id="a1"></div></div>
	</div>
	<?php include(CP_BASE_TEMP."template/include/header.php")?>
    <?php include(CP_BASE_TEMP."template/include/top_link.php")?>
  	<div class="ecs_top">
		<div class="container">
    		<?php echo $detail['content1']?>
    	</div>
    </div>
    <div class="tcs_design">
    	<div class="container clearfix">
<!--        	<div class="design_left fl">-->
<!--        		Designed in comply with IEC 61508 and TÜV SIL 3 standards,TCS-900 SIS (Safety Instrumented System) is SUPCON’s latest system especially made for critical safety and control throughout the industry, and to ensure the safety and availability of plant operation.-->
<!--        	</div>-->
<!--            <div class="design_line fl"></div>-->
<!--            <div class="design_right fr">-->
<!--            	<div class="design_tit">TCS-900 system is tailored for a wide range of applications including:</div>-->
<!--                <div class="design_list">-->
<!--                	<ul class="clearfix">-->
<!--                    	<li><i class="design_break1"></i>-->
<!--                        	<p>ESD</p>-->
<!--                        </li>-->
<!--                    	<li><i class="design_break2"></i>-->
<!--                        	<p>BMS</p>-->
<!--                        </li>-->
<!--                    	<li><i class="design_break3"></i>-->
<!--                        	<p>FGS</p>-->
<!--                        </li>-->
<!--                    	<li><i class="design_break4"></i>-->
<!--                        	<p>CCS</p>-->
<!--                        </li>-->
<!--                    	<li><i class="design_break5"></i>-->
<!--                        	<p>Others</p>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
			<?php echo $detail['content2']?>
        </div>
    </div>
    <div class="tcs_system">
    	<div class="container clearfix">
        	<div class="tcs_system_left fl"><img src="<?php echo CP_BASE_STATIC;?>public/img/tsc_system_pic.png" width="249" height="660" /></div>
    		<div class="tcs_system_right fr">
    			<div class="ecs_main_tit"><?php echo $detail['para3'];?></div>
<!--    			<div class="ecs_main_nav tcs_system_nav">-->
<!--                	<ul class="clearfix">-->
<!--                    	<li class="on">Availability</li>-->
<!--                    	<li>Safety</li>-->
<!--                        <li>Scalability</li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--    			<div class="ecs_main_tabs">-->
<!--                	<div class="tcs_sys_list">-->
<!--						<ul>-->
<!--                        	<li>All safety module & TMR architecture, supporting 3-3-2-0 degrading</li>-->
<!--                            <li>Providing integral on-line diagnose and supporting on-line replacement without disturbing field wiring</li>-->
<!--                            <li>Supporting various protocols: SCnetIV, SafeEthernet, Modbus-RTU, Modbus-TCP, etc.</li>-->
<!--                            <li>Supporting up to 7 expansion racks, 2,048 I/O points in one control station</li>-->
<!--                            <li>Providing integral support for remote I/O modules up to 10 km from the main rack</li>-->
<!--                        </ul>-->
<!--                   	</div>-->
<!--                	<div class="tcs_sys_list">-->
<!--						<ul>-->
<!--                        	<li>Providing integral support for remote I/O modules up to 10 km from the main rack</li>-->
<!--                            <li>Conforming to EN 61326-1:2013, especially, Surge, ESD and Burst can reach 4A level</li>-->
<!--                            <li>Conforming to ANSI/ISA-S74.01 G3 gas corrosion standard</li>-->
<!--                            <li>Capable to withstand harsh industrial environment</li>-->
<!--                        </ul>-->
<!--                   	</div>-->
<!--                	<div class="tcs_sys_list">-->
<!--						<ul>-->
<!--                        	<li>Nearly 100% fault coverage and fail-safe operation under fault scenario in ESD application</li>-->
<!--                            <li>5-level voting, supporting multi-level fault isolation</li>-->
<!--                            <li>Information security functions conforming to IEC 62443</li>-->
<!--                        </ul>-->
<!--                   	</div>-->
<!--    			</div>-->
				<?php echo $detail['content3'];?>
    		</div>
    	</div>
    </div>
    <div class="ecs_view">
    	<div class="container">
        	<div class="ecs_view_tit"><?php echo $detail['para3'];?></div>
<!--        	<div class="ecs_view_info">-->
<!--            	TCS-900 is a highly flexible and reliable system, which not only meets the requirements of<br />-->
<!--				the safety standards, but can easily integrate Basic Process Control Systems (BPCS) required<br /> -->
<!--				for plant wide control & management.-->
<!--			</div>-->
			<?php echo $detail['content4'];?>
        </div>
        <div class="tcs_view_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/tcs_arch_pic.png" /></div>
    </div>
    <div class="tcs_safe clearfix">
<!--    	<div class="tcs_safe_left fl">-->
<!--        	<div class="tcs_safe_info fr">-->
<!--            	<div class="tcs_safe_tit">Safe & Intelligent Integration</div>-->
<!--                <div class="tcs_safe_txt">-->
<!--        			<p>An independent while seamless integration for TCS-900 with other DCS products were planned ever since its initial product design, which brings significant benefits to users. </p>-->
<!--                    <p>With SUPCON’s DCS-SIS integration solution, users can access to real-time data with a common HMI, and get direct inter-system connection via SCnet IV.</p>-->
<!--        		</div>-->
<!--        	</div>-->
<!--            <div class="clear"></div>-->
<!--        </div>-->
<!--        <div class="tcs_safe_right fr">-->
<!--        	<div class="tcs_safe_pic"><img src="--><?php //echo CP_BASE_STATIC;?><!--public/img/tsc_safe_pic.png" width="622" height="683" /></div>-->
<!--        </div>-->
		<?php echo $detail['content5'];?>
    </div>
	<div class="indust_video ecs_video">
    	<div class="indust_video_title">Video</div>    
		<div class="indust_video_main">
			<div class="indust_video_pic">
				<img src="<?php echo '/'.get_imgurl($detail['videoImg']);?>" width="834" height="467" />
                <div class="indust_video_pic_yin"><a href="javascript:;" onclick="openVideo()"></a></div>
			</div>
		</div>
	</div>
	<div class="indust_more" style="background:url(img/indust_more_bg.jpg) no-repeat center 200px;">
    	<div class="indust_more_cont">
			<?php include(CP_BASE_TEMP."template/include/explore_more.php")?>
        </div>
    </div>
	<?php include(CP_BASE_TEMP."template/include/footer.php")?>
</div>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/common.js"></script>
<script src="<?php echo CP_BASE_STATIC;?>public/ckplayer/ckplayer.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/magnifier.js"></script>
<script type="text/javascript">
    $('#demo1').banqh({
        box:"#demo1",//总框架
        pic:"#ban_pic1",//大图框架
        pnum:"#ban_num1",//小图框架
        pop_div:"#demo2",//弹出框框架
        pop_pic:"#ban_pic2",//弹出框图片框架
        pop_xx:".pop_up_xx",//关闭弹出框按钮
        mhc:".mhc",//朦灰层
        autoplay:false,//是否自动播放
        interTime:5000,//图片自动切换间隔
        delayTime:400,//切换一张图片时间
        pop_delayTime:400,//弹出框切换一张图片时间
        order:0,//当前显示的图片（从0开始）
        picdire:true,//大图滚动方向（true为水平方向滚动）
        mindire:true,//小图滚动方向（true为水平方向滚动 false为垂直滚动）
        min_picnum:5,//小图显示数量
        pop_up:false//大图是否有弹出框
    });
    $('.small').mag({
        box:".small li",//小图框架
        box_w:450,//小图宽
        box_h:450,//小图高
        box2:".big",//大图框架
        box2_w:1500,//大图宽
        box2_h:1500,//大图高
        range:".range",//放大镜框架
        range_w:100,//小图放大镜范围宽度
        range_h:100//小图放大镜范围高度
    });
</script>
<script type="text/javascript">
	var flashvars={
		p:0,
		e:1,
		ht:'20',
		hr:'http://www.ckplayer.com'
		};
	var video=['<?php echo $detail['video'];?>'];
	var support=['all'];
	CKobject.embedHTML5('a1','ckplayer_a1',600,400,video,flashvars,support);	
</script>
</body>
</html>