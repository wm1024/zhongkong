<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<title>合作 - <?=get_base('hxcms_webtitle')?></title>
<meta name="keywords" content="<?=get_base('Keywords')?>" />
<meta name="description" content="<?=get_base('regkeywords')?>" /> 
<link rel="stylesheet"  href="/public/css/base.css" />
<link rel="stylesheet"  href="/public/css/common.css" />
</head>
<body>
<!--header-->
{include file="include/header"}
<!--header-->
<div class="banner" style="background:url(/public/images/banner.jpg) center no-repeat;"></div>


<!--合作伙伴-->
<div class="partner">
  <div class="title">
    <div class="title_main" style="background:url(/public/images/partner_title.png) center top no-repeat;"></div>
  </div>
  <div class="partner_tips">开放&nbsp;&nbsp;&nbsp;融合&nbsp;&nbsp;&nbsp;合作&nbsp;&nbsp;&nbsp;共赢</div>
  <div class="partner_scroll">
    <div class="hd">
      <a class="next"></a>
      <a class="prev"></a>
    </div>
    <div class="bd">
      <ul class="picList">
      <li>
        <?php
        if (! empty($link)){
            foreach($link as $k=>$v){
                $j = ($k+1);
                echo '<img src="/'.get_imgurl($v['pic']).'" width="200" height="88" />';        
                if ($j%5 == 0){
                    echo '</li><li>';
                }
            }
        }
        ?>
      </li>
      </ul>
    </div>
  </div>
</div>
<!--合作伙伴-->

<!--合作案列-->
<div class="case">
  <div class="title">
    <div class="title_main" style="background:url(/public/images/case_title.png) center top no-repeat;"></div>
  </div>
  <div class="container">
    <div class="case_scroll">
      <div class="bd">
        <ul class="picList">
<?php
if (! empty($coop)){
  foreach($coop as $k=>$v){
      echo '<li><a onClick="popWin.showWin(\'980\',\'100%\',\'/tv/det-id-'.$v['id'].'.html\');" href="javascript:;"><img src="/'.get_imgurl($v['pic']).'" width="237" height="334" /><p>'.get_classname($v['classid']).'：'.$v['title'].'</p></a></li>';
  }
}
?>
        </ul>
      </div>
      <div class="hd">
        <ul></ul>
      </div>
  </div>
  </div>
</div>
<!--合作案列-->



{include file="include/footer"}

<script src="/public/js/jquery-1.9.1.min.js"></script>
<script src="/public/js/common.js"></script>
<script src="/public/js/popwin.js"></script>
<script src="/public/js/jquery.SuperSlide.2.1.1.js"></script>
<script>
	jQuery(".partner_scroll").slide({titCell:".hd ul",mainCell:".bd ul",autoPage:true,effect:"left",autoPlay:false,vis:5,scroll:5,trigger:"click",interTime:3000,delayTime:1000});
	jQuery(".case_scroll").slide({titCell:".hd ul",mainCell:".bd ul",autoPage:true,effect:"left",autoPlay:true,vis:4,scroll:4,trigger:"click",interTime:3000,delayTime:1000});
</script>
</body>
</html>