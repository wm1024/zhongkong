<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<title><?=get_base('hxcms_webtitle')?></title>
<meta name="keywords" content="<?=get_base('Keywords')?>" />
<meta name="description" content="<?=get_base('regkeywords')?>" />
<link rel="stylesheet"  href="/public/css/base.css" />
<link rel="stylesheet"  href="/public/css/common.css" />
</head>
<body>
<!--header-->
{include file="include/header"}
<!--header-->

<div class="foucebox" style="margin:0 auto">
    <div class="bd">
        <ul>
<?php
if (! empty($ads)){
    foreach($ads as $K=>$v){
        echo '<li><a href="'. $v['content'] .'" style="background:url(/'. get_imgurl($v['pic']) .') #000  center no-repeat;"></a></li>';
    }
}
?>
        </ul>
    </div>

    <div class="hd">
        <div  class="hoverBg" ></div>
        <ul>
<?php
if (! empty($ads)){
    foreach($ads as $K=>$v){
        echo '<li><a href="javascript:void();"><img src="/'. get_imgurl($v['pic2']) .'"></a></li>';
    }
}
?>
        </ul>
    </div>
</div>



<?php if (! empty($tv1137)){ ?>
<div class="i_film">
  <div class="i_title">
    <div class="i_title_main" style="background:url(/public/images/i_film_title.png) center no-repeat;"></div>
  </div>
  <div class="i_film_scroll">
    <div class="hd">
      <a class="next"></a>
      <a class="prev"></a>
    </div>
    <div class="bd">
      <ul class="picList">
<?php
if (! empty($tv1137)){
    foreach($tv1137 as $k=>$v){

echo '<li><a onClick="popWin.showWin(\'980\',\'100%\',\'/tv/det-id-'. $v['id'] .'.html\');" href="javascript:;"><img src="/'. get_imgurl($v['pic']) .'" width="330" height="465" />
<div class="i_film_con">
<h1>'.str_len($v['title'],14).'</h1>
<p>'.str_len(replace_content($v['content']),34).'</p>
</div><div class="layer1"></div></a></li>';

    }
}
?>
      </ul>
    </div>
  </div>
</div>
<?php } ?>



<?php if (! empty($tv1138)){ ?>
<div class="i_tv">
  <div class="i_title">
    <div class="i_title_main" style="background:url(/public/images/i_tv_title.png) center no-repeat;"></div>
  </div>
  <div class="i_tv_scroll">
    <div class="bd">
      <ul class="picList">
<?php
if (! empty($tv1138)){
    foreach($tv1138 as $k=>$v){

echo '<li><a onClick="popWin.showWin(\'980\',\'100%\',\'/tv/det-id-'. $v['id'] .'.html\');" href="javascript:;"><img src="/'. get_imgurl($v['pic2']) .'" width="346" height="211" />
<div class="i_film_con">
<h1>'.str_len($v['title'],14).'</h1>
<p>'.str_len(replace_content($v['content']),34).'</p>
</div><div class="layer1"></div></a></li>';

    }
}
?>
      </ul>
    </div>
    <div class="hd">
     <ul></ul>
    </div>
  </div>
</div>
<?php } ?>




<div class="i_news">
  <div class="i_title">
    <div class="i_title_main" style="background:url(/public/images/i_news_title.png) center no-repeat;"></div>
  </div>
  <div id="slideBox" class="i_news_scroll">
    <div class="hd">
        <ul>
<?php
if (! empty($news)){
    foreach ($news as $k=>$v) {

      echo '<li><a onClick="popWin.showWin(\'980\',\'100%\',\'/news/detail-id-'. $v['id'] .'.html\');" href="javascript:;">
      <h1>'.str_len($v['title'],18).'</h1>
      <div class="i_news_con">'. str_len(replace_content($v['content']),70).'</div>
      </a></li>';
    }
}
?>
        </ul>
    </div>
    <div class="bd">
        <ul>
<?php
if (! empty($news)){
    foreach ($news as $k=>$v) {
      echo '<li><a onClick="popWin.showWin(\'980\',\'100%\',\'/news/detail-id-'. $v['id'] .'.html\');" href="javascript:;"><img src="/'. get_imgurl($v['pic']) .'" width="431" height="267" /><div class="i_news_layer">'.str_len($v['title'],18).'</div></a></li>';
    }
}
?>
        </ul>
    </div>
  </div>
</div>

{include file="include/footer"}
<script src="/public/js/jquery-1.9.1.min.js"></script>
<script src="/public/js/common.js"></script>
<script src="/public/js/jquery.SuperSlide.2.1.1.js"></script>
<script src="/public/js/popwin.js"></script>
<script type="text/javascript">
	jQuery(".foucebox").slide({ mainCell:".bd ul", effect:"fold", autoPlay:true, delayTime:300, triggerTime:50, startFun:function(i){
			jQuery(".foucebox .hoverBg").animate({"margin-left":79*i},100);
		}
	});
	
	jQuery(".i_film_scroll").slide({titCell:".hd ul",mainCell:".bd ul",autoPage:true,effect:"leftLoop",autoPlay:false,vis:3,scroll:1,trigger:"click",delayTime:1000,interTime:3000});
	jQuery(".i_tv_scroll").slide({titCell:".hd ul",mainCell:".bd ul",autoPage:true,effect:"left",autoPlay:false,vis:3,scroll:1,trigger:"click",delayTime:1000,interTime:3000});
	jQuery(".i_news_scroll").slide({mainCell:".bd ul",autoPlay:false,delayTime:1000,interTime:3000});
</script>
</body>
</html>