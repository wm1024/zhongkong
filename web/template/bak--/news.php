<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<title>最新资讯 - <?=get_base('hxcms_webtitle')?></title>
<meta name="keywords" content="<?=get_base('Keywords')?>" />
<meta name="description" content="<?=get_base('regkeywords')?>" />
<link rel="stylesheet"  href="/public/css/base.css" />
<link rel="stylesheet"  href="/public/css/common.css" />
</head>
<body>
<!--header-->
{include file="include/header"}
<!--header-->
<div class="banner" style="background:url(/public/images/banner3.jpg) center no-repeat;"></div>


<!--最新资讯-->
<div class="studio">
  <div class="title">
    <div class="title_main" style="background:url(/public/images/news_title.png) center top no-repeat;"></div>
  </div>
  <div class="container">

    <div class="search">
        <form name="myform" id="myform" action="/news/index-act-search.html" method="post">
        <input typr="text" name="keyword" value="<?php if (! empty($keyword)) { echo $keyword; }else{ echo '历史资讯搜索'; }?>" _reg="历史资讯搜索" class="search_text" />
        <input type="submit" name="btn" value="" class="search_btn" />
        </form>
    </div> 
    
    <div class="clearfix">
      <ul class="news_list fl">

<?php
if (! empty($list_arr)){
foreach($list_arr as $k=>$v){

    echo '<li><a onClick="popWin.showWin(\'980\',\'100%\',\'/news/detail-id-'.$v['id'].'.html\');" href="javascript:;">
    <div class="news_list_img fl"><img src="/'. get_imgurl($v['pic']) .'" width="155" height="155"/><div class="circle"></div></div>
    <div class="news_list_main fl">
    <div class="news_list_title"><h1 class="fl" title="'.$v['title'].'">'. str_len($v['title'],22) .'</h1><h2 class="fr">'. date('Y/m/d', strtotime($v['datetime'])).'</h2></div>
    <div class="news_list_con">'. str_len(replace_content($v['content']),120) .'</div>
    <div class="news_list_more">查看详情 ></div>
    </div>
    </a></li>';

}
}
?>

      <li style="border:0;height:20px;">
          <div class="page">
          <?php echo $page_str;?>
          </div>
      </li>
      </ul>




      <div class="new_film fr">
        <div class="new_film_title fr">新片热映</div>
        <div class="clear"></div>
        <a href="#" target="_blank"><img src="/public/img/case.jpg" width="273" height="385" /></a>
        <a href="#" target="_blank"><img src="/public/img/download5.jpg" width="273" height="385" /></a>
      </div>
    </div>
  </div>
</div>
<!--最新资讯-->


{include file="include/footer"}

<script src="/public/js/jquery-1.9.1.min.js"></script>
<script src="/public/js/common.js"></script>
<script src="/public/js/popwin.js"></script>
</body>
</html>