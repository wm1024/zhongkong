<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<title>编剧雅集 - <?=get_base('hxcms_webtitle')?></title>
<meta name="keywords" content="<?=get_base('Keywords')?>" />
<meta name="description" content="<?=get_base('regkeywords')?>" /> 
<link rel="stylesheet"  href="/public/css/base.css" />
<link rel="stylesheet"  href="/public/css/common.css" />
</head>
<body>
<!--header-->
{include file="include/header"}
<!--header-->
<div class="banner" style="background:url(/public/images/banner4.jpg) center no-repeat;"></div>


<!--编剧雅集-->
<div class="aristo_con">
  <div class="title">
    <div class="title_main" style="background:url(/public/images/aristo_title.png) center top no-repeat;"></div>
  </div>
  <div id="slideBox" class="aristo">
    <div class="hd">
      <ul>
          <?php
          if (! empty($class145_arr)){
              foreach($class145_arr as $k=>$v){
                  echo '<li>'.$v['classname'].'</li>';
              }
          }
          ?>
      </ul>
      <a class="prev" href="javascript:void(0)"></a>
      <a class="next" href="javascript:void(0)"></a>
    </div>
      
    <div class="bd">
      <ul>
        <?php
        if (! empty($class145_arr)){
            foreach($class145_arr as $k=>$v){

                $info = explode(chr(13), $v['classinfo']);
                if (! empty($info[0])) $title = $info[0];
                if (! empty($info[1])) $content = $info[1];

                echo '<li>
                <div class="aristo_top">
                <div class="aristo_title">'.$title.'</div>
                <div class="aristo_mes">'.$content.'</div>
                </div><div class="aristo_main clearfix">';
                echo get_aristo_li($v['id']);
                echo '</div></li>';
            }
        }
        ?>
      </ul>
    </div>
  </div>
</div>
<!--编剧雅集-->


{include file="include/footer"}

<script src="/public/js/jquery-1.9.1.min.js"></script>
<script src="/public/js/common.js"></script>
<script src="/public/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript">
	jQuery(".aristo").slide({mainCell:".bd ul",autoPlay:false,effect:"left",trigger:"click",delayTime:700});
</script>
<script src="/public/js/popwin.js"></script>
</body>
</html>