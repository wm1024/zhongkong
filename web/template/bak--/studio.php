<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<title>直播间 - <?=get_base('hxcms_webtitle')?></title>
<meta name="keywords" content="<?=get_base('Keywords')?>" />
<meta name="description" content="<?=get_base('regkeywords')?>" />
<link rel="stylesheet"  href="/public/css/base.css" />
<link rel="stylesheet"  href="/public/css/common.css" />
</head>
<body>
<!--header-->
{include file="include/header"}
<!--header-->
<div class="banner" style="background:url(/public/images/banner3.jpg) center no-repeat;"></div>
<a name="studio_tolist"></a>

<!--直播间-->
<div class="studio">
  <div class="title">
    <div class="title_main" style="background:url(/public/images/studio_title.png) center top no-repeat;"></div>
  </div>
  <div class="container">
    <div class="studio_nav"><a href="/studio/index-cid-1.html#studio_tolist" <?php if ($cid == 1) echo 'class="on"'; ?>>电影<i class="arrow1"></i></a><a href="/studio/index-cid-2.html#studio_tolist" <?php if ($cid == 2) echo 'class="on"'; ?>>电视剧<i class="arrow1"></i></a></div>
    <div class="studio_main">
      <ul class="studio_list clearfix">


<?php
if (! empty($list_arr)){
    foreach ($list_arr as $k=>$v) {

      echo '<li><a href="/studio/detail-id-'.$v['id'].'.html">
      <img src="/'.get_imgurl($v['pic']).'" width="338" height="188" />
      <div class="studio_con">
      <div class="studio_title">'.$v['title'].'</div>
      <div class="studio_mes clearfix">
      <div class="studio_num fl">'.$v['hits'].'</div>
      <div class="studio_time fl">2014上映</div>
      </div>
      </div>
      </a></li>';

    }
}
?>


      </ul>
      <div class="page"><?php echo $page_str;?></div>
    </div>
  </div>
</div>
<!--直播间-->


{include file="include/footer"}

<script src="/public/js/jquery-1.9.1.min.js"></script>
<script src="/public/js/common.js"></script>
</body>
</html>