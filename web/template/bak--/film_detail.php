<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<title>电影 - <?=get_base('hxcms_webtitle')?></title>
<meta name="keywords" content="<?=get_base('Keywords')?>" />
<meta name="description" content="<?=get_base('regkeywords')?>" />
<link rel="stylesheet"  href="/public/css/base.css" />
<link rel="stylesheet"  href="/public/css/common.css" />
</head>
<body>
<a href="#" class="detail_prev"></a>
<a href="#" class="detail_next"></a>
<div class="detail film_detail">
  <div class="detail_posiiton"><a href="index.php">HOME</a> &gt; 电视剧 > 详情</div>

  <div id="vertical3" class="scrollbox3 clearfix">
  <div class="slyWrap3 example2">
    <div class="scrollbar3">
        <div class="handle"></div>
    </div>
    <div class="sly3" data-options='{ "scrollBy": 100, "startAt": 0 }'>
      <div class="detail_main">
        <div class="film_detail_top clearfix">
          <img src="/public/img/download1.jpg" width="352" height="502" class="fl"/>
          <div class="film_detail_con fr">
            <div class="film_detail_title">《建党伟业》</div>
            <div class="film_detail_mes">类型：剧 情/历史</div>
            <table cellpadding="0" cellspacing="0" class="film_detail_table mrgT30" width="404">
              <tr valign="top"><th width="50">导演：</th><td width="354">韩三平 黄建新 </td></tr>
              <tr valign="top"><th>编剧：</th><td>董哲、郭俊立</td></tr>
              <tr valign="top"><th>演员：</th><td>刘烨 、陈坤、吴宇森、吴彦祖、周迅、汤唯、黄觉、李晨、张译、王柏杰、张一山、李沁、范志博、董璇</td></tr>
            </table>
            <a href="tv.php" class="about2_btn" target="_top">立即欣赏</a>
          </div>
        </div>
        <div class="film_detail_main">
          <div class="top">故事梗概：</div>
          <div class="con">
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1911年冬，孙中山从海外归来，翌年 1月 1日就任临时大总统。与此同时毛泽东参加了光复新军。很快，袁世凯凭借强大的军事实力和在皇室中的威信，从孙文手中取得了大元帅之身份，北洋民国政府成立。同年 10月，国民党成立。</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1914年夏天第一次世界大战爆发，1915年袁世凯同日本定下密约二十一条并且更改国体，自封为中华帝国皇帝，自此走上了一条不归路。孙中山与蔡锷成立护国军，出师讨袁。很快袁世凯便在举国声讨中黯然死去了。</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1911年冬，孙中山从海外归来，翌年 1月 1日就任临时大总统。与此同时毛泽东参加了光复新军。很快，袁世凯凭借强大的军事实力和在皇室中的威信，从孙文手中取得了大元帅之身份，北洋民国政府成立。同年 10月，国民党成立。</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1914年夏天第一次世界大战爆发，1915年袁世凯同日本定下密约二十一条并且更改国体，自封为中华帝国皇帝，自此走上了一条不归路。孙中山与蔡锷成立护国军，出师讨袁。很快袁世凯便在举国声讨中黯然死去了。</p>
          </div>
          
          <div class="top">获得奖项：</div>
          <div class="awards">
            <ul>
              <li>•《建党伟业》获第12届全国精神文明建设"五个一工程"奖；第14届中国电影华表奖优秀故事片奖、优秀导演奖和优秀编剧奖。</li>
            </ul>
          </div>
          
          <div class="top">剧照欣赏：</div>
          <div class="still">
            <?php for($i=0;$i<4;$i++) {?>
            <img src="/public/img/download1.jpg" width="176" height="214"/>
            <?php }?>
          </div>
        </div>
      </div>
    </div>
  </div><!--slyWrap-->

</div>
</div>

<script src="/public/js/jquery-1.9.1.min.js"></script>
<script src="/public/js/common.js"></script>
<script type="text/javascript" src="/public/js/jquery.sly.js"></script>
<script type="text/javascript" src="/public/js/jquery.easing-1.3.min.js"></script>
<script type="text/javascript" src="/public/js/gd.js"></script>
</body>
</html>