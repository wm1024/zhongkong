<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<title>电影 - <?=get_base('hxcms_webtitle')?></title>
<meta name="keywords" content="<?=get_base('Keywords')?>" />
<meta name="description" content="<?=get_base('regkeywords')?>" />
<link rel="stylesheet"  href="/public/css/base.css" />
<link rel="stylesheet"  href="/public/css/common.css" />
</head>
<body>
<!--header-->
{include file="include/header"}
<!--header-->
<div class="banner" style="background:url(/public/images/banner3.jpg) center no-repeat;"></div>


<!--电视剧-->
<div class="tv_bg">
  <div class="title">
    <div class="title_main" style="background:url(/public/images/film_title.png) center top no-repeat;"></div>
  </div>
  <div class="container">
    <div class="screen">
      <div class="screen_select fl">
        <p>类型</p>
        <i class="arrow2"></i>
        <ul>
          <?php
          if (! empty($class9991)){
              echo '<li><a href="/film/index-type--year-'.$year.'-keys-'.$keys.'.html">全部</a></li>';
              foreach($class9991 as $k=>$v){

                  if ($v['id'] == $type){ $aa = ' style="background:#eee;"'; }else{ $aa = ''; }
                  echo '<li '.$aa.'><a href="/film/index-type-'.$v['id'].'-year-'.$year.'-keys-'.$keys.'.html">'.$v['classname'].'</a></li>';
              }
          }
          ?>
        </ul>
      </div>
      <div class="screen_select fl">
        <p>年份</p>
        <i class="arrow2"></i>
        <ul>
          <?php
          if (! empty($class9992)){
              echo '<li><a href="/film/index-type-'.$type.'-year--keys-'.$keys.'.html">全部</a></li>';
              foreach($class9992 as $k=>$v){

                  if ($v['id'] == $year){ $bb = ' style="background:#eee;"'; }else{ $bb = ''; }
                  echo '<li '.$bb.'><a href="/film/index-type-'.$type.'-year-'.$v['id'].'-keys-'.$keys.'.html">'.$v['classname'].'</a></li>';
              }
          }
          ?>
        </ul>
      </div>
      <div class="screen_search fl">
        <input type="text" name="keys" id="keys" value="<?php if (! empty($keys)){ echo $keys; }else{ echo '请输入关键字'; } ?>" _reg="请输入关键字" class="text fl" />
        <input type="button" name="btn" id="btn" onClick="go_search();" value="" class="btn fl" />
      </div>
    </div>
    <div class="tv_main">
      <ul class="tv_list clearfix">




<?php
if (! empty($list_arr)){
  foreach ($list_arr as $k => $v) {

    echo '<li><a onClick="popWin.showWin(\'980\',\'100%\',\'/film/det-id-'.$v['id'].'.html\');" href="javascript:;">
    <img src="/'.get_imgurl($v['pic']).'" width="237" height="335"/>
    <div class="tv_list_con">
    <h1>'.$v['title'].'</h1>
    <p>全剧：'.$v['epis'].'集</p>
    </div>
    </a></li>';

  }
}


?>



      </ul>
      <div class="page"><?php echo $page_str;?></div>
    </div>
  </div>
</div>
<!--电视剧-->


{include file="include/footer"}

<script src="/public/js/jquery-1.9.1.min.js"></script>
<script src="/public/js/common.js"></script>
<script src="/public/js/popwin.js"></script>
<script type="text/javascript">
  
function go_search(){

    var keys = $('#keys').val();
    if (keys != '请输入关键字'){
      keys1 = encodeURI(keys);
      window.location.href="/film/index-type-<?php echo $type;?>-year-<?php echo $year;?>-keys-"+keys1+".html";
    }else{
      window.location.href="/film/index-type-<?php echo $type;?>-year-<?php echo $year;?>-keys-.html";
    }
}


</script>
</body>
</html>