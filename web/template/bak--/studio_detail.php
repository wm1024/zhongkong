<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<title><?php echo $det['title'];?>|直播间 - <?=get_base('hxcms_webtitle')?></title>
<meta name="keywords" content="<?=get_base('Keywords')?>" />
<meta name="description" content="<?=get_base('regkeywords')?>" />
<link rel="stylesheet"  href="/public/css/base.css" />
<link rel="stylesheet"  href="/public/css/common.css" />
</head>
<body>
<!--header-->
{include file="include/header"}
<!--header-->
<div class="banner" style="background:url(/public/images/banner3.jpg) center no-repeat;"></div>


<!--直播间-->
<div class="studio_detail container">
  <div class="detail_posiiton"><a href="/index.html">HOME</a> &gt; 直播间 > 详情</div>
  <div class="studio_vedio clearfix">
    <div class="studio_vedio_left fl">
      <embed src="<?php echo $det['url'];?>" allowFullScreen="true" quality="high" width="776" height="482" align="middle" allowScriptAccess="always" type="application/x-shockwave-flash"></embed>
    </div>
    <div class="studio_vedio_right fr">
      <div class="studio_vedio_right_top">《<?php echo $det['title'];?>》相关视频</div>
      <ul>


      <!-- 正在播放部分 start -->
      <li class="on"><a href="#">
      <img src="/<?php echo get_imgurl($det['pic']);?>" width="112" height="70" class="fl" />
      <div class="layer">正在播放...</div>
      <div class="studio_vedio_right_con fl">
      <h1><?php echo $det['title'];?></h1>
      <p>类型：<?php echo get_classname($det['classid']);?></p>
      </div></a></li>
      <!-- 正在播放部分 end -->


<?php
if (! empty($list)){
    foreach($list as $k=>$v){

echo '<li><a href="/studio/detail-id-'.$v['id'].'.html">
<img src="/'.get_imgurl($v['pic']).'" width="112" height="70" class="fl" />
<div class="layer">正在播放...</div>
<div class="studio_vedio_right_con fl">
<h1>'.$v['title'].'</h1>
<p>类型：'.get_classname($v['classid']).'</p>
</div></a></li>';

    }
}
?>




      </ul>
    </div>
  </div>
  <div class="studio_detail_title">
    <h1><?php echo $det['title'];?></h1>
    <p>&nbsp;&nbsp;&nbsp;类型：<?php echo get_classname($det['classid']);?></p>
  </div>
  <div class="studio_detail_main">
    <div class="studio_detail_main_title">故事梗概：</div>
    <div>
      <?php echo $det['content'];?>
    </div>
  </div>
</div>
<!--直播间-->


{include file="include/footer"}

<script src="/public/js/jquery-1.9.1.min.js"></script>
<script src="/public/js/common.js"></script>
</body>
</html>