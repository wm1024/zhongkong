<?php
$contact_str = get_base('User_Config');
$contact_arr = explode('‖', $contact_str);
?>
<a href="javascript:;" id="goTop"></a>
<div id="footer">
  <div class="footer1">
    <div class="container">
      <div class="footer1_left fl">
        <a href="index.php" class="footer_logo"></a>
        <div class="link">
          <p>-------- 友情链接 --------</p>
          <ul>
            <?php get_footer_link(); ?>
          </ul>
        </div>
      </div>
      <div class="line fl"></div>
      <div class="footer1_mid fl">
        <div class="footer1_title">联系</div>
        <div class="footer1_mid_main">
          <div class="footer_add"><?php echo $contact_arr[5]; ?></div>
          <div class="footer_email"><?php echo $contact_arr[4]; ?></div>
          <div class="footer_tel"><?php echo $contact_arr[2]; ?></div>
        </div>
      </div>
      <div class="line fl"></div>
      <div class="footer1_right fl">
        <div class="footer1_title">联系</div>
        <div class="footer1_right_main">
          <a href="#" target="_blank" class="sina"></a>
          <a href="javascript:;" class="wechat"></a>
          <a href="javascript:;" class="other"></a>
          <div class="footer1_right_con fl">
            <div class="wechat_path"><img src="/public/img/ewm.png" width="101" height="101"/><p>扫一扫添加微信号</p></div>
            <div class="other_path" style="display:none;"><img src="/public/img/ewm.png" width="101" height="101"/><p>持续开放...</p></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer2"><div class="container">Copyright @ 2014  蓝色星空影业 All rights reserved.by:<a href="htttp://www.lebang.com" target="_blank">lebang.com</a></div></div>
</div>
