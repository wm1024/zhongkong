<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<title><?php echo $det['title'];?>|电视剧/电影 - <?=get_base('hxcms_webtitle')?></title>
<meta name="keywords" content="<?=get_base('Keywords')?>" />
<meta name="description" content="<?=get_base('regkeywords')?>" />
<link rel="stylesheet"  href="/public/css/base.css" />
<link rel="stylesheet"  href="/public/css/common.css" />
</head>
<body>

<?php if(! empty($prev_arr['id'])){ ?>
<a href="<?php echo '/tv/det-id-'.$prev_arr['id'].'.html'; ?>" title="上一条" class="detail_prev"></a>
<?php } ?>

<?php if(! empty($next_arr['id'])){ ?>
<a href="<?php echo '/tv/det-id-'.$next_arr['id'].'.html'; ?>" title="下一条" class="detail_next"></a>
<?php } ?>

<div class="detail film_detail">
  <div class="detail_posiiton"><a href="/index.html" target="_blank">HOME</a> &gt; <?php echo get_classname($det['classid']);?> > 详情</div>

  <div id="vertical3" class="scrollbox3 clearfix">
  <div class="slyWrap3 example2">
    <div class="scrollbar3">
        <div class="handle"></div>
    </div>
    <div class="sly3" data-options='{ "scrollBy": 100, "startAt": 0 }'>
      <div class="detail_main">
        <div class="film_detail_top clearfix">
          <img src="/<?php echo get_imgurl($det['pic']);?>" width="352" height="502" class="fl"/>
          <div class="film_detail_con fr">
            <div class="film_detail_title"><?php echo $det['title'];?></div>
            <div class="film_detail_mes">类型：<?php echo get_classname($det['classid1']);?></div>
            <table cellpadding="0" cellspacing="0" class="film_detail_table mrgT30" width="404">
              <tr valign="top"><th width="50">导演：</th><td width="354"><?php echo $det['dire'];?></td></tr>
              <tr valign="top"><th>编剧：</th><td><?php echo $det['scree'];?></td></tr>
              <tr valign="top"><th>演员：</th><td><?php echo $det['actor'];?></td></tr>
            </table>
            <a href="<?php if (! empty($det['url'])){ echo $det['url']; }else{ echo '#'; }?>" class="about2_btn" target="_top">立即欣赏</a>
          </div>
        </div>
        <div class="film_detail_main">
          <div class="top">故事梗概：</div>
          <div class="con">
            <?php echo $det['content'];?>
          </div>
          
          <?php if (! empty($det['awards'])){ ?>
          <div class="top">获得奖项：</div>
          <div class="awards">
            <ul>
              <?php
                  $awards_arr = explode('‖', $det['awards']);
                  foreach ($awards_arr as $k=>$v) {
                      echo '<li>•&nbsp;&nbsp;'.$v.'</li>';
                  }
              ?>
            </ul>
          </div>
          <?php } ?>
          
          <?php if (! empty($det['pic3'])){ ?>
          <div class="top">剧照欣赏：</div>
          <div class="still">
            <ul>
              <?php
                  $pic3_arr = explode('‖', $det['pic3']);
                  foreach ($pic3_arr as $k=>$v) {
                      echo '<img src="/'.get_imgurl($v).'" width="176" height="214"/>';
                  }
              ?>
            </ul>
          </div>
          <?php } ?>

        </div>
      </div>
    </div>
  </div><!--slyWrap-->

</div>
</div>

<script src="/public/js/jquery-1.9.1.min.js"></script>
<script src="/public/js/common.js"></script>
<script type="text/javascript" src="/public/js/jquery.sly.js"></script>
<script type="text/javascript" src="/public/js/jquery.easing-1.3.min.js"></script>
<script type="text/javascript" src="/public/js/gd.js"></script>
</body>
</html>