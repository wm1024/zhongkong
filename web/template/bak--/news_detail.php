<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<title><?php echo $det['title'];?>|最新资讯 - <?=get_base('hxcms_webtitle')?></title>
<meta name="keywords" content="<?=get_base('Keywords')?>" />
<meta name="description" content="<?=get_base('regkeywords')?>" />
<link rel="stylesheet"  href="/public/css/base.css" />
<link rel="stylesheet"  href="/public/css/common.css" />
</head>
<body>

<?php if(! empty($prev_arr['id'])){ ?>
<a href="<?php echo '/news/detail-id-'.$prev_arr['id'].'.html'; ?>" title="上一条" class="detail_prev"></a>
<?php } ?>

<?php if(! empty($next_arr['id'])){ ?>
<a href="<?php echo '/news/detail-id-'.$next_arr['id'].'.html'; ?>" title="下一条" class="detail_next"></a>
<?php } ?>

<div class="detail">
  <div class="detail_posiiton"><a href="/index.html">HOME</a> &gt; <?php echo get_classname($det['classid']);?> &gt; 详情</div>
  <div class="detail_title"><?php echo $det['title'];?></div>
  <div class="detail_summary">类型：<?php echo get_classname($det['classid']);?></div>
  
  <div id="vertical3" class="scrollbox3 clearfix">
  <div class="slyWrap3 example2">
    <div class="scrollbar3">
        <div class="handle"></div>
    </div>
    <div class="sly3" data-options='{ "scrollBy": 100, "startAt": 0 }'>
      <div class="news_detail">
      <?php echo $det['content'];?>
      </div>
        
    </div>
  </div><!--slyWrap-->

</div>
</div>

<script src="/public/js/jquery-1.9.1.min.js"></script>
<script src="/public/js/common.js"></script>
<script type="text/javascript" src="/public/js/jquery.sly.js"></script>
<script type="text/javascript" src="/public/js/jquery.easing-1.3.min.js"></script>
<script type="text/javascript" src="/public/js/gd.js"></script>
</body>
</html>