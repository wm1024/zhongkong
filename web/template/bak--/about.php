<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<title>关于我们 - <?=get_base('hxcms_webtitle')?></title>
<meta name="keywords" content="<?=get_base('Keywords')?>" />
<meta name="description" content="<?=get_base('regkeywords')?>" /> 
<link rel="stylesheet"  href="/public/css/base.css" />
<link rel="stylesheet"  href="/public/css/common.css" />
</head>
<body>
<!--header-->
{include file="include/header"}
<!--header-->
<div class="banner" style="background:url(/public/images/banner.jpg) center no-repeat;"></div>


<!--公司介绍-->
<div class="about1">
  <div class="title">
    <div class="title_main" style="background:url(/public/images/about_title.png) center top no-repeat;"></div>
  </div>
  <div class="container">
    <?php echo str_replace(chr(13), '<br />', $det['title']); ?>
  </div>
</div>
<!--公司介绍-->

<!--智取威虎山-->
<div class="about2">
  <div class="about2_banner" style="background:url(/<?php echo get_imgurl($det['title5']);?>) center top no-repeat;"></div>
  <div class="container">
    <div class="about2_title"><?php echo $det['title2'];?></div>
    <div class="about2_mes">
      <?php echo str_replace(chr(13), '<br />', $det['title3']); ?>
    </div>
    <div class="about2_con">
      <?php echo str_replace(chr(13), '<br />', $det['title31']); ?>
    </div>
    <a href="<?php echo $det['title4'];?>" target="_blank" class="about2_btn">立即欣赏</a>
  </div>
</div>
<!--智取威虎山-->

<!--公司简介-->
<div class="about3">
  <div class="container">
    <?php echo str_replace(chr(13), '<br />', $det['title6']); ?>
  </div>
</div>
<!--公司简介-->

<!--杂志-->
<div class="about4_banner" style="background:url(/public/images/banner2.jpg) center top no-repeat;"></div>
<div class="about4">
  <div class="container">
    <div class="about2_title">宣传册电子杂志</div>
    <div class="about4_con">蓝色星空宣传册电子稿在线预览</div>
    <a href="<?php echo $det['title7'];?>" target="_blank" class="about4_btn">阅读杂志</a>
  </div>
</div>
<!--杂志-->

<!--联系我们-->
<div class="about5">
  <div class="title">
    <div class="title_main" style="background:url(/public/images/contant_title.png) center top no-repeat;"></div>
  </div>
  <div class="container">
    <?php echo str_replace(chr(13), '<br />', $det['title8']); ?>
  </div>
  <div style="width:100%;height:476px;font-size:12px" id="map"></div>
</div>
<!--联系我们-->

{include file="include/footer"}

<script src="/public/js/jquery-1.9.1.min.js"></script>
<script src="/public/js/common.js"></script>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=D1c84e7ced620bb2e3651144955ccce3"></script>
<script type="text/javascript">
    //创建和初始化地图函数：
    function initMap(){
      createMap();//创建地图
      setMapEvent();//设置地图事件
      addMapControl();//向地图添加控件
      addMapOverlay();//向地图添加覆盖物
    }
    function createMap(){ 
      map = new BMap.Map("map"); 
      map.centerAndZoom(new BMap.Point(120.087948,30.286845),17);//120.087102,30.286308
    }
    function setMapEvent(){
      map.enableScrollWheelZoom();
      map.enableKeyboard();
      map.enableDragging();
      map.enableDoubleClickZoom();
    }
    function addClickHandler(target,window){
      target.addEventListener("click",function(){
        target.openInfoWindow(window);
      });
    }
    function addMapOverlay(){
      var markers = [
        {content:"杭州市文二西路683号西溪创意产业园24座",title:"蓝色星空影业有限公司",imageOffset: {width:-46,height:-21},position:{lat:30.286839,lng:120.087964}}
      ];
      for(var index = 0; index < markers.length; index++ ){
        var point = new BMap.Point(markers[index].position.lng,markers[index].position.lat);
        var marker = new BMap.Marker(point,{icon:new BMap.Icon("http://api.map.baidu.com/lbsapi/createmap/images/icon.png",new BMap.Size(20,25),{
          imageOffset: new BMap.Size(markers[index].imageOffset.width,markers[index].imageOffset.height)
        })});
        var label = new BMap.Label(markers[index].title,{offset: new BMap.Size(25,5)});
        var opts = {
          width: 200,
          title: markers[index].title,
          enableMessage: false
        };
        var infoWindow = new BMap.InfoWindow(markers[index].content,opts);
        //marker.setLabel(label);
		map.openInfoWindow(infoWindow,map.getCenter());
       addClickHandler(marker,infoWindow);
	   
        map.addOverlay(marker);
      };
    }
    //向地图添加控件
    function addMapControl(){
      var scaleControl = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
      scaleControl.setUnit(BMAP_UNIT_IMPERIAL);
      map.addControl(scaleControl);
      var navControl = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
      map.addControl(navControl);
      var overviewControl = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:true});
      map.addControl(overviewControl);
    }
    var map;
      initMap();
  </script>
</body>
</html>