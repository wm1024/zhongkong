<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(CP_BASE_TEMP."template/include/title.php")?>
<link href="<?php echo CP_BASE_STATIC;?>public/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo CP_BASE_STATIC;?>public/css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include(CP_BASE_TEMP."template/include/header.php")?>
  	<?php include(CP_BASE_TEMP."template/include/top_link.php")?>
	<div class="sub_cont">
    	<div class="container">
 			<div class="milestone_title">Milestone</div>
            <div class="milestone_list">
            	<ul>
            		<li class="first">
                    	<div class="milestone_info">
                    		<div class="milestone_info_list">
                    			<dl>
                                	<dt>SUPCON is founded</dt>
                                    <dt>SUPCON launched China’s first DCS - JX-100</dt>
                                    <dt style="border:none;">SUPCON launched China’s first paperless recorder</dt>
                                </dl>
                    		</div>
                            <i class="milestone_arrow"></i>
                    	</div>
                    	<div class="milestone_year">1993</div>
                    </li>
            		<li class="right">
                    	<div class="milestone_info">
                    		<p>SUPCON launched ECS-100, a DCS for middle scale plants</p>
                            <i class="milestone_arrow"></i>
                    	</div>
                    	<div class="milestone_year">2001</div>
                    </li>
                    <li>
                    	<div class="milestone_info">
                    		<div class="milestone_pic clearfix">
                            	<img class="fl" src="<?php echo CP_BASE_STATIC;?>public/img/milestone_pic.jpg" />
                                <div class="milestone_pic_title fr">SUPCON launched ECS-700, a DCS for large scale united plants</div>
                            </div>    
                            <p>EPA (Ethernet for Plant Automation) developed by SUPCON was officially recognized as international standard by IEC</p> 
                            <i class="milestone_arrow"></i>
                    	</div>
                    	<div class="milestone_year">2007</div>
                    </li>
                    <li class="right last">
                    	<div class="milestone_info">
                    		<p>SUPCON launched its first SIS - TCS-900, meantime passed SIL 3 certificate</p>
                            <i class="milestone_arrow"></i>
                    	</div>
                    	<div class="milestone_year">2015</div>
                    </li>
            	</ul>
    		</div>
    	</div>
    </div>    
	<?php include(CP_BASE_TEMP."template/include/footer.php")?>
</div>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/common.js"></script>

</body>
</html>