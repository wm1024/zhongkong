<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="sub_cont">
    	<div class="container">
 			<div class="milestone_title advance_title">Advanced Search</div>
            <form>
            <div class="advance_name">Search</div>
            <input type="text" value="Keywords" _reg="Keywords" class="advance_text" />
            <div class="advance_name">Category</div>
            <div class="advance_type clearfix">
            	<div class="advance_type_left fl">
                    <input type="checkbox" value="" id="categeory_all" onclick="ChkAllClick('chkSon','categeory_all')" />
                    <label for="categeory_all">All</label>
                </div>
                <div class="advance_type_right fr">
                	<ul class="clearfix">
                    	<li class="first">
                        	<input type="checkbox" value="" id="categeory1" name="reference" class="chkSon" onclick="ChkSonClick('chkSon','categeory_all')" />
                    		<label for="categeory1">Typical Reference</label>
                    	</li>
                    	<li class="second">
                        	<input type="checkbox" value="" id="categeory2" name="catalog" class="chkSon" onclick="ChkSonClick('chkSon','categeory_all')" />
                    		<label for="categeory2">Product Catalog</label>
                    	</li>
                    	<li class="third">
                        	<input type="checkbox" value="" id="categeory3" name="brochure" class="chkSon" onclick="ChkSonClick('chkSon','categeory_all')" />
                    		<label for="categeory3">Brochure</label>
                    	</li>  
                    	<li class="first">
                        	<input type="checkbox" value="" id="categeory4" name="software" class="chkSon" onclick="ChkSonClick('chkSon','categeory_all')" />
                    		<label for="categeory4">Software</label>
                    	</li>
                    	<li class="second">
                        	<input type="checkbox" value="" id="categeory5" name="newsletter" class="chkSon" onclick="ChkSonClick('chkSon','categeory_all')" />
                    		<label for="categeory5">Newsletter</label>
                    	</li> 
                    	<li class="third">
                        	<input type="checkbox" value="" id="categeory6" name="video" class="chkSon" onclick="ChkSonClick('chkSon','categeory_all')" />
                    		<label for="categeory6">Video</label>
                    	</li> 
                    </ul>
                </div>
            </div>
            <div class="advance_name">File types</div>
            <div class="advance_type clearfix">
            	<div class="advance_type_left fl">
                    <input type="checkbox" value="" id="file_all" onclick="ChkAllClick('chkSon1','file_all')" />
                    <label for="file_all">All</label>
                </div>
                <div class="advance_type_right fr">
                	<ul class="clearfix">
                    	<li class="first">
                        	<input type="checkbox" value="" id="file1" name="pdf" class="chkSon1" onclick="ChkSonClick('chkSon1','file_all')" />
                    		<label for="file1">PDF</label>
                    	</li>
                    	<li class="second">
                        	<input type="checkbox" value="" id="file2" name="word" class="chkSon1" onclick="ChkSonClick('chkSon1','file_all')" />
                    		<label for="file2">Word</label>
                    	</li>
                    	<li class="third">
                        	<input type="checkbox" value="" id="file3" name="powerpoint" class="chkSon1" onclick="ChkSonClick('chkSon1','file_all')" />
                    		<label for="file3">PowerPoint</label>
                    	</li>  
                    	<li class="first">
                        	<input type="checkbox" value="" id="file4" name="excel" class="chkSon1" onclick="ChkSonClick('chkSon1','file_all')" />
                    		<label for="file4">Excel</label>
                    	</li>
                    	<li class="second">
                        	<input type="checkbox" value="" id="file5" name="zip" class="chkSon1" onclick="ChkSonClick('chkSon1','file_all')" />
                    		<label for="file5">ZIP</label>
                    	</li> 
                    	<li class="third">
                        	<input type="checkbox" value="" id="file6" name="file_video" class="chkSon1" onclick="ChkSonClick('chkSon1','file_all')" />
                    		<label for="file6">Video</label>
                    	</li> 
                    </ul>
                </div>
            </div>
            <div class="clearfix">
				<div class="advance_time fl">
            		<div class="advance_name">From</div>
                    <div class="advance_date">
                        <div class="selectbox">
                            <i class="date_arrow"></i>
                            <p id="year">Year</p>
                            <ul class="options year"></ul>
                        </div>
                        <div class="advance_line fl"></div>
                        <div class="selectbox selectbox_month">
                        	<i class="date_arrow"></i>
                        	<p id="month">Month</p>
                        	<ul class="options month"></ul>
                        </div>
                        <div class="advance_line advance_month_line fl"></div>
                        <div class="selectbox selectbox_month">
                        	<i class="date_arrow"></i>
                        	<p id="day">Day</p>
                        	<ul class="options day"></ul>
                        </div>
                        <input type="hidden" name="year"  value="" />
                        <input type="hidden" name="month" value=""  />
                        <input type="hidden" name="day"  value="" />
					</div>
                </div>
				<div class="advance_time fr">
                	<div class="advance_name">To</div>
					<div class="advance_date">
                        <div class="selectbox">
                            <i class="date_arrow"></i>
                            <p id="to_year">Year</p>
                            <ul class="options to_year"></ul>
                        </div>
                        <div class="advance_line fl"></div>
                        <div class="selectbox selectbox_month">
                        	<i class="date_arrow"></i>
                        	<p id="to_month">Month</p>
                        	<ul class="options to_month"></ul>
                        </div>
                        <div class="advance_line advance_month_line fl"></div>
                        <div class="selectbox selectbox_month">
                        	<i class="date_arrow"></i>
                        	<p id="to_day">Day</p>
                        	<ul class="options to_day"></ul>
                        </div>
                        <input type="hidden" name="to_year"  value="" />
                        <input type="hidden" name="to_month" value=""  />
                        <input type="hidden" name="to_day"  value="" />
					</div>
                </div>
                
            </div>
            <input type="submit" value="Search" class="advanceBtn" />
            </form>
    	</div>
    </div>    
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/date.js"></script>

</body>
</html>