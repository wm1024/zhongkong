<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(CP_BASE_TEMP."template/include/title.php")?>
<link href="<?php echo CP_BASE_STATIC;?>public/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo CP_BASE_STATIC;?>public/css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include(CP_BASE_TEMP."template/include/header.php")?>
    <?php include(CP_BASE_TEMP."template/include/top_link.php")?>
	<div class="sub_cont careers_cont">
		<div class="careers_top">
            <div class="container">
                <div class="careers_tit">Who We Are</div>
                <div class="careers_info">SUPCON is a leading supplier of products and solutions in China<br />process automation industry. </div>
                <div class="careers_list">
                    <ul class="clearfix">
                        <li style="width:230px;margin-right:70px;"><i class="careers_break1"></i>
                            <div class="careers_line"></div>
                            <div class="careers_list_tit">Vision</div>
                            <div class="careers_list_txt">A world's leading smart enterprise</div>
                        </li>
                        <li style="width:294px;margin-right:80px;"><i class="careers_break2"></i>
                            <div class="careers_line"></div>
                            <div class="careers_list_tit">Concept</div>
                            <div class="careers_list_txt">People-oriented, Innovative, <br />Self-inspired, Win-win motivated</div>
                        </li>
                        <li style="width:270px;"><i class="careers_break3"></i>
                            <div class="careers_line"></div>
                            <div class="careers_list_tit">Spirit</div>
                            <div class="careers_list_txt">Always pioneering, Pursuing excellence,<br />Pragmatic integrity, Responsiblediligence</div>
                        </li>	
                    </ul>
                </div>
            </div>	
        </div>
        <div class="careers_get" style="background:url(img/careers_get_bg.jpg) no-repeat center top;">
        	<div class="container clearfix">
        		<div class="we_get_left fl">
                	<h1>What You Get</h1>
                	<i class="we_get_icon"></i>
                </div>	
        		<div class="we_get_right fr">
                	<div class="we_get_txt">At SUPCON, our employees are our most valued assets. We recognize the importance of the balance between work content and personal interests and needs as an employer. It is highly likely you will enjoy the environment. SUPCON offers employees place where you can get things done and have fun to work.
                    </div>
                    <h2>We further support our teams and their individual growth by</h2>
                    <ul class="weget_ul">
                    	<li>Recognition and reward</li>
                        <li>Training and development</li>
                    </ul>
                </div>
        	</div>
        </div>
        <div class="careers_look">
        	<div class="container">
            	<div class="clearfix">
                    <div class="lookfor_left fl">
                        <h1>What We Look For</h1>
                        <div class="lookfro_left_txt">
                            <p>At SUPCON,<br />we hire talents with different professionals.</p>
                            <p>It doesn't matter who you are or where you live, no matter your religion or ethnic background, SUPCON employs diverse workforce from all corners of the globe.</p>
                        </div>
                    </div>
                    <div class="lookfor_right clearfix fr">
                        <div class="lookfor_value">We hope you can<br />share the following<br />values</div>
                    	<div class="lookfor_value_list">
                        	<ul>
                        		<li style="margin-bottom:5px;">Creative, energetic<br />and bright</li>
                        		<li style="padding-left:29px;margin-bottom:13px;">Integrity and honesty</li>
                                <li style="padding-left:60px;margin-bottom:6px;">Teamwork spirit</li>
                                <li style="padding-left:34px;">Mentoring programs</li>
                                <li style="padding-left:4px;">Committed to personal<br />excellence and<br />self-improvement</li>	
                        	</ul>
                        </div>
                    </div>
                </div>
                <div class="move_link lookfor_bot">
                	<a href="job.php" class="lookfor_job">Job opportunities<i class="move_link_icon"></i></a>
                    <a href="parner.php" class="lookfor_ship">Internships<i class="move_link_icon"></i></a>
                </div>
        	</div>
        </div>
    </div>
	<?php include(CP_BASE_TEMP."template/include/footer.php")?>
</div>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/common.js"></script>

</body>
</html>