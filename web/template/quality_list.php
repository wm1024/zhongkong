<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(CP_BASE_TEMP."template/include/title.php")?>
<link href="<?php echo CP_BASE_STATIC;?>public/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo CP_BASE_STATIC;?>public/css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<div class="honor_pop">
		<div class="honor_layer"></div>
        <div class="honor_main">
        	<div class="honor_pop_close"></div>
            <div class="honor_pop_pic"><div class="img"><img src="<?php echo CP_BASE_STATIC;?>public/img/honor_pic2.jpg" /></div></div>
            <div class="honr_pop_txt">Achilles Level II Certificate for ECS-700 FCU712-S01</div>	
        </div>
	</div>
	<?php include(CP_BASE_TEMP."template/include/header.php")?>
  	<?php include(CP_BASE_TEMP."template/include/top_link.php") ?>
	<div class="sub_cont">
    	<div class="container">
 			<div class="milestone_title quality_title"><?php echo get_classname($_GET['cid']);?></div>
            <div class="honor_list">
            	<ul class="clearfix">
                	<?php foreach($explainArr as $key => $value) { ?>
            		<li><a href="javascript:;" onclick="openHonor(this);">
                    	<div class="honor_pic" rel="/<?php echo get_imgurl($value['pic'])?>">
                        	<div class="img">
                        		<img src="/<?php echo get_imgurl($value['pic'])?>" />
                        	</div>
                        </div>
                    	<div class="honor_txt"><?php echo $value['title'];?></div>
            		</a></li>
                    <?php } ?>
            	</ul>
                <a href="javascript:;" class="honor_more" _cid="cid" _p="1"></a>
				<div class="page">
					<div class="page_cont">
						<?php
						if($cid){
							$add = 'cid-'.$cid;
						}
						if($pageSum>=1){
							echo get_page_str($page,$pageSum,'/quality/qlist-'.$add,$pagesPer=3);
						}
						?>
					</div>
				</div>
            </div>
    	</div>
    </div>    
	<?php include(CP_BASE_TEMP."template/include/footer.php")?>
</div>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/common.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/ajax.js"></script>
<script type="text/javascript">
$(function(){
	
	var cid=$(".honor_more").attr("_cid");
	var page=$(".honor_more").attr("_p");
	
	$(window).scroll(function(){
		
		honor_show(cid,page);
	});	

});
</script>
</body>
</html>