<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(CP_BASE_TEMP."template/include/title.php")?>
<link href="<?php echo CP_BASE_STATIC;?>public/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo CP_BASE_STATIC;?>public/css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include(CP_BASE_TEMP."template/include/login_pop.php")?>
	<?php include(CP_BASE_TEMP."template/include/header.php")?>
  	<?php include(CP_BASE_TEMP."template/include/top_link.php")?>
	<div class="sub_cont">
    	<div class="container">
        	<div class="clearfix">
				<?php if(!empty($member)){
					echo '<a class="resource_login fr" href="/member/center.html" onclick="openLogin();">
							<i class="login"></i>'.$member['name'].'</a>';
				}else{
					echo '<a class="resource_login fr" href="javascript:;" onclick="openLogin();">
							<i class="login"></i>Log in</a>';
				}?>
            </div>
 			<div class="milestone_title resource_title"><?php echo $_GET['cid']?get_classname($_GET['cid']):'ALL';?></div>

			<form action="/resource/rlist-<?php echo $cid?'cid-'.intval($cid).'-':'';?>act-search.html" method="post">
			<div class="sub_top_search resource_top_search clearfix">
    			<input type="text" name="keywords" value="" placeholder="Search Now" _reg="Search Now" class="keyword_text fl" />
                <div class="select fl">
                	<p>ALL</p>
                    <i class="select_arrow"></i>
                	<ul>
                    	<li>ALL</li>
						<?php
						$types = getClassList(12345);
						foreach($types as $k => $v){
							echo '<li>'.get_classname($v['ID']).'</li>';
						}
						?>
                    </ul>

                    <input type="hidden" name="resource_type" value="ALL" id="resource_type" />

                </div>
    			<input type="submit" value="Search" class="subBtn fl" />
                <p class="fl">or</p>
                <a class="fr" href="/resource/search.html">Advanced Search</a>
    		</div>
			</form>

            <div class="resource_sub_list">
            	<ul class="clearfix">
					<?php
					if(!empty($explainArr)){foreach($explainArr as $key => $value){
					?>
            		<li class="clearfix">
            			<div class="resource_left fl"><a href="<?php if($cid==1178){echo '/resource/video.html';}?>"><img src="/<?php echo get_imgurl($value['pic']);?>" width="156" height="205" />
                        	<div class="type_icon"><?php echo $value['filetype'];?></div>
                        	<i class="<?php if($value['lock']==1){echo 'lock_icon';}?>"></i>
                        </a></div>
            			<div class="resoucre_right fr">
                        	<div class="resource_sub_name"><a href="<?php if($cid==1178){echo '/resource/video.html';}?>"><?php echo $value['title'];?></a></div>
                        	<div class="resource_size">File size : <?php echo $value['size'];?></div>
                        	<div class="update_time">Update Time : <?php echo get_eng_date($value['datetime']);?></div>
                        	<div class="resource_download clearfix">
                            	<a class="download fl" href="<?php
								if(!empty($member)){
									echo '/resource/download-fileid-'.$value['id'].'.html';
								}else{
									if($value['lock']==1){
										echo 'javascript:;';
									}else{
                                        if($value['classid']==1178){
                                            echo '/resource/video.html';
                                        }else{
                                            echo '/resource/download-fileid-'.$value['id'].'.html';
                                        }										
									}
								}
								?>"><?php
									if($value['classid']==1178){echo 'Watch Online';}else{echo 'Download';}?></a>
                                <div class="resourse_share fl">
                                    <div class="bdsharebuttonbox">
                                        <a title="分享到一键分享" href="javascript" class="bds_mshare" data-cmd="mshare">Share</a>
                                    </div> 
                                </div>
                            </div>
                        </div>
            		</li>
					<?php }}?>
            	</ul>
				<div class="page">
					<div class="page_cont">
						<?php
						if($cid){
							$add = 'cid-'.$cid;
						}
						if($pageSum>=1){
							echo get_page_str($page,$pageSum,'/resource/rlist-'.$add,$pagesPer=3);
						}
						?>
					</div>
				</div>
            </div>
    	</div>
    </div>    
	<?php include(CP_BASE_TEMP."template/include/footer.php")?>
</div>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/common.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/form.js"></script>

</body>
</html>