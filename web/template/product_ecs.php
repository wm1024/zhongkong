<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(CP_BASE_TEMP."template/include/title.php")?>
<link href="<?php echo CP_BASE_STATIC;?>public/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo CP_BASE_STATIC;?>public/css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<div class="ind_video">
		<div class="ind_video_layer"></div>
        <div class="ind_video_main"><div id="a1"></div></div>
	</div>
	<?php include(CP_BASE_TEMP."template/include/header.php")?>
    <?php include(CP_BASE_TEMP."template/include/top_link.php")?>
  	<div class="ecs_top">
		<div class="container">
    		<div class="ecs_top_tit1">Increase your performance with</div>
            <div class="ecs_top_tit2">SUPCON Webfield<sup>®</sup> ECS-700</div>
    		<div class="ecs_top_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/esc_pic.jpg" width="490" height="510" /></div>
    	</div>
    </div>
    <div class="ecs_main">
    	<div class="container clearfix">
<!--    		<div class="ecs_main_left fl">-->
<!--    			<div class="ecs_main_tit">Main Features</div>-->
<!--    			<div class="ecs_main_info">-->
<!--                	ECS-700 brings your operators a friendly and efficient operating environment that keeps every operator clearly informed, and easy to control the process. Typically, the system can be called in project as DCS, FCS, PCS, IACS, PDCS, etc.-->
<!--                </div>-->
<!--    			<div class="ecs_main_nav">-->
<!--                	<ul class="clearfix">-->
<!--                    	<li class="on">Integration</li>-->
<!--                    	<li>Scalability</li>-->
<!--                        <li>Efficiency</li>-->
<!--                        <li>Reliability</li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--    			<div class="ecs_main_tabs">-->
<!--                	<div class="ecs_main_txt">-->
<!--                    	As the core digital automation platform of a modern plant, ECS-700 captures plant-wide data in real time via various standard interfaces, such as OPC, MODBUS, HART, FF and PROFIBUS, and delivers the right data to the right person on right time.-->
<!--                   	</div>-->
<!--                	<div class="ecs_main_txt">-->
<!--                    	By implementing Control Domain and Operation Domain strategy, ECS-700 supports flexible expansion of your plant up to 65,000 I/Os for each control domain and supports up to maximum 60 control domains. By adopting ECS-700, you can expand your plant scale as you wish.-->
<!--                    </div>-->
<!--                	<div class="ecs_main_txt">-->
<!--                    	ECS-700 is a reliable and efficient automation platform for process control, providing your engineer an easy integration solution of DCS, SIS, RTU, SCADA, MMS, LDS, TGS, etc.-->
<!--                    </div>-->
<!--                    <div class="ecs_main_txt">-->
<!--                    	<p>ECS-700 is a highly reliable control system. The stable operation throughout the whole process is completely free from the failure of any single component. Designed in</p>-->
<!--                        <p>compliance with European Community EMC Directive II, and special anti-corrosion coating in compliance with <a href="##">ISA71.04 standard G3.</a> Also, the system passed <a href="##">CE EMC</a> and <a href="##">CE LVD</a> certificates, as well as the highest level (Level II) test of <a href="##">Achilles</a>, one of the most critical certificates for cyber security. <a href="quality_list.php">See more</a></p>-->
<!--                   	</div>-->
<!--    			</div>-->
<!--    		</div>-->
<!--            <div class="ecs_main_right fr"><img src="--><?php //echo CP_BASE_STATIC;?><!--public/img/esc_main_pic.png" width="287" height="727" /></div>-->
            <?php echo $detail['content1'];?>
    	</div>
    </div>
    <div class="ecs_view">
    	<div class="container">
        	<div class="ecs_view_tit"><?php echo $detail['para2'];?></div>
<!--        	<div class="ecs_view_info">-->
<!--            	ECS-700 has an architecture consisting of HMI, field control stations, and a control network. <br />-->
<!--				These three basic components provide flexible scalability from small scale to large and complex facilities.-->
<!--			</div>-->
<!--            <div class="ecs_view_pic" id="demo1">-->
<!--            	<img src="--><?php //echo CP_BASE_STATIC;?><!--public/img/esc_view_pic.jpg" width="944" height="485" />-->
<!--            	<div class="ect_view_yin"></div>-->
<!--                <div class="range"></div>-->
<!--                <!--<div class="big"><img src="img/esc_view_pic.jpg" alt=""></div>-->-->
<!--            </div>-->
            <?php echo $detail['content2'];?>
        </div>
    </div>
    <div class="ecs_intell">
    	<div class="container">
        	<div class="ecs_intell_tit"><?php echo str_replace('/','<br/>',$detail['para3']);?></div>
<!--        	<div class="ecs_intell_info">-->
<!--            	<p>Industry is considered a driver for innovation, growth, and social stability. At the same time,<br /> -->
<!--				however, competition is growing more intense. Only those who can make do with less energy and fewer <br />-->
<!--				resources will be able to cope with the growing cost pressure.</p>-->
<!--				<p>These challenges can be overcome. As a partner of customer, SUPCON developed a variety of automation<br />-->
<!--				solutions to assist your plant performance; these solutions cover the lifecycle of your plant, <br />-->
<!--				such as SAMS, FF, SCSS, integrated platform, OTS, MES, APC, Batch, etc. </p> -->
<!--            </div>-->
<!--			<div class="clearfix">-->
<!--                <div class="ecs_intell_nav fl">-->
<!--                    <ul>-->
<!--                        <li class="on"><i class="intell_nav_break1"></i>-->
<!--                            <div class="intell_nav_tit">SAMS</div>-->
<!--                            <div class="intell_nav_txt">SUPCON Asset Management System</div>	 -->
<!--                        </li>-->
<!--                        <li><i class="intell_nav_break2"></i>-->
<!--                            <div class="intell_nav_tit">FF</div>-->
<!--                            <div class="intell_nav_txt">FOUNDATION Fieldbus System</div>	-->
<!--                        </li>-->
<!--                        <li><i class="intell_nav_break3"></i>-->
<!--                            <div class="intell_nav_tit">SCSS</div>-->
<!--                            <div class="intell_nav_txt">SUPCON Cyber Security Solution</div>	-->
<!--                        </li>-->
<!--                        <li><i class="intell_nav_break4"></i>-->
<!--                            <div class="intell_nav_tit">FCS-SIS</div>	-->
<!--                            <div class="intell_nav_txt">Integrated Platform</div>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--                <div class="ecs_intell_tabs fr">-->
<!--                    <div class="ecs_intell_list">-->
<!--                    	<div class="ecs_intell_list_tit">SAMS - SUPCON Asset Management System</div>-->
<!--                    	<div class="ecs_intell_list_txt">	-->
<!--							SAMS (SUPCON Asset Management System) Software, as the software part of SUPCON intelligent device management solution, is to realize the management of HART, FOUNDATION Fieldbus and other intelligent instruments. SAMS has functions including device status monitoring, device configuration, fault diagnosis, device alarm and operation record, etc. -->
<!--                        </div>-->
<!--                        <div class="ecs_intell_list_tit">SAMS Makes Predictive Maintenance Possible</div>-->
<!--                        <div class="ecs_intell_list_txt">Alarms generated by SAMS will be reported to system; based on the information from SAMS maintenance, engineers can take predictive maintenance action to reduce unplanned shutdown.  In addition, engineers can sort all the alarms, and the typical prioritized alert list includes:</div>-->
<!--                       	<div class="ecs_intell_list_tit">SAMS Supports the Device of FDT/DTM</div>-->
<!--                        <div class="ecs_intell_list_txt">Users can use the device DTM to easily modify parameter and execute advanced diagnosis. Device DTM provides more advanced diagnosis functions combined with graph and is more illustrative.-->
<!--                        <ul class="intell_ul clearfix" style="width:610px;">-->
<!--                        	<li style="width:355px;">Output failure</li>-->
<!--                            <li style="widows:225px;">Local override</li>-->
<!--                        	<li style="width:355px;">Loop in manual</li>-->
<!--                            <li style="widows:225px;">Device out of service reports</li>-->
<!--                            <li style="width:355px;">Input failure/process variable has bad status</li>-->
<!--                            <li style="widows:225px;">Excessive loop variability</li>-->
<!--                        </ul>-->
<!--                        </div>-->
<!---->
<!--                       	<div class="ecs_intell_list_tit">SAMS Supports PST (Partial Stroke Test)</div>-->
<!--                        <div class="ecs_intell_list_txt">SAMS Software develops PST diagnosis function which is applicable to various kinds of instruments and valves through the use of Equipment DTM. PST enables users to extend the time interval between proof tests of safety valves and reduce plant maintenance cost.</div> -->
<!--                    </div>	-->
<!--                    <div class="ecs_intell_list">-->
<!--                    	<div class="ecs_intell_list_tit">FOUNDATION Fieldbus System</div>-->
<!--                        <div class="ecs_intell_list_txt">SUPCON FOUNDATION Fieldbus System is based on WebField™ ECS-700, which conforms to the 61b standards of FF host system and can meet the application requirements of Fieldbus project. </div>-->
<!--                        <div class="ecs_intell_list_txt">With SUPCON WebField™ ECS-700 Fieldbus System, you will benefit from system design, installation, debugging, startup, operation and maintenance. FCS simplified infrastructure saves cost of investment, its high distribution and self-control with field devices will improve the system control performance. -->
<!--                        </div>-->
<!--                        <div class="ecs_intell_list_tit">Openness and Interoperability</div>-->
<!--                        <div class="ecs_intell_list_txt">The system is based on the open system interconnection model of ISO/OSI, take the physical layer, data link layer and the application layer as the corresponding layers for H1 communication model, and add the user layer on the application layer that conforms to the international standard of IEC 61158-2 Fieldbus protocol. Supporting all devices which are tested and certificated by Fieldbus Foundation, there are more than 350 manufacturers who can provide Fieldbus devices.-->
<!--                        	<img src="--><?php //echo CP_BASE_STATIC;?><!--public/img/intell_pic.jpg" />-->
<!--                        </div>-->
<!--                        -->
<!--                    </div> -->
<!--                    <div class="ecs_intell_list">-->
<!--                    	<div class="ecs_intell_list_tit">SCSS - SUPCON Cyber Security Solution</div>-->
<!--                        <div class="ecs_intell_list_txt">Nowadays, security threats aimed at control systems are increasing by malwares (i.e. worms, viruses, Trojan horse, etc.) and appearance of Advanced Persistent Threats (APT) (i.e. targeted attacks). To easy your mind, SUPCON developed SUPCON Cyber Security Solution (SCSS) to protect your IACS from these threats. It is a management system based on the risk assessment.-->
<!--                        </div>-->
<!--                        <div class="ecs_intell_list_tit">Typical Procedures to Reach SCSS</div>-->
<!--                        <div class="ecs_intell_list_txt">-->
<!--                            <ul class="intell_ul clearfix" style="width:610px;">-->
<!--                                <li style="width:320px;">Organization of the security committee</li>-->
<!--                                <li >Identification of the assets</li>-->
<!--                                <li >Continuous monitoring and revision</li>-->
<!--                                <li >Identification and evaluation of the threats</li>-->
<!--                                <li >Identification and evaluation of the vulnerability</li>-->
<!--                                <li >Design and implementation of the security measures</li>-->
<!--                                <li >Examination and enforcement of system change management</li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                    </div>   -->
<!--                    <div class="ecs_intell_list">-->
<!--                    	<div class="ecs_intell_list_tit">FCS-SIS Integrated Platform</div>-->
<!--                        <div class="ecs_intell_list_txt">The unique design of ECS-700 FCS simplified the integration of plant automation control system, and shorten the installation and commissioning process; in addition, the seamless integration of SUPCON ECS-700 (FCS) and TCS-900 (SIS) will bring you more benefits.</div>-->
<!--                    	<div class="ecs_intell_list_tit">Integrated Platform Based on ECS-700 and TCS-900</div>-->
<!--                        <div class="ecs_intell_list_txt">The TCS-900 Safety Instrumented System (SIS) is more than just a shutdown system. TCS-900 supports seamless integration with ECS-700 via Scnet IV, meanwhile TCS-900 control stations can talk together via SafeEthernet, it provides the backbone of secure, and safe plant operation.</div>-->
<!--                    	<div class="ecs_intell_list_tit">Functions:</div>-->
<!--                        <div class="ecs_intell_list_txt">-->
<!--                            <ul class="intell_ul clearfix" style="width:670px;">-->
<!--                                <li style="width:320px;">Enhanced cyber security design</li>-->
<!--                                <li >Integrated engineering tools</li>-->
<!--                                <li style="width:320px;">No single point of failure design</li>-->
<!--                                <li >Intuitive and integrated operation platform</li>-->
<!--                                <li style="width:320px;">Integrated asset management platform</li>-->
<!--                                <li>EEMUA guidelines oriented alarm management platform</li>-->
<!--                            </ul> -->
<!--                        </div>-->
<!--                    </div>            -->
<!--                </div>	-->
<!--        	</div>-->
            <?php echo $detail['content3'];?>
        </div>
    </div>
	<div class="indust_video ecs_video">
    	<div class="indust_video_title">Video</div>    
		<div class="indust_video_main">
			<div class="indust_video_pic">
                <img src="<?php echo '/'.get_imgurl($detail['videoImg']);?>" width="834" height="467" />
                <div class="indust_video_pic_yin"><a href="javascript:;" onclick="openVideo()"></a></div>
			</div>
		</div>
	</div>
	<div class="indust_more" style="background:url(img/indust_more_bg.jpg) no-repeat center 200px;">
    	<div class="indust_more_cont">
			<?php include(CP_BASE_TEMP."template/include/explore_more.php")?>
        </div>
    </div>
	<?php include(CP_BASE_TEMP."template/include/footer.php")?>
</div>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/common.js"></script>
<script src="<?php echo CP_BASE_STATIC;?>public/ckplayer/ckplayer.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/magnifier.js"></script>
<script type="text/javascript">
    $('#demo1').banqh({
        box:"#demo1",//总框架
        pic:"#ban_pic1",//大图框架
        pnum:"#ban_num1",//小图框架
        pop_div:"#demo2",//弹出框框架
        pop_pic:"#ban_pic2",//弹出框图片框架
        pop_xx:".pop_up_xx",//关闭弹出框按钮
        mhc:".mhc",//朦灰层
        autoplay:false,//是否自动播放
        interTime:5000,//图片自动切换间隔
        delayTime:400,//切换一张图片时间
        pop_delayTime:400,//弹出框切换一张图片时间
        order:0,//当前显示的图片（从0开始）
        picdire:true,//大图滚动方向（true为水平方向滚动）
        mindire:true,//小图滚动方向（true为水平方向滚动 false为垂直滚动）
        min_picnum:5,//小图显示数量
        pop_up:false//大图是否有弹出框
    });
    $('.small').mag({
        box:".small li",//小图框架
        box_w:450,//小图宽
        box_h:450,//小图高
        box2:".big",//大图框架
        box2_w:1500,//大图宽
        box2_h:1500,//大图高
        range:".range",//放大镜框架
        range_w:100,//小图放大镜范围宽度
        range_h:100//小图放大镜范围高度
    });
</script>
<script type="text/javascript">
	var flashvars={
		p:0,
		e:1,
		ht:'20',
		hr:'http://www.ckplayer.com'
		};
	var video=['<?php echo $detail['video'];?>'];
	var support=['all'];
	CKobject.embedHTML5('a1','ckplayer_a1',600,400,video,flashvars,support);	
</script>
</body>
</html>