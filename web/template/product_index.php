<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(CP_BASE_TEMP."template/include/title.php")?>
<link href="<?php echo CP_BASE_STATIC;?>public/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo CP_BASE_STATIC;?>public/css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include(CP_BASE_TEMP."template/include/header.php")?>
  	<?php include(CP_BASE_TEMP."template/include/top_link.php")?>
	<div class="sub_cont">
      <div class="proind_cont">
            <h1>SPlant Total Solution <br />for Process Automation</h1>
            <div class="product_map">
                <img src="<?php echo CP_BASE_STATIC;?>public/img/product_index.png" usemap="#Map2" style="z-index:5;" border="0">
                <map name="Map2" id="Map2">
                  <area shape="poly" coords="728,6,727,86,672,107,674,142,681,153,704,151,734,184,734,201,740,214,774,204,775,176,814,190,824,189,823,94,732,6" href="#" />
                </map>
                <img border="0" style="z-index:10;" src="<?php echo CP_BASE_STATIC;?>public/img/product_layer.png" usemap="#Map" class="map_area">
                <map id="Map" name="Map">
                
                </map>
            </div>
        </div>
	</div>
	<?php include(CP_BASE_TEMP."template/include/footer.php")?>
</div>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/common.js"></script>

</body>
</html>