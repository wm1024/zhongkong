<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(CP_BASE_TEMP."template/include/title.php")?>
<link href="<?php echo CP_BASE_STATIC;?>public/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo CP_BASE_STATIC;?>public/css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include(CP_BASE_TEMP."template/include/header.php")?>
  	<?php include(CP_BASE_TEMP."template/include/top_link.php")?>

  	<?php
  	// 头条新闻
  	if (!empty($news_top_arr)) {
  		echo '<div class="news_banner" style="background:url(\'/'.get_imgurl($news_top_arr[0]['pic1']).'\') center top no-repeat;">
			<a href="/news/detail-id-'.$news_top_arr[0]['id'].'.html">
			<div class="title">'.str_len($news_top_arr[0]['title'],81).'</div>
			<div class="date">Release time：'.date('Y-m-d', strtotime($news_top_arr[0]['datetime'])).'</div>
			<div class="con">'.str_len(strip_tags($news_top_arr[0]['content']),180).'</div>
			</a></div>';
  	}
  	?>

	<div class="container">
		<div class="news1">
			<div class="news_tit">News Highlights<a href="/news/nlist-cid-1136.html" class="fr">More<i></i></a></div>
			<ul class="news_list clearfix">
				<?php
				if (!empty($news_1136_arr)) {
					foreach ($news_1136_arr as $k => $v) {
						echo '<li><a href="/news/detail-id-'.$v['id'].'.html"><div class="ind_news_pic"><img src="/'.get_imgurl($v['pic']).'" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
							<div class="ind_news_name"><a href="/news/detail-id-'.$v['id'].'.html" >'.str_len($v['title'],36).'</a></div>
							<div class="ind_news_time">'.date('Y-m-d', strtotime($v['datetime'])).'</div>
							<div class="ind_news_info"><a href="/news/detail-id-'.$v['id'].'.html">'.str_len(strip_tags($v['content']),81).'</a></div>
							</li>';
					}
				}
				?>

<!-- <li><a href="#"><div class="ind_news_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/news_pic1.jpg" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
<div class="ind_news_name"><a href="#" title="SUPCON Awarded 2 Honors for Internet+ Industry">SUPCON Awarded 2 Honors for Internet+ Industry</a></div>
<div class="ind_news_time">December 14, 2015</div>
<div class="ind_news_info"><a href="#">Nov 12, SUPCON welcomed a 20-people delegation led by Zhang …</a></div>
</li>

				<li><a href="#"><div class="ind_news_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/news_pic2.jpg" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
					<div class="ind_news_name"><a href="#" title="Vice Chairman of Zhejiang Association for Science and …">Vice Chairman of Zhejiang Association for Science and …</a></div>
					<div class="ind_news_time">December 14, 2015</div>
					<div class="ind_news_info"><a href="#">Nov 4, SUPCON welcomed the visit of Zhu Guangyu, Director of …</a></div>
				</li>	    
				<li><a href="#"><div class="ind_news_pic"><img src="<?php echo CP_BASE_STATIC;?>public/img/news_pic3.jpg" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
					<div class="ind_news_name"><a href="#" title="Vice Chairman of Zhejiang Association for Science and …">Vice Chairman of Zhejiang Association for Science and …</a></div>
					<div class="ind_news_time">December 14, 2015</div>
					<div class="ind_news_info"><a href="#">Nov 11, SUPCON welcomed Zhang Hongjian, Vice President of …</a></div>
				</li>	 --> 
			</ul>
		</div>
		
		<div class="news2">
			<div class="news_tit">News<a href="/news/nlist-cid-1135.html" class="fr">More<i></i></a></div>
			<a href="/news/detail-id-<?php echo $news_1135_arr[0]['id']?>.html" class="news2_hot">
				<img src="/<?php echo get_imgurl($news_1135_arr[0]['pic']);?>" class="fl"/>
				<div class="main fl">
					<div class="title"><?php echo str_len($news_1135_arr[0]['title'],36);?></div>
					<div class="date">Release time：<?php echo get_eng_date($news_1135_arr[0]['datetime']);?></div>
					<div class="con"><?php echo str_len(strip_tags($news_1135_arr[0]['content']),81)?></div>
					<div class="more fr">More<i></i></div>
				</div>
			</a>
			<ul class="news_list clearfix">
				<?php foreach($news_1135_arr as $k => $v){
					if($k>0){?>
						<li><a href="/news/detail-id-<?php echo $v['id']?>.html"><div class="ind_news_pic"><img src="/<?php echo $v['pic']?get_imgurl($v['pic']):'public/images/default_news_img.jpg';?>" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
							<div class="ind_news_name"><a href="/news/detail-id-<?php echo $v['id']?>.html" title="<?php echo $v['title'];?>"><?php echo substr($v['title'],0,36);?></a></div>
							<div class="ind_news_time"><?php echo get_eng_date($v['datetime']);?></div>
							<div class="ind_news_info"><a href="/news/detail-id-<?php echo $v['id']?>.html"><?php echo str_len($v['content'],81);?></a></div>
						</li>
				<?php }}?>
			</ul>
		</div>
	</div>
	<div class="news3">
		<div class="container"><div class="news_tit">Newsletter<a href="/resource/rlist-cid-1177.html" class="fr">More<i></i></a></div></div>
		<div class="main">
			<a href="#"><?php echo get_eng_month($journal_time[0]['month']);?> <?php echo $journal_time[0]['year'];?></a>
			<div class="news_subscribe"><input type="text" class="text fl" placeholder="Enter your e-mail" id="email"/><input type="submit" class="btn fl" value="Subscribe" onclick="return subscribeForm()"/></div>
		</div>
	</div>
	<?php include(CP_BASE_TEMP."template/include/footer.php")?>
</div>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/common.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/form.js"></script>

</body>
</html>