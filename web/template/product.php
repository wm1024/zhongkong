<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(CP_BASE_TEMP."template/include/title.php")?>
<link href="<?php echo CP_BASE_STATIC;?>public/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo CP_BASE_STATIC;?>public/css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include(CP_BASE_TEMP."template/include/header.php")?>
  	<?php include(CP_BASE_TEMP."template/include/top_link.php")?>
	<div class="pro_nav">
		<div class="container">
			<a href="#" class="on">ALL</a>
			<a href="#">Recorder</a>
			<a href="#">Transmitter</a>
			<a href="#">Calibrator</a>
			<a href="#">Controller</a>
			<a href="#">Flowmeter</a>
			<a href="#">Isolator</a>
			<a href="#">Control Valves</a>
		</div>
	</div>
	
	<div class="pro_list">
		<div class="container">
			<ul class="explore_pro clearfix">
				<?php for($i=0;$i<8;$i++){?>
				<li><a href="product_det.php">
					<div class="imgBox">
						<div class="img"><img src="<?php echo CP_BASE_STATIC;?>public/img/ind_product_pic.png" /></div>
					</div>
					<div class="main">
						<div class="title ellipsis" title="Webfield ECS-700">Webfield ECS-700</div>
						<div class="type">PCS — DCS / FCS</div>
						<div class="con">
							<p><strong>High accuracy :</strong> up to 0.04%</p>
							<p><strong>Stability :</strong> ±0.1%/10 years</p>
							<p>CE Certificate</p>
						</div>
					</div>
				</a></li>
				<?php }?>
			</ul>
		</div>
	</div>
	<?php include(CP_BASE_TEMP."template/include/footer.php")?>
</div>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/common.js"></script>

</body>
</html>