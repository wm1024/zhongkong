<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include("include/title.php")?>
<link href="css/base.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include("include/header.php")?>
  	<?php include("include/top_link.php")?>
	<div class="sub_cont">
    	<div class="container">
 			<div class="milestone_title contact_title">Zhejiang SUPCON Technology Co., Ltd.</div>
            <div class="contact_list">
            	<ul class="clearfix">
					<li><div class="contact_add"></div>
                    	<div class="contact_line"></div>
                        <div class="contact_name">Address</div>
                        <div class="contact_info">
                        	<p>No.309, Liuhe Road,<br />Binjiang District, Hangzhou,<br />Zhejiang,China, 310053</p>
                        </div>
                    </li>
					<li><div class="contact_tel"></div>
                    	<div class="contact_line"></div>
                        <div class="contact_name">Tel</div>
                        <div class="contact_info">
                        	<p>0086-571-86667361</p>
                        </div>
                    </li>
					<li><div class="contact_fax"></div>
                    	<div class="contact_line"></div>
                        <div class="contact_name">Fax</div>
                        <div class="contact_info">
                        	<p>0086-571-86667318</p>
                        </div>
                    </li>
					<li><div class="contact_email"></div>
                    	<div class="contact_line"></div>
                        <div class="contact_name">E-mail</div>
                        <div class="contact_info">
                        	<p>overseas@supcon.com</p>
                        </div>
                    </li> 
            	</ul>
            </div>
    	</div>
    </div>    
	<?php include("include/footer.php")?>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</body>
</html>