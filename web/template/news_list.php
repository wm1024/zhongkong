<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(CP_BASE_TEMP."template/include/title.php")?>
<link href="<?php echo CP_BASE_STATIC;?>public/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo CP_BASE_STATIC;?>public/css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="mainbody">
	<?php include(CP_BASE_TEMP."template/include/header.php")?>
  	<?php include(CP_BASE_TEMP."template/include/top_link.php")?>
	<div class="sub_cont">
		<div class="container">
			<div class="newsList_title"><?php if($cid==1136){echo 'News Highlights';}elseif($cid==1135){echo 'News';}?></div>
			<div class="newsList_search">
				<form action="/news/nlist-cid-<?php echo $cid;?>-act-search.html" method="post">
					<input type="text" name="keywords" value="<?php echo $keywords;?>" placeholder="Keyword" class="text fl"/><input type="submit" value="Search" class="btn fr"/>
				</form>
			</div>
			<ul class="newsList">
				<?php if(!empty($explainArr)){foreach($explainArr as $key => $value){?>
				<li><a href="/news/detail-id-<?php echo $value['id']?>.php" class="clearfix">
					<div class="date fl">
						<div class="day"><?php echo substr($value['datetime'],8,2);?></div>
						<div class="mon"><?php echo substr(get_eng_month(substr($value['datetime'],5,2)),0,3);?></div>
					</div>
					<div class="main fl">
						<div class="title"><?php echo $value['title'];?></div>
						<div class="con"><?php echo str_len($value['content'],249);?></div>
					</div>
				</a></li>
				<?php }}?>
			</ul>
			<div class="page">
				<div class="page_cont">
					<?php
					if($cid){
						$add = 'cid-'.$cid;
					}
					if($pageSum>=1){
						echo get_page_str($page,$pageSum,'/news/nlist-'.$add,$pagesPer=3);
					}
					?>
				</div>
			</div>
		</div>
	</div>
	<?php include(CP_BASE_TEMP."template/include/footer.php")?>
</div>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/common.js"></script>

</body>
</html>