<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(CP_BASE_TEMP."template/include/title.php")?>
<link href="<?php echo CP_BASE_STATIC;?>public/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo CP_BASE_STATIC;?>public/css/swiper_min.css" rel="stylesheet" type="text/css">
<link href="<?php echo CP_BASE_STATIC;?>public/css/common.css" rel="stylesheet" type="text/css">

</head>

<body>

<div class="mainbody">
	<div class="ind_video">
		<div class="ind_video_layer"></div>
        <div class="ind_video_main">
        	<div id="a1"></div>
        	<div class="ind_video_close"><i class="close_video_icon"></i></div>
        </div>
	</div>
	<div class="head_fixed">
    	<div class="container clearfix">
    		<div class="logo fl"><a href="/index/index.html"><img src="<?php echo CP_BASE_STATIC;?>public/images/logo.png" width="154" height="25" /></a></div>
            <div class="head_nav ind_position fr">
            	<ul class="clearfix">
                	<li><a href="#about">About SUPCON</a></li>
                    <li><a href="#news">News</a></li>
                    <li><a href="#product">Products</a></li>
                    <li><a href="#partner">Partner</a></li>
                    <li style="margin-right:0;">
                    	<div class="head_search">
                        	<input type="submit" value="" class="search_text" />
                      	</div>
                    	<div class="head_search_word">
                        	<input type="text" value="Search …" _reg="Search …" class="search_word" />
                            <i class="head_search_close"></i>
                        </div>
                    </li>
                    <li style="margin-right:0;"><div class="head_menu"><i class="head_menu_icon"></i></div></li>
                </ul>
            </div>
    	</div>
    </div>
    <div class="head_fixed_erify">
    	<ul>
        	<li class="close">Menu<i class="erify_close"></i></li> 
    		<li><div class="fixed_tabs"><i class="erify_right"></i></div>
            	<a class="fixed_nav" href="/about/index.html">About us</a>
            	<div class="fixed_sub_menu">
            		<a href="/about/index.html">Company overview</a>
            		<a href="/milestone/index.html">Milestone</a>
                    <a href="/quality/index.html">Quality assurance</a>
                    <a href="/career/index.html">Careers</a>
                    <a href="/contact/index.html">Contact us</a>
            	</div>
            </li>
            <li><div class="fixed_tabs"><i class="erify_right"></i></div>
            	<a class="fixed_nav" href="/product/index.html">Products</a>
            	<div class="fixed_sub_menu">
            		<a href="/product/index.html">SPlant Total Solution</a>
            		<a href="/product/plist-pid-1148.html">Instruments</a>
                    <a href="/product/plist-pid-1149.html">PCS</a>
                    <a href="/product/plist-pid-1150.html">APC & MES</a>
                    <a href="/product/plist-pid-1151.html">ERP</a>
            	</div>
            </li>
            <li><a class="fixed_nav" href="/industry/index.html">Industries</a></li>
            <li><a class="fixed_nav" href="/career/partner.html">Partner</a></li>
            <li><div class="fixed_tabs"><i class="erify_right"></i></div>
            	<a class="fixed_nav" href="/service/index.html">Service</a>
            	<div class="fixed_sub_menu">
            		<a href="/service/index.html">Life-cycle service</a>
            		<a href="/service/requirement.html">Submit your requirements</a>
            	</div>
            </li>
            <li><div class="fixed_tabs"><i class="erify_right"></i></div>
            	<a class="fixed_nav" href="/resource/index.html">Resources</a>
            	<div class="fixed_sub_menu">
            		<a href="/resource/rlist.html">All</a>
                    <a href="/resource/rlist-cid-1172.html">Brochure</a>
            		<a href="/resource/rlist-cid-1175.html">Product Catalog</a>
                    <a href="/resource/rlist-cid-1176.html">Typical Preference</a>
                    <a href="/resource/rlist-cid-1177.html">Newsletter</a>
                    <a href="/resource/rlist-cid-1178.html">Video</a>
            	</div>
            </li>
            <li><div class="fixed_tabs"><i class="erify_right"></i></div>
            	<a class="fixed_nav" href="/news/index.html">News</a>
            	<div class="fixed_sub_menu">
            		<a href="/news/nlist-cid-1136.html">Highlights</a>
            		<a href="/news/nlist-cid-1135.html">News</a>
                    <a href="/newsletter/index.html">Newsletter</a>
            	</div>
                <i class="erify_right"></i>
            </li>
    	</ul>
    </div>
    <!--<div class="banner">
        <div class="hd">
            <ul class="clearfix"></ul>
        </div>
        <div class="bd">
            <ul>
                <?php
                if(!empty($banner)){
                    foreach($banner as $key => $value) { ?>
                        <li><a href="#" style="background-image:url(<?php echo '/'.get_imgurl($value['pic']);?>)">
                            <div class="banner_txt">
                                <div class="banner_title"><?php echo $value['title'];?></div>
                                <div class="banner_subtit"><?php echo $value['title1'];?></div>
                                <div class="banner_info"><?php echo $value['title2'];?></div>
                            </div>
                        </a></li>
                <?php }} ?>
            </ul>
        </div>-->
	<div class="banner">
    	<div class="swiper-container">
            <div class="swiper-wrapper">
            	<?php
                if(!empty($banner)){
                    foreach($banner as $key => $value) { ?>
                <div class="swiper-slide"><a href="<?php echo $value['link'];?>" style="background-image:url(<?php echo '/'.get_imgurl($value['pic']);?>)">
					<div class="banner_txt">
                        <div class="banner_title"><?php echo $value['title'];?></div>
                        <div class="banner_subtit"><?php echo $value['title1'];?></div>
                        <div class="banner_info"><?php echo $value['title2'];?></div>
                    </div>
                </a></div>
                <?php }} ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"><a class="next"><i class="indust_next"></i></a></div>
            <div class="swiper-button-prev"><a class="prev"><i class="indust_prev"></i></a></div>
        </div>    
        <div class="ind_header">
        	<div class="container">
        		<div class="ind_top_logo"><a href="/index/index.html"><img src="<?php echo CP_BASE_STATIC;?>public/images/logo.png" width="154" height="25" /></a></div>
        		<div class="ind_top_lan">
                	<ul class="clearfix">
                    	<li><a href="http://www.supcon.com/" class="lan_ch">中文</a></li>
                        <li class="line"></li>
                        <li><a href="/index/index.html" class="lan_en">EN</a></li>
                    	<li class="add_adv"><a href="javascript:;" onclick="join_favorite(document.title,window.location)"><i class="add_adv_icon"></i>Add bookmark</a></li>
                    </ul>
                </div>
                <div class="clearfix">
                    <div class="ind_head_nav ind_position fl">
                        <ul class="clearfix">
                            <li><a href="#about">About SUPCON</a></li>
                            <li><a href="#news">News</a></li>
                            <li><a href="#product">Products</a></li>
                            <li><a href="#partner">Partner</a></li>
                    	</ul>
                    </div>
                    <div class="ind_head_right fr">
                    	<div class="clearfix">
                        	<div class="ind_head_search fl">
                                <input type="text" value="Search Now" _reg="Search Now" class="ind_head_word" />
                                <input type="submit" value="" class="ind_search" />
                            </div>
                            <div class="ind_head_menu fr"><i class="head_menu_icon"></i>Menu</div>
                        </div> 
                    </div>
                </div>
        	</div>
        </div>
        <div class="banner_video"><a href="javascript:;" onclick="openVideo();">Corporate Video</a></div>
    </div>
	<div class="ind_about" style="overflow:hidden;">
    	<a class="anchor" name="about"></a>
    	<div class="container">
        	<div class="ind_title"><a href="/about/index.html"><?php echo $overview[0]['title'];?></a></div>
            <div class="ind_about_txt"><a href="/about/index.html">
                <?php echo $overview[0]['content'];?>
            </a></div>	
        </div>
    </div>
    <div class="ind_news" style="overflow:hidden;">
    	<a class="anchor" name="news"></a>
    	<div class="container">
        	<div class="ind_title ind_news_title"><a href="/news/index.html">NEWS</a></div>
        	<ul class="news_list clearfix">
                <?php if(!empty($news_rmd2_arr)){
                    foreach($news_rmd2_arr as $key1 => $value1){
                ?>
            	<li><a href="/news/detail-id-<?php echo $value1['id'];?>.html"><div class="ind_news_pic"><img src="/<?php echo get_imgurl($value1['pic']);?>" width="298" height="140" /><div class="ind_news_yin"></div></div></a>
                	<div class="ind_news_name"><a href="/news/detail-id-<?php echo $value1['id'];?>.html" title="<?php echo $value1['title'];?>"><?php echo str_len($value1['title'],42);?></a></div>
<!--                    <div class="ind_news_time">--><?php //echo substr($value1['datetime'],5,2).' '.substr($value1['datetime'],8,2).','.substr($value1['datetime'],0,4);?><!--</div>-->
                    <div class="ind_news_time"><?php echo get_eng_date($value1['datetime']);?></div>
                    <div class="ind_news_info"><a href="/news/detail-id-<?php echo $value1['id'];?>.html"><?php echo str_len($value1['content'],88);?></a></div>
                </li>
                <?php }}?>
            </ul>
            <div class="ind_news_more"><a href="/news/index.html">Explore more<i class="ind_more_icon"></i></a></div>
        </div>
    </div>
	<div class="ind_product" style="background:url(<?php echo CP_BASE_STATIC;?>public/img/ind_product_bg.jpg) no-repeat center top;overflow:hidden;">
    	<a class="anchor" name="product"></a>
		<div class="container clearfix">
    		<div class="ind_product_left fl">
            	<div class="ind_product_name">SUPCON SPlant</div>
            	<div class="ind_product_info">Total Solution for Process Automation</div>
                <div class="ind_news_more ind_product_more"><a href="/product/index.html">Product porfolio<i class="ind_more_icon"></i></a></div>
            </div>
            <div class="ind_product_sildeBox fr">
            	<div class="hd">
                	<ul class="clearfix"></ul>
                </div>
                <div class="bd">
                	<ul>
                    	<?php foreach($product_rmd2 as $kk => $vv) { ?>
                    	<li><a href="/product/detail-id-<?php echo $vv['id']?>.html"><div class="ind_product_pic"><div class="img"><img src="/<?php echo get_imgurl($vv['pic']);?>" /></div></div></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
    	</div>
    </div>
	<div class="ind_partner">
    	<a class="anchor" name="partner"></a>
    	<div class="container">
            <div class="ind_title ind_partner_title"><a href="/career/partner.html">PARTNER</a></div>
            <div class="ind_partner_txt">JOIN & SUCCEED WITH SUPCON GLOBAL PARTNERSHIP PROGRAM</div>
            <div class="ind_news_more ind_partner_more"><a href="/career/partner.html">Explore more<i class="ind_more_icon"></i></a></div>
        </div>
    </div>
<?php include(CP_BASE_TEMP."template/include/footer.php")?>
</div>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/common.js"></script>
<script type="text/javascript" src="<?php echo CP_BASE_STATIC;?>public/js/swiper.min.js"></script>
<script src="<?php echo CP_BASE_STATIC;?>public/ckplayer/ckplayer.js" type="text/javascript"></script>
<script type="text/javascript">
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
		autoplay: 3000,
		loop: true,
        paginationClickable: true
    });
	var flashvars={
		p:0,
		e:1,
		ht:'20',
		hr:'http://www.ckplayer.com'
		};
	var video=['http://movie.ks.js.cn/flv/other/1_0.mp4->video/mp4'];
	var support=['all'];
	CKobject.embedHTML5('a1','ckplayer_a1',600,400,video,flashvars,support);	
	

$(document).ready(function() {
	
	var length=$(".swiper-pagination-bullets .swiper-pagination-bullet").length;
	if(length==1){
		$(".swiper-pagination-bullet").hide();
		$(".swiper-button-next").hide();
		$(".swiper-button-prev").hide();
	}
	
	var pro_length=$(".ind_product_sildeBox .hd ul li").length;
	if(pro_length==1){
		$(".ind_product_sildeBox .hd ul").hide();
	}
	

}); 
</script>
</body>
</html>