<?php
$keywords = explode(',',$detail['keywords']);

$relate_products_arr = array(); //相关产品
foreach($keywords as $k_k => $v_k){
    $relate_products = selectList("products","",'0','4',"`keywords` like '%".$v_k."%' and id<>".$detail['id']);
    $relate_products_arr = array_merge($relate_products_arr,$relate_products);
}

$relate_industry_arr = array(); //相关产业
foreach($keywords as $k_k => $v_k){
    $relate_industry = selectList("industry","",'0','2',"`keywords` like '%".$v_k."%' and id<>".$detail['id']);
    $relate_industry_arr = array_merge($relate_industry_arr,$relate_industry);
}

$relate_resource_arr = array(); //相关资源
foreach($keywords as $k_k => $v_k){
    $relate_resource = selectList("resource","",'0','4',"`keywords` like '%".$v_k."%' and id<>".$detail['id']);
    $relate_resource_arr = array_merge($relate_resource_arr,$relate_resource);
}

$relate_news_arr = array(); //相关新闻
foreach($keywords as $k_k => $v_k){
    $relate_news = selectList("news","",'0','3',"`keywords` like '%".$v_k."%' and id<>".$detail['id']);
    $relate_news_arr = array_merge($relate_news_arr,$relate_news);
}

?>
<div class="explore_more">
				<div class="tit">EXPLORE MORE</div>
				<ul class="explore_nav clearfix">
					<li class="on"><i class="icon1"></i>Products</li>
					<li><i class="icon2"></i>Industries</li>
					<li><i class="icon3"></i>Download</li>
					<li><i class="icon4"></i>News</li>
				</ul>
				<div class="explore_main">
					<div class="path" style="display:block;">
						<ul class="explore_pro clearfix">
							<?php foreach($relate_products_arr as $k1 => $v1){
                                if(!empty($v1['id'])){
                                    $pic_arr = explode('‖',$v1['pic']);?>
                                    <li title="<?php echo $v1['title'];?>"><a href="/product/detail-id-<?php echo $v1['id']?>.html">
                                        <div class="imgBox">
                                            <div class="img"><img src="/<?php echo get_imgurl($pic_arr[0]);?>" /></div>
                                        </div>
                                        <div class="main">
                                            <div class="title ellipsis"><?php echo $v1['title'];?></div>
                                            <div class="type"><?php echo $v1['title1'];?></div>
                                            <div class="con">
                                                <?php echo $v1['hangCon'];?>
                                            </div>
                                        </div>
                                    </a></li>
							<?php }}?>
						</ul>
					</div>
					<div class="path">
						<ul class="explore_ind clearfix">
                            <?php foreach($relate_industry_arr as $k2 => $v2){
                            if(!empty($v2['id'])){?>
							<li><a href="#">
								<i class="fl icon1"></i>
								<div class="main fr">
									<div class="title ellipsis" title="<?php echo $v2['title'];?>"><?php echo $v2['title'];?></div>
									<div class="con"><?php echo str_len($v2['content'],120);?></div>
								</div>
							</a></li>
							<?php }}?>
						</ul>
					</div>
					<div class="path">
						<ul class="explore_down clearfix">
                            <?php foreach($relate_resource_arr as $k3 => $v3){
                            if(!empty($v3['id'])){?>
							<li><a href="#">
								<img src="<?php echo '/'.get_imgurl($v3['pic']);?>" />
								<div class="type_icon"><?php echo $v3['filetype']?></div>
								<i class="<?php if($v3['lock']==1){echo 'lock_icon';}?>"></i>
								<p>SUPCON <?php echo get_classname($v3['classid']);?> <?php echo get_eng_date($v3['datetime']);?></p>
							</a></li>
							<?php }}?>
						</ul>
					</div>
					<div class="path">
						<ul class="explore_news clearfix">
                            <?php foreach($relate_news_arr as $k4 => $v4){
                            if(!empty($v4['id'])){?>
							<li><a href="#">
								<img src="/<?php echo $v4['pic']?get_imgurl($v4['pic']):'public/images/default_news_img.jpg';?>" />
								<div class="main">
									<div class="title"><?php echo str_len($v4['content'],20);?></div>
									<div class="date"><?php echo get_eng_date($v4['datetime']);?></div>
								</div>
							</a></li>
							<?php }}?>
						</ul>
					</div>
				</div>
			</div>