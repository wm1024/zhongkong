<?php
$product_class = getClassList(158);
//$news_class = getClassList(154);
?>
<div class="header">
	<div class="container clearfix">
		<div class="logo fl"><a href="/index/index.html"><img src="<?php echo CP_BASE_STATIC;?>public/images/logo.png" width="154" height="25" /></a></div>
		<div class="head_sub_nav fl">
        	<ul class="clearfix">
            	<li class="on"><a href="/about/index.html">About us</a></li>
            	<li><a href="/product/index.html">Products</a></li>
                <li><a href="/industry/index.html">Industries</a></li>
                <li><a href="/service/index.html">Service</a></li>
                <li><a href="/resource/index.html">Resources</a></li>
                <li><a href="/career/partner.html">Partner</a></li>
                <li><a href="/news/index.html">News</a></li>
            </ul>
        </div>
        <div class="head_sub_right clearfix">
            <a href="/resource/search.html"><div class="head_sub_search fl"><input type="submit" value="" class="search_sub_text" /></div></a>
            <a href="javascript:;" class="head_sub_add fl" onclick="join_favorite(document.title,window.location)"></a>
            <div class="head_sub_lan clearfix">
                <a href="##" class="head_sub_ch fl">中文</a>
                <div class="head_sub_lan_line fl"></div>
                <a href="/index/index.html" class="head_sub_en fr">EN</a>
            </div>
        </div>
	</div>

	<div class="header_nav_erify">    
    	<div class="container">	
        	<!--关于我们下拉-->
        	<div class="nav_erify_list clearfix">
            	<div class="head_erify_left fl">
    				<ul>
    					<li><a href="/about/index.html">Company overview</a></li>
    					<li><a href="/milestone/index.html">Milestone</a></li>
                        <li><a href="/quality/index.html">Quality assurance</a></li>
                        <li><a href="/career/index.html">Careers</a></li>
                        <li><a href="/contact/index.html">Contact us</a></li>
    				</ul>
    			</div>
                <div class="head_erify_right fr">
 					<div class="news_erify_cont clearfix">
                    <?php
                    $about_us = __query("select * from {tabpre}overview where `classid`=1139 and `show`=1 and `recover`=0 order by sequence desc,id asc");
                    $company_video = __query("select * from {tabpre}index where `classid`=1222 and `show`=1 and `recover`=0 order by sequence desc,id asc");
                    ?>
                        <div class="news_erify_info about_erify_info fl">
                    		<h2><a href="/about/index.html"><?php echo $about_us[0]['title'];?></a></h2>
                        	<div class="news_erify_txt"><a href="/about/index.html">
                            <?php echo $about_us[0]['content'];?>
                        	</a></div>
                            <a href="/about/index.html" class="head_erify_link">Explore More<i class="erify_link_icon"></i></a>
                        </div>
                    	<div class="news_erify_pic fr" style="background:url(<?php echo '/'.get_imgurl($company_video[0]['pic']);?>) no-repeat left top;">
                        	<a href="javascript:;" class="about_video" onclick="opennavVideo();"></a>
                        </div>
                    </div>
                </div>
           	</div>
        	<!--产品下拉-->
			<div class="nav_erify_list clearfix">
				<div class="head_erify_left head_product_nav fl">
    				<ul>
    					<li id="m1"><a href="/product/index.html">Total solution</a></li>
    			        <?php foreach($product_class as $key=>$value){
                            echo '<li id="m'.($key+2).'"><a href="/product/plist-pid-'.$value['ID'].'.html">'.$value['ClassName'].'</a></li>';
                        }?>
    				</ul>
    			</div>
                <div class="head_erify_right clearfix fr">
                    <div class="head_erify_list head_product_tabs fr">
                        <div class="head_erify_right_list">
                            <a href="/product/index.html"><img style="width: 460px" src="/public/img/product_index.png" /></a>
                        </div>
                    	<?php foreach($product_class as $key1 => $value1) { ?>
                        <div class="head_erify_right_list">
                            <div class="head_erify_left_list fl">
                                <ul>
                                    <?php
                                    $product_class_two = get_child_class($value1['ID']);
                                    $product_class_two_str = '';
                                    if(!empty($product_class_two)){
                                        foreach($product_class_two as $key2 => $value2){
                                            echo '<li><a href="';
                                            echo '/product/plist-pid-'.$value1['ID'].'-cid-'.$value2['ID'].'.html';
                                            echo '">'.$value2['ClassName'].'</a></li>';
                                            if($key2){
                                                $product_class_two_str .= ',';
                                            }
                                            $product_class_two_str .= $value2['ID'];
                                        }
                                    }else{
                                        $product_class_two_str .= $value1['ID'];
                                    }?>
                                </ul>
                            </div>
                            <?php $rmd_pro = __query("select * from {tabpre}products where `show`=1 and `recover`=0 and classid in(".$product_class_two_str.") and rmd3=1 order by sequence desc,id desc limit 0,1");
                            if(!empty($rmd_pro)){?>
                            <div class="head_erify_txt fl">
                                <h1><?php echo substr($rmd_pro[0]['title'],0,12);?></h1>
                                <div class="head_erify_info">
                                    <?php echo str_len($rmd_pro[0]['content1'],120);?>
                                </div>
                                <a href="   /product/plist-pid-<?php echo $value1['ID'];?>.html" class="head_erify_link">Explore more<i class="erify_link_icon"></i></a>
                            </div>
                            <div class="head_erify_pic fr">
                                <a href="#" class="clearfix">
                                    <div class="head_product_info fl">
                                        <h2><?php echo substr($rmd_pro[0]['title'],0,6);?></h2>
                                        <h3><?php echo $rmd_pro[0]['title1'];?></h3>
                                    </div>
                                    <div class="head_product_pic fr">
                                        <div class="img">
                                            <?php
                                                $rmd_pic_arr = explode('‖',$rmd_pro[0]['pic']);
                                                echo '<img src="/'.get_imgurl($rmd_pic_arr[0]).'" />';
                                            ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php }?>
                        </div>
                        <?php } ?>
					</div>

    			</div>
    		</div>
            <!--服务下拉-->
            <div class="nav_erify_list clearfix">
            	<div class="head_erify_left service_erify_left fl">
    				<ul>
    					<li><a href="/service/index.html">Life-cycle service</a></li>
    					<li><a href="/service/request.html">Submit your requirements</a></li>
    				</ul>
    			</div>
                <div class="head_erify_right service_erify_right fr">
					<div class="head_service_erify_tit">Need service now?</div>
                  	<div class="service_erify_cont clearfix">
                    	<div class="service_erify_list fl">
                    		<div class="service_erify_txt">
                        		Call SUPCON<br/>24/7 service hotline:
                                <i class="service_head_icon1"></i>
                        	</div>
                            <div class="service_erify_tel">400-887-6000</div>
                        </div>
                        <div class="service_erify_line fl">Or<div class="service_erify_mid_line"></div></div>
                    	<div class="service_erify_list fr" style="width:286px;">
                    		<div class="service_erify_txt">
                        		Submit your<br /> requirements online
                                <i class="service_head_icon2"></i>
                        	</div>
                            <a href="/service/request.html" class="head_erify_link">Online Support Center<i class="erify_link_icon"></i></a>
                        </div>
                	</div>
                </div>
           	</div>
            <!--新闻下拉-->
            <div class="nav_erify_list clearfix">
            	<div class="head_erify_left fl">
    				<ul>
    					<li><a href="/news/nlist-cid-1136.html">News Highlights</a></li>
    					<li><a href="/news/nlist-cid-1135.html">News</a></li>
                        <li><a href="/resource/rlist-cid-1177.html">Newsletter</a></li>
    				</ul>
    			</div>
                <div class="head_erify_right fr">
                    <?php
                        $nav_topnews_arr = get_nav_news();
                        if (!empty($nav_topnews_arr)) {
                            echo '<div class="news_erify_cont clearfix">
                            <div class="news_erify_info fl">
                                <h2><a href="/news/detail-id-'.$nav_topnews_arr[0]['id'].'.html">'.str_len($nav_topnews_arr[0]['title'],60).'</a></h2>
                                <h3>Release time：'.date('Y-m-d', strtotime($nav_topnews_arr[0]['datetime'])).'</h3>
                                <div class="news_erify_txt"><a href="/news/detail-id-'.$nav_topnews_arr[0]['id'].'.html">'.str_len($nav_topnews_arr[0]['content'],90).'</a></div>
                            </div>
                            <div class="news_erify_pic fr">
                            <a href="/news/detail-id-'.$nav_topnews_arr[0]['id'].'.html">';
                            if(!empty($nav_topnews_arr[0]['pic2'])){
                                echo '<img src="/'.get_imgurl($nav_topnews_arr[0]['pic2']).'" />';
                            }
                            echo '</a></div></div>';
                        }
                    ?>
                </div>
           	</div>
    	</div>
	</div>    
</div>

<div class="nav_video">
    <div class="ind_video_layer"></div>
    <div class="ind_video_main">
        <div id="a2"></div>
        <div class="nav_video_close" onclick="navcloseVideo();"><i class="close_video_icon"></i></div>
    </div>
</div>
<script src="<?php echo CP_BASE_STATIC;?>public/ckplayer/ckplayer.js" type="text/javascript"></script>
<script>
    //导航栏视频
    var flashvars={
        p:0,
        e:1,
        ht:'20',
        hr:'http://www.ckplayer.com'
    };
    var video=['<?php echo $company_video[0]['video'];?>'];
    var support=['all'];
    CKobject.embedHTML5('a2','ckplayer_a2',600,400,video,flashvars,support);

</script>
