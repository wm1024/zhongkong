<?php
    $links = __query("select * from {tabpre}links where `show`=1 and `recover`=0 order by rmd desc, datetime desc, id desc");
?>
<div class="footer">
    <div class="foot_top">
    	<div class="container clearfix">
    		<div class="foot_top_left fl">
            	<div class="foot_top_nav">
                	<ul class="clearfix">
                		<li style="padding-left:0;"><a href="/about/index.html">About us</a></li>
                        <li><a href="/product/index.html">Products</a></li>
                        <li><a href="/industry/index.html">Industries</a></li>
                        <li><a href="/service/index.html">Service</a></li>
                        <li><a href="/resource/index.html">Resources</a></li>
                        <li><a href="/career/partner.html">Partner</a></li>
                        <li style="border:none;"><a href="/news/index.html">News</a></li>
                	</ul>
                </div>
                <div class="foot_top_share">
					<div class="bdsharebuttonbox">
                    	<a title="Facebook" href="https://www.facebook.com/supcon" target="_blank" class="bdsfbook"></a>
                        <a title="linkedin" href="https://www.linkedin.com/company/zhejiang-supcon" target="_blank" class="bdslinkedin"></a>
                        <a title="Rss" href="http://en.supcon.com/Rss/testRss.xml" target="_blank" class="bdstwi"></a>
                        <div class="bds_mshare">
							<div class="share">
                            	<a href="http://www.facebook.com/share.php?u=mainUrl" class="share_facebook"></a>
                            	<a href="https://www.linkedin.com/uas/login?session_redirect=mainUrl" class="share_linkedin"></a>
                                <a href="http://www.tumblr.com/share/link?url=mainUrl" class="share_twitter"></a>
                                <!--<a href="#" class="share_weixin"></a>-->
                                <a href="http://login.sina.com.cn/sso/login.php?url=mainUrl" class="share_sina"></a>
                            </div>
                        </div>
                	</div>              
                </div>
                <div class="foot_top_link">
                	<ul class="clearfix">
                		<li><a href="/index/sitmap.html"><i class="foot_map"></i>Sitemap</a></li>
                        <li><a href="http://supcon.com/" target="_blank"><i class="foot_email"></i>E-mail</a></li>
                        <li><a href="javascript:;" onclick="pagePrint();"><i class="foot_print"></i>Print</a></li>
                	</ul>
                </div>
            </div>
    		<div class="foot_top_right fr">
            	<div class="foot_right_tit">Related Websites</div>
                <div class="foot_right_nav">
                	<p><?php echo $links[0]['title'];?></p>
                    <i class="foot_arrow"></i>
                	<ul>
                    	<?php foreach($links as $k_l => $v_l) { ?>
                    	<li><a href="<?php echo $v_l['link'];?>" target="_blank"><?php echo $v_l['title'];?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
    	</div>
    </div>
    <div class="foot_bot"><div class="container">Copyright©2016 SUPCON All Rights Reserved</div></div>
</div>