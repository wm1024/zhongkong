<div class="login_pop">
    <div class="login_layer"></div>
    <div class="login_main">
    	<div class="login_close"></div>
        <div class="login_pop_title">User Login</div>
        <div class="login_pop_tb">
            <form action="/member/login.html" method="post">
            <table width="100%">
                <tr>
                    <td style="height:38px;">Username</td>
                </tr>
                <tr>
                    <td style="height:40px;"><input type="text" value="Please enter your e-mail" _reg="Please enter your e-mail" class="login_pop_text" id="login_name" name="login[name]"/></td>
                </tr>
                <tr><td style="height:6px;"></td></tr>
                <tr>
                    <td style="height:38px;">Password</td>
                </tr>
                <tr>
                    <td><input type="password" value="" class="login_pop_text" id="login_pwd" name="login[password]"/></td>
                </tr>
                <tr>
                    <td style="height:38px;"><a href="javascript:;" class="login_forget fl" onclick="showForget();">Forget password?</a>
                        <a href="javascript:;" class="login_sign fr" onclick="showSign();">Sign up</a>
                    </td>
                </tr>
                <tr><td style="height:28px;"></td></tr>
                <tr>
                    <td><input type="submit" value="Login" class="loginBtn" onclick="return loginForm();" /></td>
                </tr>
            </table>
            </form>
        </div>
    </div>
    <div class="forget_main">
    	<div class="login_close"></div>
        <div class="login_pop_title">Forget password</div>
        <div class="login_pop_tb">
            <form action="/member/forget.html" method="post">
            <table width="100%">	
                <tr>
                    <td style="height:38px;">Your e-mail</td>
                </tr>
                <tr>
                    <td style="height:40px;"><input type="text" name="forget[email]" value="Please enter your e-mail" _reg="Please enter your e-mail" class="login_pop_text" id="forget_email" /></td>
                </tr>
                <tr>
                    <td style="height:44px;"><div class="tips"></div></td>
                </tr>
                <tr><td style="height:15px;"></td></tr>
                <tr>
                    <td><input type="submit" value="Submit" class="loginBtn" onclick="return forgetForm();" /></td>
                </tr>
            </table>
            </form>
        </div>
    </div>
    <div class="sign_main">
    	<div class="login_close"></div>
        <div class="login_pop_title">Sign up</div>
        <div class="login_pop_tb">
            <form action="/member/signup.html" method="post">
            <table width="100%">	
                <tr>
                    <td style="height:38px;position:relative;"><div class="must">*</div>Username / Email</td>
                </tr>
                <tr>
                    <td style="height:40px;"><input type="text" name="signup[email]" value="Please enter your e-mail" _reg="Please enter your e-mail" class="login_pop_text" id="sign_email" /></td>
                </tr>
                <tr>
                    <td style="height:38px;position:relative;"><div class="must">*</div>Password</td>
                </tr>
                <tr>
                    <td style="height:40px;"><input type="text" name="signup[password]" value="Enter your password" _reg="Enter your password" _pwd="Enter your password" class="login_pop_text" id="sign_pwd" /></td>
                </tr>
                <tr><td style="height:14px;"></td></tr>
                <tr>
                    <td style="height:40px;"><input type="text" name="signup[repeat_pwd]" value="Repeat password" _reg="Repeat password" _pwd="Repeat password" class="login_pop_text" id="repeat_pwd" /></td>
                </tr>
                <tr><td style="height:43px;"></td></tr>
                <tr>
                    <td style="height:38px;">Your name</td>
                </tr>
                <tr>
                    <td style="height:40px;"><input type="text" name="signup[name]" value="" class="login_pop_text" id="sign_name" /></td>
                </tr>
                <tr><td style="height:6px;"></td></tr>
                <tr>
                    <td style="height:38px;">Country/region</td>
                </tr>
                <tr>
                    <td style="height:40px;"><div class="select sign_area">
                        <p></p>
                        <i class="sign_arrow"></i>
                        <ul>
                            <li>China</li>
                            <li>India</li>
                            <li>Thailand</li>
                            <li>Burma</li>
                        </ul>
                        <input type="hidden" name="signup[nation]" value="" id="sign_country" />
                    </div>
                    </td>
                </tr>
                <tr><td style="height:6px;"></td></tr>
                <tr>
                    <td style="height:38px;">Company</td>
                </tr>
                <tr>
                    <td style="height:40px;"><input type="text" name="signup[company]" value="" class="login_pop_text" id="sign_conpany" /></td>
                </tr>
                <tr><td style="height:6px;"></td></tr>
                <tr>
                    <td style="height:38px;">Phone number</td>
                </tr>
                <tr>
                    <td style="height:40px;"><input type="text" name="signup[phone]" value="" class="login_pop_text" id="sign_phone" /></td>
                </tr>
                <tr>
                <tr><td style="height:30px;"></td></tr>
                    <td><input type="submit" value="Submit" class="loginBtn" onclick="return signForm();" /></td>
                </tr>
            </table>
            </form>
        </div>
    </div>
</div>